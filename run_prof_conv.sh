#!/bin/bash

NUM_ITER=1000

METHODS=( 'FLOAT' 'FXPT64' )
SAMPLE_RATES=( 250e6 500e6 750e6 1000e6 )
PULSE_LENS=(1000 2000 3000)
BB_RATES=(5e6 10e6 15e6 25e6 50e6 100e6)
NUM_SAMPLES_LIST=(50000 100000 150000 250000 500000 1000000)

RES_FILE='prof_conv_res.csv'

echo > ${RES_FILE}

for RATE_IND in "${!SAMPLE_RATES[@]}"
do
    SAMPLE_RATE=${SAMPLE_RATES[RATE_IND]}

    for BB_RATE_IND in "${!BB_RATES[@]}"
    do
        BB_RATE=${BB_RATES[BB_RATE_IND]}
        NUM_SAMPLES=${NUM_SAMPLES_LIST[BB_RATE_IND]}

        for PULSE_LEN_IND in "${!PULSE_LENS[@]}"
        do
            PULSE_LEN=${PULSE_LENS[PULSE_LEN_IND]}

            for METHOD_IND in "${!METHODS[@]}"
            do
                METHOD="${METHODS[METHOD_IND]}"
                PROF_CMD="./Release/lib/prof_conv --method=${METHOD} --sample_rate=${SAMPLE_RATE} --pulse_len=${PULSE_LEN} --baseband_rate=${BB_RATE} --num_samples=${NUM_SAMPLES} --num_iter=${NUM_ITER}"
                echo "Running ${PROF_CMD}"
                TIME_IN_SECS=$((time -p ${PROF_CMD})  2>&1 | grep user | sed 's/user //g')
                echo "  +  Done in ${TIME_IN_SECS} s"

                echo "${SAMPLE_RATE},${BB_RATE},${NUM_SAMPLES},${PULSE_LEN},${METHOD_IND},${NUM_ITER},${TIME_IN_SECS}" >> ${RES_FILE}
            done
                
        done
    done
done

