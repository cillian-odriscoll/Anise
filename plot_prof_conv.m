%
% Load the outputs of the chip convolution profiling run and generate some plots:

% Some fixed constants:
Tc = 1e-6;

d = csvread( 'prof_conv_res.csv' );

% Data format:
%
% fSample, fSampleBB, NSamples, pulse_len, method, num_iter, time_s
%
% method = 0 => Floating point indexing
% method = 1 => 64 bit fixed point indexing

fSampleInd = 1;
fSampleBBInd = 2;
NSamplesInd = 3;
pulseLenInd = 4;
methodInd = 5;
numIterInd = 6;
timeInd = 7;

METHOD_FLOAT = 0;
METHOD_FXPT64 = 1;

% Compute the number of chips per pulse_len
Nc = Tc.*d(:, fSampleInd);
NumChipsPerPulse = ceil( d(:,pulseLenInd)./Nc );

% Compute the average time per iter:
TPerIter = d(:,timeInd)./d(:,numIterInd);

% Compute the ratio between 'real' time and computation time:
Tgen = d(:, NSamplesInd)./d(:,fSampleBBInd);
Tratio = TPerIter./Tgen;

nCols = size(d,2);
chipsPerPulseInd = nCols+1;
tPerIterInd = nCols+2;
tRatioInd = nCols+3;

d = [ d, NumChipsPerPulse, TPerIter, Tratio ];

allFSample = unique( d(:, fSampleInd) );
allFSampleBB = unique( d(:,fSampleBBInd ) );
allChipsPerPulse = unique( d(:,chipsPerPulseInd) );


% Split by method
floatD = d( d(:,methodInd) == METHOD_FLOAT, : );
fxpt64D = d( d(:,methodInd) == METHOD_FXPT64, : );


for ii = 1:length(allFSample)

    currFSample = allFSample( ii );

    figure;

    legendStrs = {};

    for jj=1:length( allFSampleBB )

        currFSampleBB = allFSampleBB( jj );

        currFloatD = floatD( (floatD(:,fSampleInd)==currFSample) & ...
            (floatD(:,fSampleBBInd)==currFSampleBB), : );

        currFxpt64D = fxpt64D( (fxpt64D(:,fSampleInd)==currFSample) & ...
            (fxpt64D(:,fSampleBBInd)==currFSampleBB), : );

        if isempty( currFloatD ) || isempty( currFxpt64D )
            continue;
        end
        semilogy( currFloatD(:,chipsPerPulseInd), currFloatD(:,tRatioInd), '-', ...
            currFxpt64D(:, chipsPerPulseInd), currFxpt64D(:,tRatioInd ), '-.' );

        grid on; hold all;

        legendStrs = { legendStrs{:}, [ num2str( currFSampleBB/1e6 ) 'MHz FLOAT' ], ...
            ['     FXPT64' ] };

    end
    xlabel( '# chips per pulse' )
    ylabel( 'Computation Time/Real Time' );
    title( [ 'Pulse Sample Rate: ' num2str( currFSample/1e6 ) ...
        ' MHz.' ] );

    legend( legendStrs, 'Location', 'NorthWest' );
end

