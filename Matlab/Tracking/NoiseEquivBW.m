function bw = NoiseEquivBW( b, a, fs, N )
%
% NoiseEquivBW - Compute the noise equivalent bandwidth of a filter
%
% Usage:  bw = NoiseEquivBW( b, a, fs )
%
%   b - Filter numerator
%   a - Filter denominator
%   fs - The sampling frequency
%   N - Optional: number of points to use in the integration.
%
% The noise equivalent bandwidth of a digital filter H(z) is defined as
%
%          / 0.5
%          |                    2        fs
%   BWn =  | | H(exp(j*2*pi*fd)|  d fd  ----- 2
%          |                            |H(1)|
%          /-0.5
%
%   where:
%           b_0 + b_1 z + ... + b_n z^n       b(z)
%   H(z) = ----------------------------  =  --------
%           a_0 + a_1 z + ... + a_m z^m       a(z)

% (c) 2016 Cillian O'Driscoll

if nargin < 4
    N = 50000;
end

% dfd is our sampling of the digitial frequency around the unit circle
fd = linspace( -0.5, 0.5, N );
dfd = fd(2)-fd(1);

% Now reform the a and b inputs into column vectors:
bb = b(:);
aa = a(:);

% length of a and b
nb = length(b);
na = length(a);

%
% We use a little trick here: we generate a column vector of powers of form:
%
% z_i = alpha^i
%
% alpha = exp(1j*2*pi*dfd)
z = exp( 1j * (2*pi*fd ) );

% Here's the trick: the vandermonde matrix will generate all powers of z for
% each power required in the numerator and denominator respectively:
zb = myvander( z, nb );
za = myvander( z, na );

% so now we can compute the H(z) transfer function at all samples in one matrix multiplication:
% since we can evaluate b(z) as zb*b and a(z) = za*a
Hz = (zb*bb)./(za*aa);

% Finally, we need to normalise at the point z = 1;
H1 = sum( bb )/sum(aa );

bw = (sum( abs(Hz).^2 ) )/2/abs(H1)^2*fs*dfd;

%
% myvander
% get vander to work for rectangular matrices in Matlab
function A = myvander( v, n )


v = v(:);
if nargin < 2
    n = length(v);
end
if n == 0
    A = reshape(v, n, n);
    return;
end
A = repmat(v, 1, n);
A(:, n) = 1;
A = cumprod(A, 2, 'reverse');