function [ a, b ] = MakeLoopFilter( order, bw, T, doIncludeLastIntegrator )

% MakeLoopFilter - Design a tracking loop filter
%
% Usage:
%
% [ a, b ] = MakeLoopFilter( order, bw, T, doIncludeLastIntegrator )
%
% order - The loop order [1,2,3]
% bw    - THe noise equivalent bandwidth
% T     - The loop update interval
% doIncludeLastIntegrator - True if the last integrator is to be included

% (c) 2016 Cillian O'Driscoll

if nargin < 4
    doIncludeLastIntegrator = false;
end

if nargin < 3
    T = 0.001;
end

if ~( ( order == 1 ) | ( order == 2 ) | ( order == 3 ) )
    error( 'Order must be 1, 2 or 3' );
end

g1 = 0;
g2 = 0;
g3 = 0;

% Natural frequency
wn = 0;

zeta = 1/sqrt(2);

% The following is based on the bilinear transform approximation of
% the analog integrator. The loop format is from Kaplan & Hegarty
% Table 5.6. The basic concept is that the loop has a cascade of
% integrators:
% 1 for a 1st order loop
% 2 for a 2nd order loop
% 3 for a 3rd order loop
% The bilinear transform approximates 1/s as
% T/2(1 + z^-1)/(1-z^-1) in the z domain.

switch( order )
    case 1
        wn = bw*4.0;
        g1 = wn;
        if( doIncludeLastIntegrator )
            b = zeros( 1, 2 );
            b(1) = g1*T/2.0;
            b(2) = g1*T/2.0;

            a = zeros(1,2);
            a(1) = 1;
            a(2) = -1;
        else
            b = zeros( 1, 1 );
            b(1) = g1;
            a = zeros(1,1);
            a(1) = 1;
        end
    case 2
        wn = bw * (8*zeta)/ (4*zeta*zeta + 1 );
        g1 = wn*wn;
        g2 = wn*2*zeta;
        if( doIncludeLastIntegrator )
            b = zeros(1,3);
            b(1) = T/2*( g1*T/2 + g2 );
            b(2) = T*T/2*g1;
            b(3) = T/2*( g1*T/2 - g2 );

            a = ones( 1, 3 );
            a(2) = -2;
            a(3) = 1;
        else
            b = zeros( 1, 2 );
            b(1) = ( g1*T/2.0+g2 );
            b(2) = g1*T/2-g2;

            a = ones( 1, 2 );
            a(2) = -1;
        end

    case 3
        wn = bw / 0.7845; % From Kaplan
        a3 = 1.1;
        b3 = 2.4;
        g1 = wn*wn*wn;
        g2 = a3*wn*wn;
        g3 = b3*wn;

        if( doIncludeLastIntegrator )
            b = zeros(1,4);
            b(1) = T/2*(  g3 + T/2*( g2 +   T/2*g1 ) );
            b(2) = T/2*( -g3 + T/2*( g2 + 3*T/2*g1 ) );
            b(3) = T/2*( -g3 - T/2*( g2 - 3*T/2*g1 ) );
            b(4) = T/2*(  g3 - T/2*( g2 -   T/2*g1 ) );

            a = ones(1,4);
            a(2) = -3;
            a(3) = 3;
            a(4) = -1;
        else
            b = zeros(1,3);
            b(1) = g3 + T/2*( g2 + T/2*g1 );
            b(2) = g1*T*T/2 -2*g3;
            b(3) = g3 + T/2*( -g2 + T/2*g1 );


            a = ones(1,3);
            a(2) = -2;
            a(3) = 1;
        end

end
