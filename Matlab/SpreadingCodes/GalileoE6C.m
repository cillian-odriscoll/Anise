function code = GalileoE6C( prn )
%
% GalileoE6C - Generates the Galieo E6C Pilot Code
%
%   code = GalileoE6C( prn )
%
%   prn must be between 1 and 50
%
%   returns the codes as +/- 1
%

%!
% \file GalileoE6C.m
% \author Cillian O'Driscoll
%
% Copyright (C) 2020 Cillian O'Driscoll
%
%
% All rights reserved.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

if nargin < 1; prn = 1; end
    
persistent gGalileo_E6C_CODES

if isempty( gGalileo_E6C_CODES )
    t = load( 'GalileoE6C.mat' );
    gGalileo_E6C_CODES = t.Galileo_E6C_CODES;
end

if min( prn ) < 1 || max( prn ) > 50
    error( 'Unknown PRN for Galileo' );
end

code = gGalileo_E6C_CODES( prn, : );




