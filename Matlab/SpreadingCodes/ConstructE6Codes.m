function [Galileo_E6B_CODES, Galileo_E6C_CODES] = ConstructE6Codes()
%
% ConstructL1CCodes - Create and store the Galileo E6 codes
%
%   [Galileo_E6B_CODES, Galileo_E6C_CODES] = ConstructE6Codes
%
% This need only be called once. It stores the generated codes in mat files
% for future use by the GpsL1CD and GpsL1CP functions


Galileo_E6B_CODES = load_e6_codes_from_txt_file( 'Galileo_E6B.TXT');
Galileo_E6C_CODES = load_e6_codes_from_txt_file( 'Galileo_E6C.TXT');

save( 'GalileoE6B.mat', 'Galileo_E6B_CODES', '-v7' );
save( 'GalileoE6C.mat', 'Galileo_E6C_CODES', '-v7' );

function E6_CODES = load_e6_codes_from_txt_file( fileName )
Galileo_E6_CODE_LEN = 5115;
NUM_Galileo_CODES   = 50;

display( [ 'Processing: ' fileName ] );
fid = fopen( fileName );

if fid == -1
    error( ['Could not find the file ''' fileName ...
        ''' please ensure Anise is properly installed'] );
end

e6_table = textscan( fid, '%s %s', 'Delimiter', ';' );
fclose(fid);

e6_table = e6_table{2};

nr = length(e6_table);

if nr ~= NUM_Galileo_CODES
    error( [ 'Expected ' num2str(NUM_Galileo_CODES) ' codes, got ' ...
        num2str(nr) ] );
end

for ii = 1:NUM_Galileo_CODES
    tmp = 1 - 2*hex2binvec(e6_table{ii});
    E6_CODES(ii,:) = tmp(1:Galileo_E6_CODE_LEN);
end
