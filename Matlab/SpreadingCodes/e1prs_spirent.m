function code = e1prs_spirent( sv, codePhase, N )
%
%   e1prs_sprient - Generate chips of the E1 PRS as used by Spirent
%
%   code = e1prs_spirent( sv, codePhase, N );
%
%   sv - PRN number (1 <= sv <= 37 )
%   codePhase - Starting code phase to generate
%   N - number of chips to Generate
%
%   The Spirent PRS code is generated as a down sampled version of the GPS
%   P-code [1].
%
%  [1] https://support.spirent. com/SpirentCSC/SC_KnowledgeView?id=FAQ13323&origin=cscanswers

% (c) 2016 Cillian O'Driscoll

% Down sample by 4:
dsf = 4;

% offset:
offset = mod( sv, 2 );

pcodephase = codePhase*dsf - offset;

P_CODE_LENGTH = 24*3600*7*10.23e6;

if pcodephase < 0
    pcodephase = pcodephase + P_CODE_LENGTH;
end

pcode = gpspcode( sv, pcodephase, N*dsf );

code = pcode(1:dsf:end);
