function [E5aI_CODES, E5aQ_CODES] = ConstructE5aCodes()
%
% ConstructE5aCodes - Create and store the Galileo E5a codes
%
%  [E5aI_CODES, E5aQ_CODES] = ConstructE5aCodes()
%
% This need only be called once. It stores the generated codes in mat files
% for future use by the GalileoE5aI and GalileoE5aQ functions

GALILEO_E5aI_CODES = GalileoE5aISR(1:50);
GALILEO_E5aQ_CODES = GalileoE5aQSR(1:50);

save( 'GalileoE5aI.mat', 'GALILEO_E5aI_CODES', '-v7' );
save( 'GalileoE5aQ.mat', 'GALILEO_E5aQ_CODES', '-v7' );


