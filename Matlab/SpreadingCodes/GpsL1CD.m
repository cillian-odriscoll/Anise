function code = GpsL1CD( prn )
%
% GpsL1CD - Generates the GPS L1C data Code
%
%   code = GpsL1CD( prn )
%
%   prn must be between 1 and 63
%
%   returns the codes as +/- 1
%

%!
% \file GpsL1CD.m
% \author Cillian O'Driscoll
%
% Copyright (C) 2016 Cillian O'Driscoll
%
%
% All rights reserved.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

if nargin < 1; prn = 1; end
    
persistent gGPS_L1CD_CODES

if isempty( gGPS_L1CD_CODES )
    t = load( 'GpsL1CD.mat' );
    gGPS_L1CD_CODES = t.GPS_L1CD_CODES;
end

if min( prn ) < 1 || max( prn ) > 63
    error( 'Unknown PRN for GPS' );
end

code = gGPS_L1CD_CODES( prn, : );


