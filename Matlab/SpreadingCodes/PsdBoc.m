function psd = PsdBoc( f, fs, fc )
%
% PsdBoc - Sine Boc Sub-Carrier power spectral density
%
% Usage psd = PsdBoc( f, fs, fc )
%
%   f - Frequency at which to evaluate PsdBoc [Hz]
%   fs - Subcarrier rate [Hz]
%   fc - Code rate [chips/s]
%
% Example:
%   fo = 1.023e6;
%   fs = 6*fo;
%   fc = fo;
%   ff = linspace( -4*fs, 4*fs, 1000 );
%
%   Pxx = PsdBoc( ff, fs, fc );
%
%   figure;
%   plot( ff/1e6, 10*log10( Pxx ) )
%   grid on; hold all;
%
%   xlabel( 'Freq. [MHz]' );
%   ylabel( 'PSD dBW/Hz' );
%
%   ylim( [-150, -50] );
%
%   title( 'PSD BOC_s(6,1)' );
%

% (c) 2016 Cillian O'Driscoll

Ts = 1/(2*fs); Tc = 1/fc;

N = Tc/Ts;

psd = Ts/N*abs( sinc( f*Ts ) .* sin( N/2*pi + pi*f*Tc ) ./ cos( pi * f * Ts ) ).^2;
