function binvec = hex2binvec( hexStr )
% hex2binvec - Convert a hex string to a binary vector
%
%   binvec = hex2binvec( hexStr);
%

bitLen = length(hexStr)*4;

bidx = 1:4;

binvec = zeros(1,bitLen);

for ii = 1:length(hexStr)
    %fprintf('.');
    binvec(bidx) = dec2base(hex2dec(hexStr(ii)),2, 4) - '0';
    bidx = bidx+4;
end
    fprintf('\n');

%binvec = ( str2num(binvec) );
