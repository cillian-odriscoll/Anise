function L = GpsL1CLegendre()
%
% GpsL1CLegendre - Get the GPS L1 C Legendre sequence
%
%  L = GpsL1CLegendre
%
% L is the unique length 10223 sequence defined in GPS IS-800
%

%!
% \file GpsL1CLegendre.m
% \author Cillian O'Driscoll
%
% Copyright (C) 2016 Cillian O'Driscoll
%
%
% All rights reserved.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

N = 10223;

persistent L_GPS_L1C

if isempty( L_GPS_L1C )
    tSet = 0:(N-1);
    xSet = mod( tSet.^2, 10223 );

    L_GPS_L1C = zeros( size(tSet) );

    [ ~, congruentInds, ~] = intersect( tSet, xSet );

    L_GPS_L1C(congruentInds) = 1;
    L_GPS_L1C(1) = 0;

end

L = L_GPS_L1C;
