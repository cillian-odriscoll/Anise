function cs25 = GalileoE1cSecondaryCode

% Return the Galileo E1C Secondary Code (CS25)
%

% (c) 2016 Cillian O'Driscoll

cs25 = 1-2*[ 0 0 1 1 1 0 0 0 0 0 0 0 1 0 1 0 1 1 0 1 1 0 0 1 0 ];
