function [GPS_L1CD_CODES, GPS_L1CP_CODES] = ConstructL1CCodes()
%
% ConstructL1CCodes - Create and store the GPS l1C codes
%
%   [GPS_L1CD_CODES, GPS_L1CP_CODES] = ConstructL1CCodes
%
% This need only be called once. It stores the generated codes in mat files
% for future use by the GpsL1CD and GpsL1CP functions

GPS_L1C_CODE_LEN = 10230;
GPS_L1C_INSERTION_SEQ = [ 0 1 1 0 1 0 0 ];
Nseq = length( GPS_L1C_INSERTION_SEQ );

fid = fopen( 'IS_GPS_800_TableL1C_Codes.txt' );

if fid == -1
    error( 'Could not find the file ''IS_GPS_800_TableL1C_Codes.txt'' please ensure Anise is properly installed' );
end

l1c_table = fscanf( fid, '%d %d %d %8o %8o %d %d %8o %8o', [9, inf] ).';

fclose(fid);

[ nr, nc ] = size( l1c_table );

if nr ~= 63 || nc ~= 9
    error( [ 'Expected ''IS_GPS_800_TableL1C_Codes.txt'' to be 63x9, instead it is: ' ... 
        num2str( nr ) 'x' num2str(nc) ] );
end

GPS_L1CD_CODES = zeros( nr, GPS_L1C_CODE_LEN );
GPS_L1CP_CODES = zeros( nr, GPS_L1C_CODE_LEN );

pow2Vec = 2.^(23:-1:0);

success = 1;

for prn = 1:nr
    
    % L1C Pilot:
    weilInd = l1c_table(prn, 2 );
    insInd = l1c_table(prn, 3 )-1;

    W = GpsL1CWeil( weilInd );
    GPS_L1CP_CODES( prn, 1:insInd ) = W(1:insInd);
    GPS_L1CP_CODES( prn, insInd+(1:Nseq) ) = GPS_L1C_INSERTION_SEQ;
    GPS_L1CP_CODES( prn, (insInd + Nseq + 1):end ) = W((insInd+1):end);

    % Check:
    first24Bits = sum( GPS_L1CP_CODES(prn, 1:24 ) .* pow2Vec );
    if first24Bits ~= l1c_table( prn, 4 )
        disp( [ 'Check failed for first 24 bits of L1CP for prn' num2str( prn ) ] )
        success = 0;
    end

    last24Bits = sum( GPS_L1CP_CODES(prn, end-23:end ) .* pow2Vec );
    if last24Bits ~= l1c_table( prn, 5 )
        disp( [ 'Check failed for last 24 bits of L1CP for prn' num2str( prn ) ] )
        success = 0;
    end
    
     % L1C Data:
    weilInd = l1c_table(prn, 6 );
    insInd = l1c_table(prn, 7 )-1;
    W = GpsL1CWeil( weilInd );
    GPS_L1CD_CODES( prn, 1:insInd ) = W(1:insInd);
    GPS_L1CD_CODES( prn, insInd+(1:Nseq) ) = GPS_L1C_INSERTION_SEQ;
    GPS_L1CD_CODES( prn, (insInd + Nseq +1 ):end ) = W((insInd+1):end);
    
    % Check:
    first24Bits = sum( GPS_L1CD_CODES(prn, 1:24 ) .* pow2Vec );
    if first24Bits ~= l1c_table( prn, 8 )
        disp( [ 'Check failed for first 24 bits of L1CD for prn' num2str( prn ) ] )
        success = 0;
    end

    last24Bits = sum( GPS_L1CD_CODES(prn, end-23:end ) .* pow2Vec );
    if last24Bits ~= l1c_table( prn, 9 )
        disp( [ 'Check failed for last 24 bits of L1CD for prn' num2str( prn ) ] )
        success = 0;
    end

end

GPS_L1CD_CODES = 1-2*GPS_L1CD_CODES;
GPS_L1CP_CODES = 1-2*GPS_L1CP_CODES;

if success
    save( 'GpsL1CD.mat', 'GPS_L1CD_CODES', '-v7' );
    save( 'GpsL1CP.mat', 'GPS_L1CP_CODES', '-v7' );
end
