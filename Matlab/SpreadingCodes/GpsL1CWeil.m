function W = GpsL1CWeil( weilInd )
%
% GpsL1CWeil - The GPS L1C Weil sequence for given Weil index
%
%   W = GpsL1CWeil( weilInd );
%
%       weilInd - integer in range 1 <= weilInd <= 5111
%
%       W - Weil sequence of length 10223 \in [0,1]

%!
% \file GpsL1CWeil.m
% \author Cillian O'Driscoll
%
% Copyright (C) 2016 Cillian O'Driscoll
%
%
% All rights reserved.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


if weilInd < 1 || weilInd > 5111
    error( 'weilInd must be in the range [1,5111]' );
end

L = GpsL1CLegendre;
N = length(L);

shiftInds = mod( (0:(N-1)) + weilInd, N ) + 1;

W = bitxor( L, L(shiftInds) );

