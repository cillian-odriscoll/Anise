function code = gpsx1a(dbg)
%
%   gpsx1a - get the GPS X1A code, part of the P-Code

% (c) 2016 Cillian O'Driscoll


if nargin < 1
    dbg = 0;
else
    dbg = 1;
end

SR_LEN = 12;

CODE_PERIOD = 4092;

INIT_STATE = [ 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0];


% the feedback polynomial is:
% sum x^[0 6 8 11 12]
FB_TAPS =[ 6 8 11 12 ];

LEN = CODE_PERIOD;
nShifts = LEN; % First SR_LEN bits are already in the shift reg


%code = zeros(length(sv),LEN);
state = ones(1,SR_LEN);

% Initialise: Put the init state at the start of the code
%code(:,1:27) = CM_INIT_STATES(sv,:);
state(:,1:SR_LEN) = 1-2*INIT_STATE;

code = zeros(1,LEN);


% loop over all shifts
code(1) = state(end);
for i = 2:1:LEN

    tmp = prod(state(FB_TAPS));
    state(2:end) = state(1:(end-1));
    state(1) = tmp;

    code(i) = state(end);

end % for i

code = (1-code)/2;


if dbg
    disp( [ 'State: ' num2str( (1-state)/2 ) ] );
end


