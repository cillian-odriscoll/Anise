function code = GalileoE5aQSR( prn )
%
% GalileoE5aQSR - Generates the Galileo E5aQ Code
%
%   code = GalileoE5aQSR( prn )
%
%   prn must be between 1 and 50
%
%   returns the codes as +/- 1
%
%   Uses a shift register implementation
%

%!
% \file GalileoE5aQ.m
% \author Cillian O'Driscoll
%
% Copyright (C) 2016 Cillian O'Driscoll
%
%
% All rights reserved.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

CODE_LEN = 10230;
NUM_CODES = 50;
SR_LEN = 28;

persistent FB_TAPS INIT_STATE 

if isempty( FB_TAPS )
    % These are the shift register taps in octal:
    TAPS_1_OCT = '40503';
    TAPS_2_OCT = '50661';

    % Convert to decimal:
    TAPS_1_DEC = sscanf( TAPS_1_OCT, '%o' );
    TAPS_2_DEC = sscanf( TAPS_2_OCT, '%o' );

    % Convert decimal to binary vector:
    TAPS_1_VEC = double( int8(dec2bin( TAPS_1_DEC ))-int8('0') );
    TAPS_2_VEC = double( int8(dec2bin( TAPS_2_DEC ))-int8('0') );

    % Now compute the resultant 28 stage shift register taps:
    TAPS_VEC = mod( conv( TAPS_1_VEC, TAPS_2_VEC ), 2 );

    FB_TAPS = find( TAPS_VEC(1:(end-1)) ) - 1;


    %
    % Now look at the first N chips:
    INIT_STATE_HEX = [ ...
        '515537A'; 'D67539A'; '58B2E58'; '3059141'; '4427106'; '593CF84'; '214AD70'; ...
        '435EA61'; 'C1A7D55'; 'F0E94A2'; 'A8C2390'; '2EB63BA'; '8F0A462'; '896DD4C'; ...
        '0245F1F'; 'EB01608'; 'ED28B36'; 'CB9F5B3'; '576592F'; 'A888113'; 'DD36498'; ...
        'B59F424'; 'FF81F6E'; 'CE81289'; 'DCD55CF'; '79E4509'; 'B634600'; '6D562B9'; ...
        '3A9010C'; '59CD72F'; 'C0211AF'; '28EB964'; 'D7554B0'; '4251265'; 'E0DAFB9'; ...
        'EF79F2E'; '18085D4'; 'D50CD8E'; '0447B9C'; '8DE8774'; '0D1FA05'; 'C9FCF7D'; ...
        '48116D6'; '840BCC5'; '152004D'; '0D4897E'; 'AF6D254'; '4B7593A'; '71BB1B2'; ...
        '53DA0E7'; ...
    ];


    INIT_STATE_DEC = hex2dec( INIT_STATE_HEX );
    INIT_STATE = double( int8( dec2bin( INIT_STATE_DEC ) ) - int8('0') );
end

nbits = CODE_LEN;

nShifts = nbits - SR_LEN; % First SR_LEN bits are already in the shift reg

if min(prn) < 1 || max(prn) > NUM_CODES
    error([' ERROR: prn must be in the range (1,',num2str(NUM_CODES),')']);
else
    
    %code = zeros(length(prn),LEN);
    code = ones(length(prn),CODE_LEN);
    
    % Initialise: Put the init state at the start of the code
    %code(:,1:27) = CM_INIT_STATES(prn,:);
    code(:,1:SR_LEN) = 1-2*INIT_STATE(prn,:);
    
    k = FB_TAPS;
    % loop over all shifts
    for i = 1:1:nShifts
        
        k = k+1;
        code(:,i+SR_LEN) = prod(code(:,k),2);
        
    end % for i
    
end


