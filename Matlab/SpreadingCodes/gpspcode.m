function code = gpspcode( sv, codePhase, N )
%
%   gpspcode - Generate chips of the GPS p-code
%
%   code = gpspcode( sv, tow, N );
%
%   sv - PRN number (1 <= sv <= 37 )
%   codePhase - Starting code phase to generate
%   N - number of chips to Generate

% (c) 2016 Cillian O'Driscoll

WK_SECS = 24*3600*7;
fc = 10.23e6;
MAX_CODE_PHASE = WK_SECS*fc;
tow = codePhase/fc;

if sv < 1 || sv > 37
    error( [ 'Invalid sv number: ' sv '. Should be between 1 and 37' ] );
end

if tow < 0 || codePhase > MAX_CODE_PHASE
    error( [ 'Invalid TOW: ' num2str( tow ) '. Should be between 0 and ' num2str( WK_SECS ) ] );
end

% Setup some constants:
% L - code length
% N - number of codes in an epoch
% xij - one of the X1A, X1B, X2A, X2B codes
L1A = 4092;
L1B = 4093;
L2A = 4092;
L2B = 4093;

N1A = 3750;
N1B = N1A-1;

N2A = 3750;
N2B = N2A - 1;

T1 = 1.5; % The X1 epoch in seconds
L1 = L1A*N1A; % Number of chips in the X1 epoch
N1 = WK_SECS/T1; % The number of X1 epochs in a week

Lp = L1*N1; % Total number of chips in the P-Code

T2 = T1 + 37/fc;
N2 = floor( WK_SECS/T2 );
L2 = L2A*N2A + 37;

persistent x1a x1b x2a x2b;
if isempty( x1a )
    x1a = gpsx1a();
    x1b = gpsx1b();
    x2a = gpsx2a();
    x2b = gpsx2b();
end

% Now establish the starting x1 epoch:
% The code index:
np = floor( tow*fc ) + (0:(N-1));
np2 = mod( np-sv, Lp );

e1 = floor( np/L1 ); % X1 epoch
e2 = floor( np2/L2 ); % X2 epoch

e1a = floor( (np-e1*L1)/L1A );
e1b = floor( (np - e1*L1)/L1B );

e2a = floor( (np2-e2*L2)/L2A );
e2b = floor( (np2-e2*L2)/L2B );


n1a = mod( np, L1A );
n1b = (np - e1*L1 - e1b*L1B);

n2a = np2 - e2*L2 - e2a*L2A;
n2b = np2 - e2*L2 - e2b*L2B;

% Now handle the hold at the end of an epoch:
n1b( e1b >= N1B ) = L1B - 1;
n2a( e2a >= N2A ) = L2A - 1;
n2b( e2b >= N2B ) = L2B - 1;


% Finally handle the end of week cases:
n2a( e2 == N2 & e2a > 103 ) = L2A - 1;
n2b( e2 == N2 & e2b > 103 ) = L2B - 1;

code = (1-2*x1a( n1a + 1 ) ) .* ( 1 - 2* x1b(n1b + 1) ) .* ...
    ( 1 - 2*x2a(n2a+1) ) .* ( 1 - 2*x2b(n2b+ 1) );

code = (1-code)/2;
