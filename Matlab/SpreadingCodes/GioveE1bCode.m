function code = GioveE1bCode(sv,nbits)
% Usage: GioveE1bCode(sv,[nbits])
% Generates the Giove E1-b code for the satellite with
% ID = 'sv', generates 'nbits' bits; default = 4092.

% (c) 2016 Cillian O'Driscoll

sv = 1;

NUM_CODES = 2;

GIOVE_E1B_PERIOD = 4092;

SR_LEN = 26;
if nargin < 2
    nbits = GIOVE_E1B_PERIOD;
end
%CM_PERIOD = 1000;

%if nargin < 2, nbits = GIOVE_E1B_PERIOD; end

% Init states LSB to MSB
GIOVE_E1B_INIT_STATE = [ 1 1 0 1 1 1 0 0 0 0 0 1 1 0 0 0 0 0 1 1 0 1 1 1 1 1];


% the feedback polynomial is:
% 0o1112225171 ==  sum x^[27,24,21,19,16,13,11,9,6,5,4,3,0]
% FB_TAPS: 
FB_TAPS = [ 0 1 3 8 10 11 16 18 20 22 ];
% NUM_TAPS = length(FB_TAPS);

LEN = nbits;
nShifts = nbits - SR_LEN; % First SR_LEN bits are already in the shift reg

if min(sv) < 1 || max(sv) > NUM_CODES
    error([' ERROR: sv must be in the range (1,',num2str(NUM_CODES),')']);
else
    
    %code = zeros(length(sv),LEN);
    code = ones(length(sv),LEN);
    
    % Initialise: Put the init state at the start of the code
    %code(:,1:27) = CM_INIT_STATES(sv,:);
    code(:,1:SR_LEN) = 1-2*GIOVE_E1B_INIT_STATE;
    
    k = FB_TAPS;
    % loop over all shifts
    for i = 1:1:nShifts
        
        k = k+1;
        code(:,i+SR_LEN) = prod(code(:,k),2);
        
    end % for i
    
    code = (1-code)/2;
    
end
