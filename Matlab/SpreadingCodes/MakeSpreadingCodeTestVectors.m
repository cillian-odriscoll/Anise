function MakeSpreadingCodeTestVectors( theCodes, fileName, numBytes )
%
% MakeSpreadingCodeTestVectors - Generate test vectors for a set of spreading codes
%
% MakeSpreadingCodeTestVectors( theCodes, fileName, numBytes )
%   theCodes - Matrix of spreading codes, one code per row
%   fileName - The name of the file in which to store the test vectors
%   numBytes - The number of bytes to output in the test vectors
%   
% The output file will be filled with test vectors of form:
%
% PRN  FFBB99AA2200
%
% where the PRN number is assumed to start at 1 and the test vector is the hex
% representation of the last numBytes bytes in the sequence

[nSv, codeLen] = size( theCodes );
numBits = numBytes * 8;

if numBits > codeLen
    error( [ 'Requested ' num2str( numBytes ) ' bytes, but code is only ' num2str( codeLen/8 ) ' bytes long' ] );
end

ofid = fopen( fileName, 'w' );

if ofid == -1
    error( [ 'Unable to open output file ' fileName ] );
end

theBits = theCodes( :, (end-numBits+1):end );

for prn = 1:nSv
    fprintf( ofid, '%02d ', prn );
    for ii = 1:numBytes
        currBits = ( 1 - theBits(prn, ((ii-1)*8 + 1):(ii*8) ) )/2;
        currDec = 2.^(7:-1:0)*currBits';
        fprintf( ofid, '%02x', currDec );
    end
    fprintf( ofid, '\n' );

end


fclose( ofid );
