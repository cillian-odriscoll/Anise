function code = gpsx1()
%
% gpsx1 - Generates the GPS X1 code

% (c) 2016 Cillian O'Driscoll
if nargin < 1
    dbg = 0;
else
    dbg = 1;
end

SR_LEN = 12;

x1a_period = 4092;
x1b_period = 4093;

N1a = 3750;
N1b = N1a - 1;

maxb = N1b*x1b_period;

CODE_PERIOD = x1a_period*N1a;

% Code indices:
np = (0:(CODE_PERIOD-1));

%x1a indices
na = mod( np, x1a_period );
nb = mod( np, x1b_period );

nb( np >= maxb ) = x1b_period-1;

persistent x1a;
x1a = gpsx1a();
persistent x1b;
x1b = gpsx1b();

code = ( 1 - 2*x1a(na+1) ).*( 1 - 2*x1b(nb+1) );
code = (1-code)/2;
