function code = gpsx1b(dbg)
%
% gpsx1b - Generate the GPS X1B code, part of the P-Code

% (c) 2016 Cillian O'Driscoll

if nargin < 1
    dbg = 0;
else
    dbg = 1;
end

SR_LEN = 12;

CODE_PERIOD = 4093;

INIT_STATE = [ 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0];


% the feedback polynomial is:
FB_TAPS =[ 1 2 5 8 9 10 11 12 ];

LEN = CODE_PERIOD;
nShifts = LEN; % First SR_LEN bits are already in the shift reg


%code = zeros(length(sv),LEN);
state = ones(1,SR_LEN);

% Initialise: Put the init state at the start of the code
%code(:,1:27) = CM_INIT_STATES(sv,:);
state(:,1:SR_LEN) = 1-2*INIT_STATE;

code = zeros(1,LEN);


% loop over all shifts
code(1) = state(end);
for i = 2:1:LEN

    tmp = prod(state(FB_TAPS));
    state(2:end) = state(1:(end-1));
    state(1) = tmp;

    code(i) = state(end);

end % for i

code = (1-code)/2;

if dbg
    disp( [ 'State: ' num2str( (1-state)/2 ) ] );
end


