function code = GalileoE1c( prn )
%
% GalileoE1c - Generates the Galileo E1c Code
%
%   code = GalileoE1c( prn )
%
%   prn must be between 1 and 50
%
%   returns the codes as +/- 1
%

% (c) 2016 Cillian O'Driscoll

if nargin < 1; prn = 1; end
    
global gGALILEO_E1C_CODES

if isempty( gGALILEO_E1C_CODES )
    t = load( 'MCodeE1Cr' );
    gGALILEO_E1C_CODES = -t.Rcode;
end

if min( prn ) < 1 || max( prn ) > 50
    error( 'Unknown PRN for Galileo' );
end

code = gGALILEO_E1C_CODES( prn, : );

