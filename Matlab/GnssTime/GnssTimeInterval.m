classdef GnssTimeInterval < handle
    %GnssTimeInterval - Class representing a time interval in a GNSS time
    %system. This class permits us to represent very long time intervals
    %(decades) with a high degree of precision (sub-femto second).
    %
    % GnssTimeInterval methods:
    %   AsSeconds - Gets the interval as seconds [potential loss of precision]
    %   AsWeeks - Gets the interval as an integer number of weeks
    %
    % GnssTimeInterval static methods:
    %   Weeks - Create a GnssTimeInterval of an integer number of weeks
    %   Days - Create a GnssTimeInterval of an integer number of days
    %   Hours - Create a GnssTimeInterval of a given number of hours
    %   Seconds - Create a GnssTimeInterval of a given number of seconds
    %   MilliSeconds - Create a GnssTimeInterval of a given number of milliseconds
    %   MicroSeconds - Create a GnssTimeInterval of a given number of microseconds
    %   NanoSeconds - Create a GnssTimeInterval of a given number of nanoseconds

    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
    end
    methods
        %% Constructor - Create a new C++ class instance
        function this = GnssTimeInterval(varargin)
            this.objectHandle = GnssTimeInterval_interface('new', varargin{:});
        end

        %% Destructor - Destroy the C++ class instance
        function delete(this)
            GnssTimeInterval_interface('delete', this.objectHandle);
        end

        %-- DISPLAY
        function varargout = disp( this )
            nWeeks = this.AsWeeks();
            modWeek = this - GnssTimeInterval.Weeks( nWeeks );
            modSecond = mod( modWeek.AsSeconds(), 1.0 );
            intSeconds = floor( modWeek.AsSeconds() );
            fracSeconds = modWeek - GnssTimeInterval.Seconds( intSeconds );
            nanoSeconds = fracSeconds.AsSeconds()*1e9;
            str = '';
            if abs( nWeeks ) > 0
                str = [ num2str( nWeeks ) ' Wk '];
            end

            str = [ str num2str( intSeconds ) ' Secs ' ];
            str = [ str num2str( nanoSeconds ) ' ns ' ] ;
            if nargout == 0
                disp( str );
            else
                varargout{1} = str;
            end

        end

        %% AsWeeks - Get the integer number of weeks in the time interval
        function varargout = AsWeeks(this, varargin)
        %
        % AsWeeks - Conver the time interval to a number of weeks
            varargout{1} = GnssTimeInterval_interface('AsWeeks', this.objectHandle, varargin{:});
        end

        %% AsSeconds - Get the number of seconds in the time interval (may overflow)
        function varargout = AsSeconds(this, varargin)
        %
        % AsSeconds - Convert the time interval to a number of seconds
        %
        %   dtAsSecs = dt.AsSeconds();
        %
        %   Note that this can cause overflow for very long time intervals 
            varargout{1} = GnssTimeInterval_interface('AsSeconds', this.objectHandle, varargin{:});
        end

        %%----------- OPERATORS

        %% Plus - Add two time intervals
        function varargout = plus(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTimeInterval( ...
                    GnssTimeInterval_interface('plus', this.objectHandle, varargin{1}.objectHandle) );
            else
                varargout{1} = plus( this, GnssTimeInterval.Seconds( varargin{1} ) );
            end
        end

        %% Minus - Difference of two time intervals
        function varargout = minus(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTimeInterval( ...
                    GnssTimeInterval_interface('minus', this.objectHandle, varargin{1}.objectHandle) );
            else
                varargout{1} = minus( this, GnssTimeInterval.Seconds( varargin{1} ) );
            end
        end

        %% eq - comparison operator
        function varargout = eq(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTimeInterval_interface('eq', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% ne - inequality operator
        function varargout = ne(this, varargin)
            varargout{1} = ~(this == varargin{1});
        end

        %% lt - less than operator
        function varargout = lt(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTimeInterval_interface('lt', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% gt - greater than operator
        function varargout = gt(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTimeInterval_interface('gt', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% le - less than or equal to operator
        function varargout = le(this, varargin)
            varargout{1} = ~( this > varargin{1} );
        end

        %% ge - greater than or equal to operator
        function varargout = ge(this, varargin)
            varargout{1} = ~( this < varargin{1} );
        end


    end % methods

    methods( Static )
        function varargout = Weeks(varargin)
        %
        % Weeks - Generate a GnssTimeInterval of the given number of weeks
        %
        %   eg.
        %   currTime = GnssTimeInterval.Weeks( 1857 );
        %
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('Weeks', varargin{:}) );
        end

        function varargout = Days(varargin)
        % Days - Generate a GnssTimeInterval of the given number of days
        %
        %   eg
        %   currTime = GnssTimeInterval.Days( 12 );
        %
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('Days', varargin{:}) );
        end

        function varargout = Hours(varargin)
        % Hours - Create a time interval with the given integer number of hours
        %
        % eg
        %   currTime = GnssTimeInterval.Hours( 6 );
        %
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('Hours', varargin{:}) );
        end

        function varargout = Seconds(varargin)
        % Seconds - Create a time interval with the given number of seconds
        %
        % eg
        %   currTime = GnssTimeInterval.Weeks( 1857 ) + ...
        %       GnssTimeInterval.Seconds( 204000 ) + ...
        %       GnssTimeInterval.NanoSeconds( 300.0123 );
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('Seconds', varargin{:}) );
        end

        function varargout = MilliSeconds(varargin)
        % MilliSeconds - Create a time interval with the given number of milliseconds
        %
        %   currTime = GnssTimeInterval.Weeks( 1857 ) + ...
        %       GnssTimeInterval.Seconds( 204000 ) + ...
        %       GnssTimeInterval.MilliSeconds( 300.0123 );
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('MilliSeconds', varargin{:}) );
        end

        function varargout = MicroSeconds(varargin)
        % MicroSeconds - Create a time interval with the given number of microseconds
        %
        %   currTime = GnssTimeInterval.Weeks( 1857 ) + ...
        %       GnssTimeInterval.Seconds( 204000 ) + ...
        %       GnssTimeInterval.MicroSeconds( 300.0123 );
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('MicroSeconds', varargin{:}) );
        end

        function varargout = NanoSeconds(varargin)
        % NanoSeconds - Create a time interval with the given number of nanoseconds
        %
        % eg
        %   currTime = GnssTimeInterval.Weeks( 1857 ) + ...
        %       GnssTimeInterval.Seconds( 204000 ) + ...
        %       GnssTimeInterval.NanoSeconds( 300.0123 );
            varargout{1} = GnssTimeInterval( ...
                GnssTimeInterval_interface('NanoSeconds', varargin{:}) );
        end

    end % methods( Static)
end
