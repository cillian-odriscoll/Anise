classdef GnssTime < handle
    %GnssTime - Class representing a point in time in a GNSS time system
    % This class allows us to represent a point in time with a high degree of
    % accuracy over a very large time range. At their heart each GnssTime object
    % consists of a System identifer (GPS, Galileo, etc), a Week number and a
    % Time of Week (TOW)
    %   
    % GnssTime Methods:
    %   System - Gets the GNSS time system that this object refers to
    %   Week - Get the week number of the GnssTime object
    %   TOW - Get the time of week [s] of the GnssTime object
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
    end
    methods
        %% Constructor - Create a new C++ class instance
        function this = GnssTime(varargin)
        % GnssTime - Construct a GnssTime object
        %
        %   t = GnssTime( sys, dt );
        %
        %   sys - Gnss System identifier
        %   dt - A GnssTimeInterval object representing the time since the sys
        %        epoch
        %
        % See also:
        %   GnssTimeInterval
            if nargin == 1
                if isa( varargin{1}, 'GnssTime' )
                    this.objectHandle = GnssTime_interface( 'copy', varargin{1}.objectHandle);
                elseif isa( varargin{1}, 'uint64' )
                    this.objectHandle = GnssTime_interface( 'copy', varargin{1} );
                else
                    this.objectHandle = GnssTime_interface( 'new', varargin{:} );
                end
            elseif nargin == 2 && isa( varargin{2}, 'GnssTimeInterval' )
                this.objectHandle = GnssTime_interface( 'new', varargin{1}, varargin{2}.objectHandle );
            else
                this.objectHandle = GnssTime_interface('new', varargin{:});
            end
        end

        %% Destructor - Destroy the C++ class instance
        function delete(this)
            GnssTime_interface('delete', this.objectHandle);
        end

        %-- DISPLAY
        function varargout = disp( this )
            str = [ 'System: ' num2str( this.System() ) ' ' ];
            dt = this - GnssTime( this.System(), GnssTimeInterval.Seconds(0) );
            towStr = disp( dt );
            str = [ str towStr ];
            if nargout == 0
                disp( str );
            else
                varargout{1} = str;
            end
        end

        %% System - Get the ID of the system to which this time is referenced
        function varargout = System(this, varargin)
        % System - Get the GNSS system to which this GnssTime object refers
        %
        %   theSys = t.System();
        %
            varargout{1} = GnssTime_interface('System', this.objectHandle, varargin{:});
        end

        %% Week - Get the week number of this time point relative to the system epoch
        function varargout = Week(this, varargin)
        % Week - Get the Week number of the GnssTime object
        %
        %   wkNo = t.Week();
        %
        % See also:
        %   TOW
            varargout{1} = GnssTime_interface('Week', this.objectHandle, varargin{:});
        end

        %% TOW - Get the time of week of this time point relative to the system epoch
        function varargout = TOW(this, varargin)
        % TOW - Get the Time of Week [s] of the GnssTime object
        %
        %   tow = t.TOW();
        %
        % See also:
        %   Week
            varargout{1} = GnssTime_interface('TOW', this.objectHandle, varargin{:});
        end

        %%----------- OPERATORS

        %% Plus - Add a time interval to a GNSS Time
        function varargout = plus(this, varargin)
            if isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTime( ...
                    GnssTime_interface('plus', this.objectHandle, varargin{1}.objectHandle) );
            else
                varargout{1} = plus( this, GnssTimeInterval.Seconds( varargin{1} ) );
            end
        end

        %% Minus - Difference of two time intervals
        function varargout = minus(this, varargin)
            if isa( varargin{1}, 'GnssTime' )
                varargout{1} = GnssTimeInterval( ...
                    GnssTime_interface( 'minus', this.objectHandle, varargin{1}.objectHandle ) );
            elseif isa( varargin{1}, 'GnssTimeInterval' )
                varargout{1} = GnssTime( ...
                    GnssTime_interface('minus_time_interval', ...
                    this.objectHandle, varargin{1}.objectHandle) );
            else
                varargout{1} = minus( this, GnssTimeInterval.Seconds( varargin{1} ) );
            end
        end

        %% eq - comparison operator
        function varargout = eq(this, varargin)
            if isa( varargin{1}, 'GnssTime' )
                varargout{1} = GnssTime_interface('eq', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% ne - inequality operator
        function varargout = ne(this, varargin)
            varargout{1} = ~(this == varargin{1});
        end

        %% lt - less than operator
        function varargout = lt(this, varargin)
            if isa( varargin{1}, 'GnssTime' )
                varargout{1} = GnssTime_interface('lt', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% gt - greater than operator
        function varargout = gt(this, varargin)
            if isa( varargin{1}, 'GnssTime' )
                varargout{1} = GnssTime_interface('gt', ...
                    this.objectHandle, varargin{1}.objectHandle);
            else
                varargout{1} = false;
            end
        end

        %% le - less than or equal to operator
        function varargout = le(this, varargin)
            varargout{1} = ~( this > varargin{1} );
        end

        %% ge - greater than or equal to operator
        function varargout = ge(this, varargin)
            varargout{1} = ~( this < varargin{1} );
        end
    end
end
