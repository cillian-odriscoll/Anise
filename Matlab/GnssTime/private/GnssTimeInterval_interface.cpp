#include "mex.h"
#include "class_handle.hpp"
#include <Anise/GnssTime.h>
#include <iostream>


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the command string
    char cmd[64];
	if (nrhs < 1 || mxGetString(prhs[0], cmd, sizeof(cmd)))
		mexErrMsgTxt("First input should be a command string less than 64 characters long.");

    // New
    if (!strcmp("new", cmd)) {

        if( nlhs != 1 ){
            mexErrMsgTxt("New: must have one output argument");
        }

        // Check input parameters
        Anise::GnssTimeInterval *pNew = nullptr;
        if( nrhs == 1 ){
            pNew = new Anise::GnssTimeInterval( Anise::GnssTimeInterval::Seconds(0) );
        }
        else if (nrhs == 2){
            // This should be a copy constructor:
            Anise::GnssTimeInterval const*pRhs = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[1] );

            if( pRhs != nullptr ){
                pNew = new Anise::GnssTimeInterval( *pRhs );
            }
        }
        else{
            mexErrMsgTxt("New: incorrect number of arguments");
        }


        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<Anise::GnssTimeInterval>(pNew);
        return;
    }

    // Static Methods:
    // Weeks
    if (!strcmp("Weeks", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("Weeks: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        int nWeeks = static_cast< int >( mxGetScalar( prhs[1] ) );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::Weeks(nWeeks) ) );
        return;
    }
    // Days
    if (!strcmp("Days", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("Hours: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        int nDays = static_cast< int >( mxGetScalar( prhs[1] ) );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::Days(nDays) ) );
        return;
    }
    // Hours
    if (!strcmp("Hours", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("Hours: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        int nHours = static_cast< int >( mxGetScalar( prhs[1] ) );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::Hours(nHours) ) );
        return;
    }
    // Seconds
    if (!strcmp("Seconds", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("Seconds: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        double dSeconds = mxGetScalar( prhs[1] );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::Seconds(dSeconds) ) );
        return;
    }
    // MilliSeconds
    if (!strcmp("MilliSeconds", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("MilliSeconds: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        double dMilliSeconds = mxGetScalar( prhs[1] );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::MilliSeconds(dMilliSeconds) ) );
        return;
    }
    // MicroSeconds
    if (!strcmp("MicroSeconds", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("MicroSeconds: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        double dMicroSeconds = mxGetScalar( prhs[1] );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::MicroSeconds(dMicroSeconds) ) );
        return;
    }
    // NanoSeconds
    if (!strcmp("NanoSeconds", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2){
            mexErrMsgTxt("NanoSeconds: Unexpected arguments.");
        }

        // Call the method
        // This is a static method which should create a new GnssTimeInterval object
        double dNanoSeconds = mxGetScalar( prhs[1] );
        plhs[0] = convertPtr2Mat< Anise::GnssTimeInterval >( new
                Anise::GnssTimeInterval( Anise::GnssTimeInterval::NanoSeconds(dNanoSeconds) ) );
        return;
    }
    // Check there is a second input, which should be the class instance handle
    if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");

    // Delete
    if (!strcmp("delete", cmd)) {
        // Destroy the C++ object
        destroyObject<Anise::GnssTimeInterval>(prhs[1]);
        // Warn if other commands were ignored
        if (nlhs != 0 || nrhs != 2)
            mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
        return;
    }

    // Get the class instance pointer from the second input
    Anise::GnssTimeInterval *gnss_time_interval_instance = convertMat2Ptr<Anise::GnssTimeInterval>(prhs[1]);


    // Call the various class methods
    // AsWeeks
    if (!strcmp("AsWeeks", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("AsWeeks: Unexpected arguments.");
        // Call the method
        plhs[0] = mxCreateDoubleScalar( gnss_time_interval_instance->AsWeeks() );

        return;
    }
    // AsSeconds
    if (!strcmp("AsSeconds", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("AsSeconds: Unexpected arguments.");
        // Call the method
        plhs[0] = mxCreateDoubleScalar( gnss_time_interval_instance->AsSeconds() );
        return;
    }

    ////////////////// OPERATORS
    // Plus
    if( !strcmp("plus", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("Plus: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pRet = nullptr;
        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        if( pTmp != nullptr ){
            pRet = new Anise::GnssTimeInterval(
                    *gnss_time_interval_instance + *pTmp );
        }

        if( pRet == nullptr ){
            mexErrMsgTxt("Plus: badly formed second argument");
        }

        plhs[0] = convertPtr2Mat( pRet );
        return;
    }
    // Minus
    if( !strcmp("minus", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("Plus: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pRet = nullptr;
        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        if( pTmp != nullptr ){
            pRet = new Anise::GnssTimeInterval(
                    *gnss_time_interval_instance - *pTmp );
        }

        if( pRet == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        plhs[0] = convertPtr2Mat( pRet );
        return;
    }
    // eq
    if( !strcmp("eq", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("eq: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        ret = *gnss_time_interval_instance == *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // ne
    if( !strcmp("ne", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("ne: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        ret = *gnss_time_interval_instance != *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // lt
    if( !strcmp("lt", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("lt: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        ret = *gnss_time_interval_instance < *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // gt
    if( !strcmp("gt", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("gt: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        ret = *gnss_time_interval_instance > *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }

    // Got here, so command not recognized
    mexErrMsgTxt("Command not recognized.");
}
