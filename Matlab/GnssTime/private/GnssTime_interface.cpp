#include "mex.h"
#include "class_handle.hpp"
#include <Anise/GnssTime.h>
#include <iostream>


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the command string
    char cmd[64];
	if (nrhs < 1 || mxGetString(prhs[0], cmd, sizeof(cmd)))
		mexErrMsgTxt("First input should be a command string less than 64 characters long.");

    // New
    if (!strcmp("new", cmd)) {

        if( nlhs < 1 || nrhs > 3 ){
            mexErrMsgTxt("New: incorrect number of arguments ");
        }

        // Check input parameters
        Anise::GnssTime *pNew = nullptr;
        if( nrhs == 1 ){
            pNew = new Anise::GnssTime();
        }
        else if (nrhs >= 2){
            unsigned int theSys = 0;
            if( mxIsNumeric( prhs[1] ) ){
                if( mxIsDouble( prhs[1] ) ){
                    theSys = static_cast< unsigned int >( *mxGetPr( prhs[1] ) );
                }
            }
            else{
                mexErrMsgTxt("New: input argument should be numeric");
            }

            if( nrhs == 3){
                // We should have: arguments:
                // 1) 'new'
                // 2) GNSS System (double)
                // 3) GnssTimeInterval object handle
                Anise::GnssTimeInterval *pTi = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

                pNew = new Anise::GnssTime( theSys, *pTi );
            }
            else{
                pNew = new Anise::GnssTime( theSys );
            }


        }

        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<Anise::GnssTime>(pNew);
        return;
    }
    // copy
    if (!strcmp("copy", cmd)) {

        if( nlhs < 1 || nrhs > 2 ){
            mexErrMsgTxt("copy: incorrect number of arguments ");
        }

        // Check input parameters
        Anise::GnssTime *pNew = nullptr;
        Anise::GnssTime *pOrig = convertMat2Ptr< Anise::GnssTime >( prhs[1] );

        pNew = new Anise::GnssTime( *pOrig );

        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<Anise::GnssTime>(pNew);
        return;
    }

    // Check there is a second input, which should be the class instance handle
    if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");

    // Delete
    if (!strcmp("delete", cmd)) {
        // Destroy the C++ object
        destroyObject<Anise::GnssTime>(prhs[1]);
        // Warn if other commands were ignored
        if (nlhs != 0 || nrhs != 2)
            mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
        return;
    }

    // Get the class instance pointer from the second input
    Anise::GnssTime *gnss_time_instance = convertMat2Ptr<Anise::GnssTime>(prhs[1]);

    // Call the various class methods
    // System
    if (!strcmp("System", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("System: Unexpected arguments.");
        // Call the method
        plhs[0] = mxCreateDoubleScalar( gnss_time_instance->System() );

        return;
    }
    // Week
    if (!strcmp("Week", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Week: Unexpected arguments.");
        // Call the method
        plhs[0] = mxCreateDoubleScalar( gnss_time_instance->Week() );
        return;
    }
    // TOW
    if (!strcmp("TOW", cmd)) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("TOW: Unexpected arguments.");
        // Call the method
        plhs[0] = mxCreateDoubleScalar( gnss_time_instance->TOW() );
        return;
    }

    ////////////////// OPERATORS
    // Plus
    if( !strcmp("plus", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("Plus: Unexpected arguments");
        }

        Anise::GnssTime *pRet = nullptr;
        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        if( pTmp != nullptr ){
            pRet = new Anise::GnssTime(
                    *gnss_time_instance + *pTmp );
        }

        if( pRet == nullptr ){
            mexErrMsgTxt("Plus: badly formed second argument");
        }

        plhs[0] = convertPtr2Mat( pRet );
        return;
    }
    // Minus
    if( !strcmp("minus", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("Plus: Unexpected arguments");
        }

        Anise::GnssTimeInterval *pRet = nullptr;
        Anise::GnssTime *pTmp = convertMat2Ptr< Anise::GnssTime >( prhs[2] );

        if( pTmp != nullptr ){
            pRet = new Anise::GnssTimeInterval(
                    *gnss_time_instance - *pTmp );
        }

        if( pRet == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        plhs[0] = convertPtr2Mat( pRet );
        return;
    }
    // Minus_time_interval
    if( !strcmp("minus_time_interval", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("Plus: Unexpected arguments");
        }

        Anise::GnssTime *pRet = nullptr;
        Anise::GnssTimeInterval *pTmp = convertMat2Ptr< Anise::GnssTimeInterval >( prhs[2] );

        if( pTmp != nullptr ){
            pRet = new Anise::GnssTime(
                    *gnss_time_instance - *pTmp );
        }

        if( pRet == nullptr ){
            mexErrMsgTxt("minus: badly formed second argument");
        }

        plhs[0] = convertPtr2Mat( pRet );
        return;
    }
    // eq
    if( !strcmp("eq", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("eq: Unexpected arguments");
        }

        Anise::GnssTime *pTmp = convertMat2Ptr< Anise::GnssTime >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("eq: badly formed second argument");
        }

        ret = *gnss_time_instance == *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // lt
    if( !strcmp("lt", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("lt: Unexpected arguments");
        }

        Anise::GnssTime *pTmp = convertMat2Ptr< Anise::GnssTime >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("lt: badly formed second argument");
        }

        ret = *gnss_time_instance < *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // gt
    if( !strcmp("gt", cmd)){
        // Check parameters
        if( nlhs < 0 || nrhs < 3 ){
            mexErrMsgTxt("gt: Unexpected arguments");
        }

        Anise::GnssTime *pTmp = convertMat2Ptr< Anise::GnssTime >( prhs[2] );

        bool ret = false;

        if( pTmp == nullptr ){
            mexErrMsgTxt("gt: badly formed second argument");
        }

        ret = *gnss_time_instance > *pTmp;

        plhs[0] = mxCreateLogicalScalar( ret );
        return;
    }
    // Got here, so command not recognized
    mexErrMsgTxt("Command not recognized.");
}
