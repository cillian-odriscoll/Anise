function tRx = TxToRxTime( tSCO, tTx )
%
% TxToRxTime - Converts from transmit to received time frame for a tagged SCO
%
%   tRx = TxToRxTime( tSCO, tTx )
%
%       tSCO - A tagged SCO
%       tTx - a vector of timestamps in the transmit time frame
%
%       tRx - tTx expressed in the SCOs receive time frame


% Check for a valid tSCO

if ~isTaggedSCO( tSCO )
    error( 'Require first argument to be a tagged SCO' );
end

tt = tTx(:);

dtTx = tt - tSCO.Ttx;

tRx = tSCO.Trx + dtTx/( 1 + tSCO.timeDilation );

tRx = reshape( tRx, size( tTx ) );

