function tTx = RxToTxTime( tSCO, tRx )
%
% RxToTxTime - Converts from received to transmitted time frame for a tagged SCO
%
%   tTx = RxToTxtime( tSCO, tRx )
%
%       tSCO - A tagged SCO
%       tRx - a vector of timestamps in the received time frame
%
%       tTx - tRx expressed in the SCOs transmit time frame


% Check for a valid tSCO

if ~isTaggedSCO( tSCO )
    error( 'Require first argument to be a tagged SCO' );
end

tt = tRx(:);

T = tSCO.Trx;

if isa( T, 'GnssTime' )
    T = T.TOW();
end

dtRx = tt - T;

T = tSCO.Ttx;

if isa( T, 'GnssTime' )
    T = T.TOW();
end

tTx = T + dtRx*( 1 + tSCO.timeDilation );

tTx = reshape( tTx, size( tRx ) );
