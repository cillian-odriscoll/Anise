function scoT = TrimSCO( sco, tMin, tMax )
%
% TrimSCO - removes head and tail zero elements from an SCO
%
%    scoT = TrimSCO( sco );
%
%    scoT = TrimSCO( sco, Tmin );
%
%    scoT = TrimSCO( sco, Tmin, TMax );
%
% Reduces the length of a subcarrier object. With one input argument it
% determines the maximum and mininum indices to retain based on the cumulative
% energy. With Tmin and/or Tmax it uses these to determine the earliest/latest
% indices to retain.
%
% 

% (c) 2016 Cillian O'Driscoll

% Exclude points below or above which the maximum value that occurs
% fraction of the overall maximum:
dbMin = -80;

% Compute the cumulative energy as a CDF:
ay = abs( sco.y );

if nargin < 2
    mm = cummax( ay );
    mm = mm/mm(end);
    mmdB = 10*log10( mm );
    firstInd = find( mmdB >= dbMin );
else
    firstInd = find( sco.x >= tMin );
end

if nargin < 3
    omm = fliplr( cummax( fliplr( ay ) ) );
    omm = omm/mm(end);

    ommdB = 10*log10( omm );
    lastInd = find( ommdB <= dbMin, 1 );
else
    lastInd = find( sco.x >= tMax, 1 ) - 1;
end

if isempty( firstInd )
    firstInd = 1;
else
    firstInd = firstInd(1);
end

if isempty( lastInd )
    lastInd = length( sco.y );
else
    lastInd = lastInd(1);
end

scoT = sco;
scoT.x = sco.x(firstInd:lastInd);
scoT.y = sco.y(firstInd:lastInd);



