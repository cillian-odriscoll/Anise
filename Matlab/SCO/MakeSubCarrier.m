function sco = MakeSubCarrier( scGenerator, Tc, fSample, Tmax )

% MakeSubCarrier - Create a subcarrier object
%
% sco = MakeSubCarrier( scGenerator, Tc, fSample)
%   scGenerator - Function object to generate subcarrier samples
%   Tc          - Duration of a single chip
%   fSample     - sampling rate to use
%   Tmax        - Max duration of SCO (extras padded with zeros) defaults to 1.5*Tc
%
% Example:
%   fo = 1.023e6;
%   fs = 6*fo;
%   fc = fo;
%   rho = 1/11;
%   fSample = 300*pi*fo;
%
%   e1bSco = MakeSubCarrier( @(t)CBocSubCarrier( t, fs, fc, rho ), ...
%       1/fc, fSample );
%
%   figure;
%   plot( e1bSco.x*fc, e1bSco.y );
%   xlabel( 'Code phase [chips]' );
%   ylabel( 'Amplitude' );
%   grid on; hold all;
%   xlim( [-0.5, 1.5] );
%   title( 'Galileo E1b Subcarrier' );

% (c) 2016 Cillian O'Driscoll

if nargin < 4
    Tmax = 1.5*Tc;
end

x = -Tc/2:1/fSample:Tmax;
y = scGenerator( x );

sco = struct( 'fSample', fSample, ...
    'x', x, ...
    'y', y, ...
    'fc', 1/Tc, ...
    'scaleFactor', sqrt( 1.0/Tc/fSample ));

sco = NormaliseSCO( sco );
