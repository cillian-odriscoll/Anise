function Hfo = FilterToHfo( B, A, fSample, fMax )

% FilterToHfo - converts a filter to a frequency domain object representation
%
%   Hfo = FilterToHfo( B, A, fSample, fMax )
%
%   B - Filter B coefficients
%   A - Filter A coefficients
%   fSample - Sampling rate
%   fMax - Maximum frequency

% (c) 2016 Cillian O'Driscoll

if nargin < 4 || isempty( fMax )
    fMax = fSample/2;
end

dfMax = 10e3;

N = ceil( 2*fMax/dfMax );
ff = linspace( -fMax, fMax, N );

[ H, f ] = freqz( B, A, ff, fSample );

Hfo.x = f;
Hfo.y = H;
Hfo.fSample = 1/diff(f(1:2)) ;
Hfo.scaleFactor = 1;
