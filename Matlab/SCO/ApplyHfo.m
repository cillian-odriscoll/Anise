function scoOut = ApplyHfo( sco, Hfo )
%
% ApplyHfo - Applies a freqeuncy domain transformation to an SCO
%
%  socOut = ApplyHfo( sco, Hfo )
%
%   sco - A subcarrier object to be filtered
%   Hfo - A frequency domain filter object to apply to sco
%

% (c) 2016 Cillian O'Driscoll

fo = FftSCO( sco );

foOut = DotMultiplySCO( fo, Hfo );

scoOut = IFftSCO( foOut );

scoOut.x = scoOut.x + sco.x(1);
scoOut.y = scoOut.y/sco.scaleFactor;
scoOut.scaleFactor = sco.scaleFactor;

