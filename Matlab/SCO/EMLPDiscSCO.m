function eml = EMLPDiscSCO( sco, d, scaleFactor )

% EMLPDiscSCO - Early minus Late Power Discriminator for subcarrier objects
%
% eml = EMLPDiscSCO( sco, d, [scaleFactor] )
%   sco - Subcarrier object for which to compute the discriminator output
%   d   - Early - Late spacing in seconds
%   scaleFactor - Optional - normalisation factor to yield a slope of 1
%
%   Computes a new sco with each x element given by
%   eml(i) = ( abs( sco.x(i - d/2 ) ) - abs( sco.x(i+d/2) ) )/( abs( sco.x(i - d/2 ) ) + abs( sco.x(i+d/2) ) )*scaleFactor;

% (c) 2016 Cillian O'Driscoll

eml = sco;

if nargin < 3
    scaleFactor = 1;
end

tauEarly = sco.x - d/2;
tauLate  = sco.x + d/2;
E = abs( ValueAt( sco, tauEarly ) );
L = abs( ValueAt( sco, tauLate ) ) ;
eml.y = ( E - L )./( E+L) * scaleFactor;
