function q = isTaggedSCO( sco )
%
% isTaggedSCO - Determine if an object is a valid Tagged SCO
%
%   isTaggedSCO( sco )
%   returns true if sco is a valid tagged subcarrier object, false otherwise.
%
%
% See also:
%   MakeSubCarrier, TagSCO

tScoFields = { 'Ttx', 'Trx', 'timeDilation', 'phi0', 'phiDot' };

q= isSCO( sco ) && all( isfield( sco, tScoFields ) );

