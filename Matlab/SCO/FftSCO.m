function fo = FftSCO( sco, dfMax )

% FftSCO - Compute the FFT of a sub-carrier object
%
%   fo = FftSCO( sco )
%
%   fo = FftSCO( sco, dfMax )
%   Do the FFT but pad with zeros to ensure that the frequency resolution is at
%   least dfMax

% (c) 2016 Cillian O'Driscoll

if nargin < 2
    dfMax = 100e3;
end

fo = sco;

N = length( sco.y );

T = length(sco.x)/sco.fSample;
df = 1/T;

F = sco.fSample;

if( df > dfMax )
    N = ceil(F/dfMax);
    df = F/N;
end



fo.y = fft( sco.y, N );
N = length( fo.y );


no2 = floor(N/2);
ff = ( (0:(N-1)) - no2 )*df;

fo.x= fftshift( ff );
fo.fSample = 1/df;
fo.scaleFactor = 1/( length(ff) )*sco.scaleFactor;
fo.y = fo.y*fo.scaleFactor;
fo.scaleFactor=1;

%fo.y( abs(fo.x) > sco.fSample/2 ) = 0;
