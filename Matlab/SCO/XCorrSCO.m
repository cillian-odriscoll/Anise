function ro = XCorrSCO( scoX, scoY )

% XCorrSCO - Compute cross correlation of SCO objects
%
% ro = XCorrSCO( scoX, scoY )
%   scoX - first input subcarrier object
%   scoY - second input subcarrier object (optional);

% (c) 2016 Cillian O'Driscoll

ro = scoX;
dt = 0;

if nargin < 2
    [ro.y, ro.x] = xcorr( scoX.y );
    scaleFactor = scoX.scaleFactor.^2;
    Neff = scoX.fSample/scoX.fc;
else
    if scoX.fSample == scoY.fSample
        [ro.y, ro.x] = xcorr( scoX.y, scoY.y );
    else
        yy = ValueAt( scoY, scoX.x );
        [ro.y, ro.x ] = xcorr( scoX.y, yy );
    end
    scaleFactor = scoX.scaleFactor*scoY.scaleFactor;
    dt = scoY.x(1) - scoX.x(1);
    Neff = sqrt( scoX.fSample/scoX.fc * scoY.fSample/scoY.fc );
end

%%%%%%Ts = diff( scoX.x(1:2) );

ro.x = ro.x/ro.fSample - dt;
ro.y = ro.y*scaleFactor/Neff; %*Ts;
ro.scaleFactor = 1.0;
