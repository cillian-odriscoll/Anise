function q = isSCO( sco )
%
% isSCO - Determine if an object is a valid SCO
%
%   isSCO( sco )
%   returns true if sco is a valid subcarrier object, false otherwise.
%

scoFields = { 'fSample', 'x', 'y', 'fc', 'scaleFactor' };

q= all( isfield( sco, scoFields ) );
