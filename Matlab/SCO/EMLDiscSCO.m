function eml = EMLDiscSCO( sco, d, scaleFactor )

% EMLDiscSCO - Early minus Late Discriminator for subcarrier objects
%
% eml = EMLDiscSCO( sco, d, [scaleFactor] )
%   sco - Subcarrier object for which to compute the discriminator output
%   d   - Early - Late spacing in seconds
%   scaleFactor - Optional - normalisation factor to yield a slope of 1
%
%   Computes a new sco with each x element given by
%   eml(i) = ( sco.y(i - d/2 ) - sco.y(i+d/2) )/( sco.y(i - d/2 ) + sco.y(i+d/2) )*scaleFactor;

% (c) 2016 Cillian O'Driscoll

eml = sco;

if nargin < 3
    scaleFactor = 1;
end

tauEarly = sco.x - d/2;
tauLate  = sco.x + d/2;

E = ValueAt( sco, tauEarly );
L = ValueAt( sco, tauLate ) ;
eml.y = ( E - L )./(E+L) * scaleFactor;
