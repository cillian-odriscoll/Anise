function areCompat = SCOsAreCompatible( scoX, scoY )

% SCOsAreCompatible - Check if two SCOs are compatible (can be added directly)
%
%   areCompat = SCOsAreCompatible( scoX, scoY );
%       scoX - first input subcarrier object
%       scoY - second input subcarrier object
%
%   Returns true if the SCOs have the same x values

% (c) 2016 Cillian O'Driscoll

areCompat = ( length( scoY.x ) == length( scoX.x ) && ...
    scoY.x(1) == scoX.x(1) && scoY.fSample == scoX.fSample );



