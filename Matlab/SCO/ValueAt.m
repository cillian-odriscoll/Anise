function Y = ValueAt( sco, XI, method )

% ValueAt - Computes values of a subcarrier object at an array of timepoints
%
% Y = ValueAt( sco, XI[, method] )
%    sco - Subcarrier object to be evaluated
%    XI  - points at which to evaluate the subcarrier
%    method - Optional - string describing the interpolation method.
%
% Interpolation is performed using interp1, see help on this function for
% valid method strings.
%
% Default interpolation method is linear. For infinite bandwidth BOC signals
% it is better to use the 'previous' interpolation method - effectively sample
% and hold interpolation.

% (c) 2016 Cillian O'Driscoll


if nargin < 3
    method = '*linear';
end

Y = interp1( sco.x, sco.y, XI, method, 0 )*sco.scaleFactor;
