function scoProd = DotMultiplySCO( scoX, scoY )

% DotMultiplySCO - Component by componet multiplication of SCO objects
%
%   scoProd = DotMultiplySCO( scoX, scoY );
%       scoX - first input subcarrier object
%       scoY - second input subcarrier object
%
%   computes scoProd.y == scoX.y .* scoY.y;

% (c) 2016 Cillian O'Driscoll

scoProd = scoX;

if( SCOsAreCompatible( scoX, scoY ) )
    scoProd.y = scoX.y .* scoY.y;

    scoProd.scaleFactor = ( scoX.scaleFactor * scoY.scaleFactor );
else
    scoProd.y = scoX.y .* ValueAt( scoY, scoX.x);
end


