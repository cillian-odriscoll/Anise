function sco = IFftSCO( fo )

% IFftSCO - Inverse fourier transform of sub-carrier object
%
%   sco = IFftSCO( fo )
%
%    fo - A frequency domain object
%   sco - The inverse Fourier transform of fo.  

% (c) 2016 Cillian O'Driscoll

sco = fo;

N = length( fo.y );

sco.y = ifft( fo.y );

F = length(fo.x)/fo.fSample;

sco.fSample = F;

sco.scaleFactor=fo.scaleFactor*( N );
sco.y = sco.y*sco.scaleFactor;
sco.scaleFactor = 1;

% no2 = floor(N/2);
sco.x = ( (0:(N-1)) )/sco.fSample;
