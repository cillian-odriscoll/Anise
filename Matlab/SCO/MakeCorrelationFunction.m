function sco = MakeCorrelationFunction( corrGenerator, Tc, fSample )

% MakeCorrelationFunction - Create a correlation function object
%
% sco = MakeCorrelationFunction( corrGenerator, Tc, fSample)
%   corrGenerator - Function object to generate correlation function samples
%   Tc          - Duration of a single chip
%   fSample     - sampling rate to use
%
% Example:
%   fo = 1.023e6;
%   fs = fo;
%   fc = fo;
%   fSample = 300*pi*fo;
%
%   boc11Ro = MakeCorrelationFunction( @(t)BocSinCorrelationFunction( t, fs, fc ), ...
%       1/fc, fSample );
%
%   figure;
%   plot( boc11Ro.x*fc, boc11Ro.y );
%   xlabel( 'Code delay [chips]' );
%   ylabel( 'Correlation' );
%   grid on; hold all;
%   xlim( [-1.5, 1.5] );
%   title( 'BOC(1,1) Correlation Function' );

% (c) 2016 Cillian O'Driscoll

N = ceil(1.5*Tc*fSample);

x = (-N:N)/fSample;
y = corrGenerator( x );

sco = struct( 'fSample', fSample, ...
    'x', x, ...
    'y', y, ...
    'scaleFactor', 1.0 );
