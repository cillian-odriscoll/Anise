function fsco = FilterSCO( B, A, sco )

% FilterSCO - Filter a subcarrier object
%
% fsco = FilterSCO( B, A, sco );
%
%    B - Filter numerator
%    A - Filter denominator
%   sco - Subcarrier object to filter.
%
% see filter.

% (c) 2016 Cillian O'Driscoll

fsco = sco;

fsco.y = filter(B,A,sco.y);

