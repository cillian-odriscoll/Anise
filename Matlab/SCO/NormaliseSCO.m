function scoNorm = NormaliseSCO( sco )
%
% NormaliseSCO - sets the scale factor to ensure unit energy in the SCO
%
%   scoNorm = NormaliseSCO( sco )
%

% (c) 2016 Cillian O'Driscoll



en = sum( abs( sco.y ).^2 );
enDesired = sco.fSample/sco.fc;

scoNorm = sco;
scoNorm.scaleFactor = sqrt( enDesired/en );


end

