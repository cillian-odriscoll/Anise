function scoSum = AddSCO( scoX, scoY )

% AddSCO - Component by componet addition of SCO objects
%
%   scoSum = AddSCO( scoX, scoY );
%       scoX - first input subcarrier object
%       scoY - second input subcarrier object
%
%   computes scoSum == scoX.y + scoY.y;

% (c) 2016 Cillian O'Driscoll

scoSum = scoX;

if( SCOsAreCompatible(scoX, scoY) )
    scoSum.y = scoX.y * scoX.scaleFactor + scoY.y * scoY.scaleFactor;
else
    scoSum.y = scoX.y * scoX.scaleFactor + ValueAt( scoY, scoX.x );
end

scoSum.scaleFactor = sqrt( 0.5*( scoX.scaleFactor^2 + scoY.scaleFactor^2 ) );
scoSum.y = scoSum.y / scoSum.scaleFactor;


