function Psco = ComputeSCOPower( sco )
%
% ComputeSCOPower - Calculates the power in a subcarrier object
%
%   P = ComputeSCOPower( sco )
%       sco - the subcarrier object for which to compute the power
%       P - The power in Watts
%
% A subcarrier object is an energy signal, not a power signal, however, the SCO
% objects in Anise form the basis for signal generation. This function tells us
% the total power in the resultant signal generated from this subcarrier object

% (c) 2016 Cillian O'Driscoll

% We first compute the total energy in the SCO
Esco = sum( abs( sco.y*sco.scaleFactor ).^2 );

% To normalise we need to know the 'effective' number of samples:
Neff = sco.fSample/sco.fc;

Psco = Esco/Neff;
