function taggedSCO = TagSCO( sco, Ttx, Trx, timeDilation, phi0, phiDot )
%
% TagSCO - Tag a subcarrier object with extra timing information
%
%   taggedSCO = TagSCO( sco, Ttx )
%
%       Tags an SCO with a transmit time
%
%   taggedSCO = TagSCO( sco, Ttx, Trx )
%       Tags an SCO with a transmit time and a received time
%
%   taggedSCO = TagSCO( sco, Ttx, Trx, timeDilation )
%       Tags an SCO with a transmit time, a received time and a time dilation coefficient
%
%   taggedSCO = TagSCO( sco, Ttx, Trx, timeDilation, phi0 )
%       Tags an SCO with a transmit time, a received time, time dilation coefficient
%       and an initial phase offset
%
%   taggedSCO = TagSCO( sco, Ttx, Trx, timeDilation, phi0, phiDot )
%       Tags an SCO with a transmit time, a received time, time dilation coefficient,
%       an initial phase offset and a frequency offset
%
%
%   Note the time dilation coefficient is a mapping from the transmit time frame
%   to the receiver time frame:
%
%   t^rx( t^tx ) = Trx + ( t^tx - Ttx )/( 1 + timeDilation )

% (c) 2016 Cillian O'Driscoll

taggedSCO = sco;

if nargin < 6
    phiDot = 0;
end

if nargin < 5
    phi0 = 0;
end

if nargin < 4
    timeDilation = 0;
end

if nargin < 2
    Ttx = 0;
end

if nargin < 3
    Trx = Ttx;
end

if nargin < 1
    error( 'Need at least one input argument' );
end

taggedSCO.('Ttx') = Ttx;
taggedSCO.('Trx') = Trx;
taggedSCO.('timeDilation') = timeDilation;
taggedSCO.('phi0') = phi0;
taggedSCO.('phiDot') = phiDot;


