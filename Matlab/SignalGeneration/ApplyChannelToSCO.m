function [ ifSig, rxSco, nextTxSco, channelOut, chipGeneratorOut ] = ...
    ApplyChannelToSCO( txSco, channelIn, tRx, fSample, chipGenerator, tickPeriod, prevRxSco )
%
% ApplyChannelToSCO - Generates IF signal applying channel to SCO
%
% [ ifSig, rxSco, nextTxSco, channelOut, chipGeneratorOut ] = 
%    ApplyChannelToSCO( txSco, channelIn, tRx, fSample, chipGenerator, tickPeriod, prevRxSco )
%
%   txSco - The subcarrier object generated at the transmitter
%   channelIn - The SimulationChannel object ot apply to txSco
%   tRx - The receiver time of the first sample to be generated (either scalar or GNSSTime object)
%   fSample - The IF signal sampling rate
%   chipGenerator - The ChipGenerator object for generating the transmitted
%           chips
%   tickPeriod - The interval between ticks in the satellite time frame [s]
%   prevRxSco - The previous output of the SimulationChannel [optional]
%
%   ifSig - The baseband analytic IF signal with samples in the range:
%       tRx:1/fSample:rxSco.Trx
%   rxSco - The subcarrier object obtained by applying channelIn to txSco
%   nextTxSco - txSco propagated by on tick period
%   channelOut - The simulation channel to be applied to nextTxSco
%   chipGeneratorOut - The ChipGenerator to be applied in the next iteration
%
%   This performs the following operations:
%       1) Applies the channelIn to txSco to obtain channelOut and rxSco
%       2) Computes the sample time instants in the range:
%           t \in [ max( prevRxSco.Trx, tRx), rxSco.Trx )
%       3) If this set is not empty then call GenerateSignalFromTaggedSCOs to
%       compute the IF signal and update the chipGenerator object
%
%   Note that the channel and chip generator objects both have state that may be
%   updated during propagation, hence channelIn/channelOut and
%   chipGenerator/chipGeneratorOut

% (c) 2016 Cillian O'Driscoll

doGenIfSig = 0;


ifSig = [];
[ rxSco, channelOut ] = apply( channelIn, txSco );
chipGeneratorOut = chipGenerator;

rxSco = TrimSCO( rxSco, 0, txSco.x(end) );

if nargin >= 7 && ~isempty( prevRxSco )
    %doGenIfSig = 1;
    doGenIfSig = ( tRx(1) >= prevRxSco.Trx && tRx(end) <= rxSco.Trx );
    if ~doGenIfSig
        dt = tRx - rxSco.Trx;
        disp( [ 'Skipping generation: time left = ' num2str( dt.AsSeconds() ) ] );
    end
end

if doGenIfSig
    t0Rel = ( tRx(1) - prevRxSco.Trx );
    tEndRel = rxSco.Trx - prevRxSco.Trx;

    if isa( t0Rel, 'GnssTimeInterval' )
        t0Rel = t0Rel.AsSeconds();
    end

    if isa( tEndRel, 'GnssTimeInterval' )
        tEndRel = tEndRel.AsSeconds();
    end

    if tEndRel < t0Rel
        tEndRel = tEndRel + 604800;
    end

    ttRel = t0Rel:(1/fSample):tEndRel;

    [ ifSig, chipGeneratorOut ] = GenerateSignalFromTaggedSCOs_Conv( ...
        prevRxSco, rxSco, ttRel, chipGenerator, 'mex' );

end

nextTxSco = PropagateTaggedSCO( txSco, tickPeriod );


