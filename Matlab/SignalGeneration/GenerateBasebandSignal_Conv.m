function [ bbSig, chipGenOut ] = GenerateBasebandSignal_Conv( tSCO1, tt, chipGen, method )
%
% GenerateSignalFromTaggedSCOs - IF signal generation from tagged subcarrier objects
%
%   [theSig, chipGenOut] = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, chipGen )
%
%   tSCO1 - A tagged SCO with receiver time <= tt(1)
%   tSCO2 - A tagged SCO with receiver time >= tt(end)
%   tt - The receiver times at which to generate the signal
%   chipGen - A chip generator object
%
%   theSig - the generated IF signal
%   chipGenOut - Updated chip generator object
%

% (c) 2016 Cillian O'Driscoll

if ~ isTaggedSCO( tSCO1 )
    error( 'First argument must be a tagged sco' );
end

if ~isa( chipGen, 'ChipGenerator' )
    error( 'Third argument must be a chip generator' );
end

if nargin < 4 || isempty( method )
    method = 'vec';
end


% Store the chip rate for future use
fc = tSCO1.fc;

% How many 'previous' chips do we need
NPrevChips = ceil( tSCO1.x(end)*fc ) - 1;

% Store the receive time of the first SCO
if isa( tSCO1.Trx, 'GnssTime' )
    t0Secs1 = tSCO1.Trx.TOW();
else
    t0Secs1 = tSCO1.Trx;
end

ttRx = t0Secs1 + tt;
%dtRx = tt;

% Get the transmit time of the first and last chips for the first and last scos
tTx1 = RxToTxTime( tSCO1, ttRx );


%NChips1 = ceil( (tTx1(end) - tTx1(1))*fc );
NChips1 = floor( tTx1(end)*fc ) - floor( tTx1(1)*fc ) + 1;

Tc = 1/fc;
Tchip1 = (floor( tTx1(1)*fc) - NPrevChips)*Tc;


Ts = tt(2)-tt(1);% fs = 1/Ts;
chipGenOut = chipGen;


% Generate the chips for the first SCO
TchipS = Tchip1;
if isa( TchipS, 'GnssTime' )
    TchipS = TchipS.TOW();
end

TtChips = TchipS + (0:(NChips1 + NPrevChips ))*Tc + 0.5*Tc;
[theChips1, chipGenOut ] = generate( chipGenOut, TtChips );


%bbSig = zeros( size( tt ) );
K = length(tt);

% tau is the transmit code phase in SCO samples
% dTau is the change in tau per output (baseband)  sample
dTau = tSCO1.fSample*(1+tSCO1.timeDilation)*Ts;

tau0 = tTx1(1);
tau0 = ( tau0 - floor( tau0/Tc)*Tc) *tSCO1.fSample;

tauChip = Tc*tSCO1.fSample;
bbSig = ConvChips( K, theChips1, tSCO1.y*tSCO1.scaleFactor, tauChip, dTau, tau0, ...
    method);

