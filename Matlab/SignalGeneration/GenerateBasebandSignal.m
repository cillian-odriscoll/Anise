function [bbSig, chipGenOut] = GenerateBasebandSignal( sco, t, chipGen )
%%
% GenerateBasebandSignal -  Generate an baseband signal from a tagged  SCO
%
%   [bbSig, state] = GenerateBasebandSignal( sco, t, chipGen )
%
%       sco - The subcarrier object for the signal (tagged)
%       t   - vector of receiver time values over which to compute the IF signal
%       chipGen - A chip generator object
%
% NOTE: THIS FUNCTION IS OBSOLETE, WILL BE DELETED
% USE: GenerateSignalFromTaggedSCOs instead.

% (c) 2016 Cillian O'Driscoll

if ~ isTaggedSCO( sco )
    error( 'First argument must be a tagged sco' );
end

if ~isa( chipGen, 'ChipGenerator' )
    error( 'Third argument must be a chip generator' );
end

fSampleOut = 1/(t(2) - t(1));
Ts = 1/fSampleOut;

fc = sco.fc; Tc = 1/fc;


tRx = t;
N = length(tRx);
tTx = RxToTxTime( sco, sco.Trx + t );
T = fSampleOut * N;

%NChips = ceil( (tTx(end)-tTx(1))*sco.fc );
NChips = floor( tTx(end)*fc ) - floor( tTx(1)*fc ) + 1;
NChipsPrev = ceil( sco.x(end)*fc ) - 1;

T0Chip = (floor( tTx(1)*sco.fc ) - NChipsPrev )/sco.fc;

allChipT = T0Chip + ( (0:(NChips+NChipsPrev)) )/sco.fc + 0.5/sco.fc;
[ theChips, chipGenOut ] = generate( chipGen, allChipT );


bbSig = zeros( size(tRx) );
%hold off;
%Compute the contribution of each chip:
for chipInd = 1:length(theChips)

    ctrb = ValueAt( sco, tTx - T0Chip, '*spline' )*theChips(chipInd);
    bbSig = bbSig + ctrb;

    T0Chip = T0Chip + Tc;

end
% Update state:
%state.t0 = state.t0 + T;

%% Testing in Octave:
%!test
%!shared t, bbSig, state, sco, fc, fSample, theChips, fSampleOut
%! state = struct( 't0', 0 );
%! bbSig = [];
%! fc = 1.023e6;
%! fSample = 10*pi*fc;
%! sco = MakeSubCarrier( @(t)BpskSubCarrier(t, fc), 1/fc, fSample );
%! theChips = [1, -1];
%! fSampleOut = 4e6;
%! t = 0:(1/fSampleOut):(1.5/fc);
%! assert( length(t), 6 )
%! [bbSig, state] = GenerateBasebandSignal( sco, t, theChips, 0, fc, state );
%! assert( length(bbSig), length(t) )
%!test
%! state = struct( 't0', 0 );
%! [bbSig1, state] = GenerateBasebandSignal( sco, t(1:4), theChips, 0, fc, state );
%! assert( bbSig1, bbSig(1:4) )
%! [bbSig2, state] = GenerateBasebandSignal( sco, t(5:6), theChips, 0, fc, state);
%! assert( bbSig2, bbSig(5:6) )
