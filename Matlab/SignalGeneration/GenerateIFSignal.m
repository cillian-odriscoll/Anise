function [ifSig, state] = GenerateIFSignal( sco, t, theChips, T0Chip, fc, freq, state )
%%
% GenerateIFSignal - Generate an Intermediate Frequency signal from an SCO
%
%   [ifSig, state] = GenerateIFSignal( sco, t, theChips, T0Chip, fc, freq, state )
%
%       sco - The subcarrier object for the signal
%       t   - vector of receiver time values over which to compute the IF signal
%       theChips - Vector of chips to modulate the subcarrier
%       T0Chip - The transmit time of the first chip in theChips
%       fc - The chipping rate
%       freq - The signal frequency (IF + Doppler)
%       state - A struct with elements:
%               1. t0 - the current reciever time at the first sample generated
%               2. phi0 - the IF phase of the first sample generated
%               3. phiDot - the rate of change of the IF phase at the first
%                  sample generated
%
% NOTE: THIS FUNCTION IS OBSOLETE, WILL BE DELETED
% USE: GenerateSignalFromTaggedSCOs instead.

% (c) 2016 Cillian O'Driscoll

fSampleOut = 1/(t(2) - t(1));
Ts = 1/fSampleOut;

if nargin < 7
    state = struct( 't0', 0, 'phi0', 0, 'phiDot', 2*pi*freq );
end

tRx = t;
N = length(tRx);
tTx = T0Chip;
T = fSampleOut * N;


[ ifSig, state ] = GenerateBasebandSignal( sco, t, theChips, T0Chip, fc, state );

% Modulate with the carrier:
carr = exp(1j*( state.phi0 + (0:(N-1))*state.phiDot*Ts) );
ifSig = ifSig .* carr;

% Update state:
state.phi0 = state.phi0 + state.phiDot*T;

