function [ acgn, nextState ] = GenerateFilteredNoise( No, N, b, a, prevState, cmplx )
%
% GenerateFilteredNoise - Create additive coloured Gaussian noise
%
%   [acgn, nextState] = GenerateFilteredNoise( No, N, b, a[, preState[, cmplx] ] );
%
%       No - Noise spectral density in dB W/sample
%       N - number of points to generate
%       b - Filter b coefficients to use
%       a - Filter a coefficients to use
%       prevState - Previous filter state [optional]
%       cmplx - Indicator to generate complex outputs [optional]
%
%       acgn - N real or complex samples of additive coloured Gaussian noise
%       nextState - Updated state vector for filtering
%

% (c) 2016 Cillian O'Driscoll

if nargin < 3
    error( 'Insufficent arguments' );
end

if nargin < 5
    cmplx = 0;
end

if nargin < 4 || isempty( prevState )
    prevState = zeros( max( [length(b), length(a)] ) -1, 1 );
end

gg = sqrt( 10^(0.1*No) );

if cmplx
    awgn = randn( N, 2 )*[1;1j]/sqrt(2);
else
    awgn = randn( N, 1 );
end

[ acgn, nextState ] = filter( b, a, gg*awgn, prevState );
