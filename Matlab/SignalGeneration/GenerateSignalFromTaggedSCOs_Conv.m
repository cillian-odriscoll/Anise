function [ theSig, chipGenOut ] = GenerateSignalFromTaggedSCOs_Conv( tSCO1, tSCO2, tt, chipGen, method )
%
% GenerateSignalFromTaggedSCOs - IF signal generation from tagged subcarrier objects
%
%   [theSig, chipGenOut] = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, chipGen )
%
%   tSCO1 - A tagged SCO with receiver time <= tt(1)
%   tSCO2 - A tagged SCO with receiver time >= tt(end)
%   tt - The receiver times at which to generate the signal
%   chipGen - A chip generator object
%
%   theSig - the generated IF signal
%   chipGenOut - Updated chip generator object
%

% (c) 2016 Cillian O'Driscoll

if nargin < 5
    method = 'mex';
end

if  tt(1) < 0  %tSCO1.Trx > tt(1)
    error( 'The first reciever time stamp is earlier than the first tagged SCO' );
end

T_TX = tSCO2.Ttx - tSCO1.Ttx;

if isa( T_TX, 'GnssTimeInterval' )
    T_TX = T_TX.AsSeconds();
end

if T_TX < 0
    T_TX = T_TX + 604800;
end

T_RX = tSCO2.Trx - tSCO1.Trx;

if isa( T_RX, 'GnssTimeInterval' )
    T_RX = T_RX.AsSeconds();
end

if T_RX < 0
    T_RX = T_RX + 604800;
end

if tt(end) >= T_RX % tSCO2.Trx < tt(end)
    error( 'The last receiver time stamp is later than the second tagged SCO' );
end


% Store the chip rate for future use
fc = tSCO1.fc;

dtRx = tt;
dtRx2 = dtRx - T_RX;

eta = ( 1 - tt/( T_RX ) );

chipGenOut = chipGen;
[ ctrb1, chipGenOut ] = GenerateBasebandSignal_Conv( tSCO1, dtRx, chipGenOut, method );

[ ctrb2, chipGenOut ] = GenerateBasebandSignal_Conv( tSCO2, dtRx2, chipGenOut, method );
bbSig = ctrb1(:) .* eta(:) + ctrb2(:).*(1-eta(:));

% Do a linear interpolation of the phase:
phi = eta.*(tSCO1.phi0 + tSCO1.phiDot*(1+ tSCO1.timeDilation)*dtRx) + ...
    (1-eta).*(tSCO2.phi0 + tSCO2.phiDot*(1+tSCO2.timeDilation)*dtRx2);
carr = exp(1j*phi);
theSig = bbSig(:) .* carr(:);
theSig = theSig.';




%% Testing in Octave:
%!test
%!shared t, bbSig, tSCO1, tSCO2, fc, sco, fSample, theChips, fso, Ttx0, Trx0
%! fc = 1.023e6;
%! fso = 8e6;
%! fSample = 100*pi*fc;
%! theChips = 1- 2 * l1ca(1);
%! sco = MakeSubCarrier( @(t)BpskSubCarrier(t, fc), 2/fc, fSample );
%! [b, a] = butter( 8, (fso*0.4)/(fSample/2) );
%! sco = FilterSCO( b, a, sco );
%! figure;
%! plot( sco.x*fc, sco.y )
%! Ttx0 = 0;
%! Trx0 = 0.0001;
%! T = 0.0001;
%! tt = Trx0 + ( 0:(1/fso):T );
%! fDoppler = 2e3;
%! fRf = 1540*fc;
%! timeDilation = fDoppler/fRf;
%! phi0 = 0;
%! phiDot = fDoppler*2*pi;
%! tSCO1 = TagSCO( sco, Ttx0, Trx0, timeDilation, phi0, phiDot );
%! Tsco = 1e-3;
%! phi0 = phi0 + phiDot*Tsco;
%! tSCO2 = TagSCO( sco, Ttx0 + Tsco, Trx0 + Tsco*(1+timeDilation), timeDilation, phi0, phiDot);
%! tic;
%! theSig = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, theChips, fc );
%! toc;
%! assert( length(theSig), length(tt) );
%! figure;
%! plot( tt*fc, [ real(theSig); imag(theSig) ], '.-' );

