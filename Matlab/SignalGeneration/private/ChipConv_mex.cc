#include "mex.h"

#include <Anise/ChipConv.h>
#include <complex>
#include <vector>

// Usage: 
// function x = ConvChips( K, chips, chipShape, tauChip, dTau, tau0, method )

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *chip_shape_r, *chip_shape_i, *the_chips_r, *the_chips_i,
           *bb_sig_r, *bb_sig_i;

    double tauChip, dTau, tau0;

    size_t len_chip_shape, len_chips, len_bb_sig;

    // Check for 6 arguments:
    if( nrhs != 6 )
    {
        mexErrMsgTxt( "Expected six input arguments");
    }

    if( nlhs > 1 )
    {
        mexErrMsgTxt( "Too many output arguments" );
    }

    len_bb_sig = mxGetScalar( prhs[0] );
    the_chips_r = mxGetPr( prhs[1] );
    the_chips_i = mxGetPi( prhs[1] );
    len_chips = mxGetNumberOfElements( prhs[1] );
    chip_shape_r = mxGetPr( prhs[2] );
    chip_shape_i = mxGetPi( prhs[2] );
    len_chip_shape = mxGetNumberOfElements( prhs[2] );

    tauChip = mxGetScalar( prhs[3] );
    dTau = mxGetScalar( prhs[4] );
    tau0 = mxGetScalar( prhs[5] );


    bool chip_shape_is_real = chip_shape_i == nullptr;
    bool the_chips_are_real = the_chips_i == nullptr;
    bool bb_sig_is_real = chip_shape_is_real && the_chips_are_real;

    int fn_id = chip_shape_is_real*2 + the_chips_are_real;
    std::vector< std::complex< double > > bb_sig_vec( len_bb_sig, 0.0 );
    std::vector< double > bb_sig_vec_r( len_bb_sig, 0.0 );

    try
    {
        if( bb_sig_is_real )
        {
            Anise::chip_conv_rrr_fxpt64( chip_shape_r, len_chip_shape,
                    the_chips_r, len_chips,
                    tau0, tauChip, dTau,
                    &bb_sig_vec_r[0], len_bb_sig );
        }
        else{

            std::vector< std::complex< double > > chip_shape_vec;
            std::vector< std::complex< double > > the_chips_vec;

            if( !the_chips_are_real )
            {
                the_chips_vec.resize( len_chips );
                for( unsigned int ii=0; ii < len_chips; ++ii )
                {
                    the_chips_vec[ii] = std::complex< double >( 
                            the_chips_r[ii], the_chips_i[ii] );
                }
            }

            if( !chip_shape_is_real )
            {
                chip_shape_vec.resize( len_chip_shape );
                for( unsigned int ii=0; ii < len_chip_shape; ++ii )
                {
                    chip_shape_vec[ii] = std::complex< double >(
                            chip_shape_r[ii], chip_shape_i[ii] );
                }
            }

            switch( fn_id )
            {
                case 0: // ccc
                Anise::chip_conv_ccc_fxpt64( &chip_shape_vec[0], len_chip_shape,
                        &the_chips_vec[0], len_chips,
                        tau0, tauChip, dTau,
                        &bb_sig_vec[0], len_bb_sig );
                break;

                case 1: // crc
                Anise::chip_conv_crc_fxpt64( &chip_shape_vec[0], len_chip_shape,
                        the_chips_r, len_chips,
                        tau0, tauChip, dTau,
                        &bb_sig_vec[0], len_bb_sig );
                break;

                case 2: // rcc
                Anise::chip_conv_rcc_fxpt64( chip_shape_r, len_chip_shape,
                        &the_chips_vec[0], len_chips,
                        tau0, tauChip, dTau,
                        &bb_sig_vec[0], len_bb_sig );
                break;
            }

        }
    }
    catch( std::exception &e )
    {
        mexErrMsgTxt( e.what() );
    }


    if( bb_sig_is_real )
    {
        plhs[0] = mxCreateDoubleMatrix( (mwSize)len_bb_sig, 1, mxREAL);
        
        bb_sig_r = mxGetPr( plhs[0] );

        for( unsigned int i = 0; i < len_bb_sig; ++i )
        {
            bb_sig_r[i] = bb_sig_vec_r[i];
        }

    }
    else
    {
        plhs[0] = mxCreateDoubleMatrix( (mwSize)len_bb_sig, 1, mxCOMPLEX);
        
        bb_sig_r = mxGetPr( plhs[0] );
        bb_sig_i = mxGetPi( plhs[0] );

        for( unsigned int i = 0; i < len_bb_sig; ++i )
        {
            bb_sig_r[i] = bb_sig_vec[i].real();
            bb_sig_i[i] = bb_sig_vec[i].imag();
        }
    }

}
