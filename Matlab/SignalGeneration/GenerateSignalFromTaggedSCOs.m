function [ theSig, chipGenOut ] = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, chipGen )
%
% GenerateSignalFromTaggedSCOs - IF signal generation from tagged subcarrier objects
%
%   [theSig, chipGenOut] = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, chipGen )
%
%   tSCO1 - A tagged SCO with receiver time <= tt(1)
%   tSCO2 - A tagged SCO with receiver time >= tt(end)
%   tt - The receiver times at which to generate the signal
%   chipGen - A chip generator object
%
%   theSig - the generated IF signal
%   chipGenOut - Updated chip generator object
%

% (c) 2016 Cillian O'Driscoll

if  tt(1) < 0  %tSCO1.Trx > tt(1)
    error( 'The first reciever time stamp is earlier than the first tagged SCO' );
end

T_TX = tSCO2.Ttx - tSCO1.Ttx;

if isa( T_TX, 'GnssTimeInterval' )
    T_TX = T_TX.AsSeconds();
end

if T_TX < 0
    T_TX = T_TX + 604800;
end

T_RX = tSCO2.Trx - tSCO1.Trx;

if isa( T_RX, 'GnssTimeInterval' )
    T_RX = T_RX.AsSeconds();
end

if T_RX < 0
    T_RX = T_RX + 604800;
end

if tt(end) > T_RX % tSCO2.Trx < tt(end)
    error( 'The last receiver time stamp is later than the second tagged SCO' );
end


%NChips = length( theChips );
fc = tSCO1.fc;
NChips = round( T_TX * fc );
NPrevChips = ceil( tSCO1.x(end)*fc ) - 1;


% Now account for previous state:
NChips = NChips + NPrevChips;

dtRx = tt; % - tSCO1.Trx;
dtRx2 = tt - T_TX; % - tSCO2.Trx;

if isa( tSCO1.Ttx, 'GnssTime' )
    t0Secs = tSCO1.Ttx.TOW();
else
    t0Secs = tSCO1.Ttx;
end
tTx = t0Secs + dtRx + ( dtRx * tSCO1.timeDilation );
dtTx = dtRx + ( dtRx * tSCO1.timeDilation );

bbSig = zeros( size( tt ) );

Tc = 1/fc;
Tchip = tSCO1.Ttx - NPrevChips*Tc;
dTchip = -NPrevChips*Tc;

tTx0 = tSCO1.Ttx + dtTx(1);
eta = ( 1 - dtRx/( T_RX ) );
Ts = tt(2)-tt(1); fs = 1/Ts;
chipGenOut = chipGen;

TchipS = Tchip;
if isa( TchipS, 'GnssTime' )
    TchipS = TchipS.TOW();
end

TtChips = TchipS + (0:(NChips-1))*Tc + 0.5*Tc;
[theChips, chipGenOut ] = generate( chipGenOut, TtChips );


ttChip = dtTx - dTchip;
% Subtract Tc here as we want to add it to again at the start of the loop
% to simplify flow control:
tSCO1.x = tSCO1.x - Tc;
tSCO2.x = tSCO2.x - Tc;
for chipInd = 1:NChips
    tSCO1.x = tSCO1.x + Tc;
    tSCO2.x = tSCO2.x + Tc;

    %goodInds = find( ttChip >= tSCO1.x(1) & ttChip < tSCO1.x(end) );
    %disp( [ 'ttChip(1): ' num2str( ttChip(1) ) '. ttChip(end): ' num2str( ttChip(end) ) ] );
    if ttChip(1) > tSCO1.x(end) || ttChip(end) < tSCO1.x(1)
        continue;
    end
    tsBar = ( ttChip(end) - ttChip(1) )/length(ttChip);

    startIndApprox = max( 1, floor( ( tSCO1.x(1) - ttChip(1) )/tsBar ) + 1 );
    endIndApprox = min( length(ttChip), ceil( (tSCO1.x(end) - ttChip(1) )/tsBar + 1 ) );

    goodInds = startIndApprox:endIndApprox;

    if ~isempty( goodInds )

        scoInds = round( ( ttChip(goodInds)-tSCO1.x(1) )*tSCO1.fSample )+1;
        
        indsToKeep = find( scoInds > 0 & scoInds <= length( tSCO1.x ) );
        scoInds = scoInds(indsToKeep);
        goodInds = goodInds(indsToKeep);
        %if isempty( scoInds ) || min( scoInds ) <= 0 || max( scoInds ) > length( tSCO1.y )
            %keyboard;
        %end
        if chipInd > length( theChips )
            keyboard;
        end
        ctrb = tSCO1.scaleFactor*tSCO1.y(scoInds)*theChips(chipInd);

        bbSig(goodInds) = bbSig(goodInds) + eta(goodInds).*ctrb;

        scoInds = round( ( ttChip(goodInds)-tSCO2.x(1) )*tSCO2.fSample )+1;
        ctrb = tSCO2.scaleFactor*tSCO2.y(scoInds)*theChips(chipInd);

        bbSig(goodInds) = bbSig(goodInds) + (1-eta(goodInds)).*ctrb;
    end
    %Tchip = Tchip + Tc;
end

% Do a linear interpolation of the phase:
phi = eta.*(tSCO1.phi0 + tSCO1.phiDot*(1+ tSCO1.timeDilation)*dtRx) + ...
    (1-eta).*(tSCO2.phi0 + tSCO2.phiDot*(1+tSCO2.timeDilation)*dtRx2);
carr = exp(1j*phi);
theSig = bbSig .* carr;

% Normalise:
% This is a bit tricky:
%  SCO's are generated with unit energy per chip Ec, so that the
%  power in the high resolution signal is P = fc Ec = fc. We need to normalise this
%  to unity by dividing the signal by sqrt( fc ).
%nrmFactor = sqrt( Tc );
%theSig = theSig * nrmFactor;



%% Testing in Octave:
%!test
%!shared t, bbSig, tSCO1, tSCO2, fc, sco, fSample, theChips, fso, Ttx0, Trx0
%! fc = 1.023e6;
%! fso = 8e6;
%! fSample = 100*pi*fc;
%! theChips = 1- 2 * l1ca(1);
%! sco = MakeSubCarrier( @(t)BpskSubCarrier(t, fc), 2/fc, fSample );
%! [b, a] = butter( 8, (fso*0.4)/(fSample/2) );
%! sco = FilterSCO( b, a, sco );
%! figure;
%! plot( sco.x*fc, sco.y )
%! Ttx0 = 0;
%! Trx0 = 0.0001;
%! T = 0.0001;
%! tt = Trx0 + ( 0:(1/fso):T );
%! fDoppler = 2e3;
%! fRf = 1540*fc;
%! timeDilation = fDoppler/fRf;
%! phi0 = 0;
%! phiDot = fDoppler*2*pi;
%! tSCO1 = TagSCO( sco, Ttx0, Trx0, timeDilation, phi0, phiDot );
%! Tsco = 1e-3;
%! phi0 = phi0 + phiDot*Tsco;
%! tSCO2 = TagSCO( sco, Ttx0 + Tsco, Trx0 + Tsco*(1+timeDilation), timeDilation, phi0, phiDot);
%! tic;
%! theSig = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, tt, theChips, fc );
%! toc;
%! assert( length(theSig), length(tt) );
%! figure;
%! plot( tt*fc, [ real(theSig); imag(theSig) ], '.-' );
