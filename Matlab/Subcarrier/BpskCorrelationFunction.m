function rr = BpskCorrelationFunction( t, fc )
%
% BpskCorrelationFunction - Rectangle function auto-correlation
%
% Usage rr =  BpskCorrelationFunction( t, fc )
%
%   tNorm is the normalised time variable
%   sc = { 1, 0<=t<=Tc
%      = { 0, otherwise

% (c) 2016 Cillian O'Driscoll

rr = zeros( size( t ) );
atNorm = abs(t)*fc;
rr( atNorm  <=1 ) = 1 - atNorm( atNorm <= 1 );
