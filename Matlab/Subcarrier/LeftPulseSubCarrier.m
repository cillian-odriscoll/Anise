function sc = LeftPulseSubCarrier( t, fc )
%
% LeftPulseSubCarrier - Rectangle function, with half a chip width
%
% Usage sc = LeftPulseSubCarrier(  t, fc )
%
%   t - The time over which to compute the sub-carrier
%   fc - The chipping rate (chips/s)
%
% Note:
%   This function computes the time-limited sub-carrier. It is always zero
%   outside the range 0 <= t < 1/fc

% (c) 2016 Cillian O'Driscoll

sc = zeros( size( t ) );
tNorm = t*fc;
sc( tNorm >= 0 & tNorm <0.5 ) = 1;

