function aa = AltBocProductSubCarrier( t, fs, fc )

% AltBocProductSubCarrier - product sub carrier for alt boc modulation
%
% sc = AltBocProductSubCarrier( t, fs, fc )
%
%   t - time over which to evaluate the subcarrier
%   fs - Subcarrier frequency (Hz)
%   fc - Chip rate (chips/sec)
%
% The Galileo AltBOC product subcarrier would be given by:
%   fs = 15*1.023e6;
%   fc = 10*1.023e6;
%
% The product subcarrier is defined in Lestarquit et al "AltBOC for Dummies
% or Everything You Always Wanted to Know about AltBOC", ION GNSS 2008.
%
%   sc(t) = gamma * exp( j pi *( 5/8 - floor( t*8*fs )*3/4 ) ) 0 <= t < 1/fc
%         = 0                                                  Otherwise
%
% Where gamma = sqrt( fc ); is chosen to give unit power.
%
%

% (c) 2016 Cillian O'Driscoll

% Normalisation
gam = sqrt( 1 - 1/sqrt(2) );
%gam = sqrt(fc);

aa = gam*exp( 1j.*pi*( 5/8 - floor( t*8*fs )*3/4 ) );

aa( t < 0 | t >= 1/fc ) = 0;
