function psd = PsdBoc( f, fs, fc )
%
% PsdBoc - Sine Boc Sub-Carrier power spectral density
%
% Usage psd = PsdBoc( f, fs, fc )
%
% The PSD is given by:
%
%               1    |                 sin( pi N/2 + pi f/fc )  |2
%   Gboc(f) = ------ | sinc( f/fs/2 ) ------------------------- |
%             2 fs N |                     cos( pi f/fs/2 )     |
%
%   where N = 2 fs/fc;
%

% (c) 2016 Cillian O'Driscoll

Ts = 1/(2*fs); Tc = 1/fc;

N = Tc/Ts;

psd = Ts/N*abs( sinc( f*Ts ) .* sin( N/2*pi + pi*f*Tc ) ./ cos( pi * f * Ts ) ).^2;
