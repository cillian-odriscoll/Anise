function Gboc = PsdAltBoc( f, fs, fc )
%
% PsdAltBoc - Sine AltBoc Sub-Carrier power spectral density
%
% Usage psd = PsdAltBoc( f, fs, fc )
%
%  psd - Power Spectral Density in W/Hz normalised to unit power over
%  infinite bandwidth.
%
%  f - Frequency at which to evaluate the Psd
%  fs - AltBOC sub-carrier frequency (Hz)
%  fc - AltBOC chipping rate (chips/sec)
%
% The Galileo AltBOC PSD would be given by:
%
%   psdGalileo = PsdAltBoc( f, 15*1.023e6, 10*1.023e6 );
%

% (c) 2016 Cillian O'Driscoll

%Ts = 1/(2*fs); Tc = 1/fc;

%N = Tc/Ts;

n = 2*fs/fc;

%oneOverN = 1/n;

u = f./fc;

v = f./2./fs;

% From Avila-Rodriquez 2008
Gboc = 4./(pi^2*f.*u).*sin( pi*u + n*pi/2 ).^2 ./( cos( pi*v ).^2 ).*( ...
        cos( pi*v ).^2 - cos( pi*v ) - 2*cos(pi*v).*cos( 0.5*pi*v ) + 2 );
    
if mod( n, 2 ) == 1
    Gboc( f == 0 ) = 1/(fc*n);
end

Gboc = Gboc/8;
