function rho = AltBocSignalSubCarriersPowerRatio( fs, fc, BT )

% AltBocSideLobePowerRatio - compute the fraction of power in AltBoc side
% lobe
%
% rho = AltBocSignalSubCarriersPowerRatio( fs, fc, BT )
%
% Computes the component of the AltBOC that is in the signal sub-carriers
% Uses the following formulas:
%
%   rho = P_sc/P_altBoc
%
%   P_sc = 1/2 * integral( Psd_Sc(f), f, -BT/2, BT/2 )
%
%   P_altBoc = integral( Psd_AltBoc(f), f, -BT/2, BT/2 )
%
% The sub-carrier is given by:
%   x_sc(t) = sqrt(fc) gamma sum_{i=0}^{11} exp( j pi / 4[ 1/2 + i ] ) 
%                      * rect_{Ts/8}( t - i Ts/8 )
%
%  where gamma = sqrt( 1 + 1/sqrt(2) )
%
% Finally the PSD of the sub-carrier is given by:
%   Psd_Sc = | FT[ x_sc(t) ] |^2
%
% From which it can be shown that:
%
% integral( Psd_Sc(f), f, -BT/2, BT/2 ) = 
%     (gamma/8)^2*fc/fs*integral(
%     sinc(u/8)^2 * sin(3*pi/2*(1-u))^2 * sin(pi/3*(1-u))^2,
%        u, -BT/2*Ts, BT/2*Ts );

% (c) 2016 Cillian O'Driscoll

Ts = 1/fs;

if nargin < 3
    BT = inf;
end

% Normalisation
gam = sqrt( 1 + 1/sqrt(2) );

if isinf( BT )
    rho = gam^2/2;
    return;
end

fsOverFc = fs/fc;

intgrl = quadgk( @(x)intgrnd(x,fsOverFc), -BT/2*Ts, BT/2*Ts );

intgrl2 = quadgk( @(x)PsdAltBoc(x,fs,fc), -BT/2, BT/2 );

rho = (gam/8)^2*intgrl*fc/fs/intgrl2/2;


function ii  = intgrnd( u, fsOverFc )

ii =  sinc( (u)/8 ).^2 .* sin( fsOverFc*pi*(1-u) ).^2 ./ sin( pi/8 * (1-u) ).^2;
