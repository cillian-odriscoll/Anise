function sc = CBocSubCarrier( t, fs, fc, rho )
%
% CBocSubCarrier - Composite Boc Sub-Carrier function
%
% Usage sc =CBocSubCarrier( t, fs, fc, rho )
%
%   t - Vector of time samples at which to compute the sub-carrier.
%   fs - The sub-carrier rate (hz)
%   fc - The chipping rate (chips/s)
%  rho - The ratio of power in the high frequncy component
%
%   sc = sqrt( 1 - |rho| ) Boc( t, fc, fc ) + sgn( rho ) sqrt( |rho| ) Boc( t, fs, fc )
%
% Note:
%   This function computes the time-limited sub-carrier. It is always zero
%   outside the range 0 <= t < 1/fc
%     

% (c) 2016 Cillian O'Driscoll

sc = sqrt( 1 - abs(rho) ) * BocSubCarrier( t, fc, fc ) + sign(rho)*sqrt(abs(rho)) * BocSubCarrier( t, fs, fc );
