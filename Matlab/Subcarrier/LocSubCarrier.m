function sc = LocSubCarrier( t, fs, fc )
%
% LocSubCarrier - Sine Loc Sub-Carrier function
%
% Usage sc = LocSubCarrier( t, fs, fc )
%
%   t - Vector of time samples at which to compute the sub-carrier.
%   fs - The sub-carrier rate (hz)
%   fc - The chipping rate (chips/s)
%
% If fc is omitted this generates the periodic subcarrier, otherwise it
% generates a subcarrier with compact support 0 <= t < Tc
%   

% (c) 2016 Cillian O'Driscoll
Ts = 1/fs;

th = pi*((2*t/Ts));
sc = ( sin( th ) );

if nargin >= 3
  Tc = 1/fc; 
  sc( t >= Tc ) = 0;
  sc( t < 0 ) = 0;
end

