function psd = PsdBpsk( f, fc )
%
% PsdBpsk - BPSK Sub-Carrier power spectral density
%
% Usage psd = PsdBpsk( f, fc )
%
%   f is the frequency at which to evaluate the PSD
%   fc is the chipping rate of the subcarrier
%
% The PSD is given by:
%                 1   |               |2
%   Gbpsk(f) =  ----- | sinc( f / fc )| 
%                fc   |               |

% (c) 2016 Cillian O'Driscoll

Tc = 1/fc;

psd = 1/fc*abs( sinc( f*Tc ) ).^2;
