function [rr] = BocSinCorrelationFunction(tau, fs, fc)
%
% BocSinCorrelationFunction - Computes the correlation function for BOC sin
%
%	rr = BocSinCorrelationFunction( tau, fs, fc )
%
%		tau - Delay [s]
%       fs  - Subcarrier frequency [Hz]
%       fc  - Code frequency [chips/s]
%
%		rr - The BOC_sin(fs,fc) correlation function evaluated at tau
%
%	This is based on the closed form expressions for BOC correlation functions
%	from [1].
%
% [1] F. Sousa and F. Nunes, "New Expressions for the auto-correlation function
%     BOC GNSS signals", NAVIGATION: Journal of the Institute of Navigation,
%     vol. 60, no. 1, 2013.

% (c) 2016 Cillian O'Driscoll


    Tc = 1/fc;

    p = fs/fc;

    w = Tc/(2*p);
    fw = 1/w;

    rr = BpskCorrelationFunction( tau, fw );

    K = 2*p-1;
    for kk = 1:K
        rr = rr + (-1)^kk*(1-kk/2/p)*BpskCorrelationFunction( abs(tau)-kk*w, fw );
    end

end

