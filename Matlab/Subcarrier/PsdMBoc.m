function psd = PsdMBoc( f, fs, fc, rho )
%
% PsdMBoc - MBoc Sub-Carrier power spectral density
%
% Usage psd = PsdMBoc( f, fs, fc, rho )
%
%   f - Frequency at which to evaluate the PSD
%   fs - High rate subcarrier frequency (Hz)
%   fc - Low rate subcarrier frequency and code chipping rate (Hz and
%   chips/s)
%
%   rho - Fraction of power in the high rate subcarrier (unitless)
%
%
% The MBOC PSD is given as:
%
%             
%   G_mboc(f) = (1 - rho) G_boc(f, fc, fc ) + rho G_boc( f, fs, fc );
%

% (c) 2016 Cillian O'Driscoll

psd = (1-rho)*PsdBoc( f, fc, fc ) + rho*PsdBoc( f, fs, fc );
