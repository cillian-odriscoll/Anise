function psd = PsdGalileoE1( f )
%
% PsdGalileoE1 - Galileo E1 interplexed power spectral density
%
% Usage psd = PsdGalileoE1( f )
%
%  f - frequency at which to compute the PSD (Hz)
%  psd - Power spectral density of the Galileo E1 interplexed signal (W/Hz)
%
% Notes:
%   The E1 interplexed signal includes E1 OS, E1 PRS and intermodulation
%   terms as follows:
%   
%   psd = 1/4( Psd_OS(f) + Psd_BOCc( f, 15, 2.5 )*(1 + 1/5*(1-2*rho)^2 )
%              + Psd_PRS(f)*1/5*(1-2*rho)^2/4 )
%
%   The PSD is normalised to unit power over an infinite bandwidth
%
%   Note here that we consider the power in the Intermodulation Term to be
%   split between a BOCc( 15, 2.5 ) component (with 4/5 of the power) and a
%   BOCc( 15, 5 ) component with 1/5 of the power. 

% (c) 2016 Cillian O'Driscoll

fo = 1.023e6;
fsA = 15*fo;
fsB = 6*fo;

fcA = 2.5*fo;
fcB = fo;

rho = 1/11;
nuSq = (1-2*rho)^2/4;
betSq = 1/(2+nuSq);
%betSq = 1/2;


psd = betSq*( PsdMBoc(f, fsB, fcB, rho ) + PsdBocCos( f, fsA, fcA ) *( 1 + 4/5* nuSq ) ...
    + PsdBocCos( f, fsA, 2*fcA ) * 1/5 * nuSq );
