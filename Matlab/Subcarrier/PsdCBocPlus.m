function psd = PsdCBocPlus( f, fs, fc, rho )
%
% PsdCBocPlus - CBoc+ Sub-Carrier power spectral density
%
% Usage psd = PsdCBocPlus( f, fs, fc, rho )
%
%   f - Frequency at which to evaluate the PSD
%   fs - High rate subcarrier frequency (Hz)
%   fc - Low rate subcarrier frequency and code chipping rate (Hz and
%   chips/s)
%
%   rho - Fraction of power in the high rate subcarrier (unitless)
%
% CBOC+ is formed as
%
%   sc_cboc+(t) = sqrt( 1-rho )sc_boc( t, fc, fc ) + sqrt( rho ) sc_boc( t, fs, fc )
%
% where sc_boc( t, fs, fc ) is the sine phased BOC subcarrier.
%
% The CBOC+ subcarrier is used on the Galileo E1 OS data signal.
%
% The PSD is given as:
%
%             
%   G_cboc+(f) = (1 - rho) G_boc(f, fc, fc ) + rho G_boc( f, fs, fc )
%                   + Cross Spectral Density
%
% Where the Cross spectral density is given by:
%
%                           2            (
%   G_cross( f ) = --------------------  (
%                 sqrt(rho-rho^2) fc 2 N (
%
%                   x cos( pi [ 1 - N/2 ] ) sinc( f/2/fc ) * sinc( f/N/fc )
%
%                          sin( pi[1-f/fc] ) sin( pi[N/2-f/fc] )   )
%                   x   ------------------------------------------ )
%                        sin( pi[1-f/fc]/2 ) sin( pi[1/2-f/fc/N] ) )
%
% Where N = 2 fs / fc;
%

% (c) 2016 Cillian O'Driscoll

N1 = 2;
N2 = 2*fs/fc;

psd = (1-rho)*PsdBoc( f, fc, fc ) + rho*PsdBoc( f, fs, fc ) ...
    + 2*sqrt(rho*(1-rho))/fc/N1/N2 * cos( pi/2*(N1-N2) ) ...
    * sinc( f/N1/fc ) .* sinc( f/N2/fc ) ...
    .* sin( pi/2 *( N1 - 2*f/fc ) ) .* sin( pi/2 * (N2-2*f/fc ) ) ...
    ./ sin( pi/2 * ( 1 - 2*f/N1/fc ) ) ./ sin( pi/2*(1-2*f/N2/fc ) );
