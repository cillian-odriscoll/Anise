function Gboc = PsdAltBocSignalSubCarrier( f, fs, fc )
%
% PsdAltBocSignalSubCarrier - AltBoc signal Sub-Carrier power spectral density
%
% Usage psd = PsdAltBocSignalSubCarrier( f, fs, fc )
%
%  psd - Power Spectral Density in W/Hz normalised to unit power over
%  infinite bandwidth.
%
%  f - Frequency at which to evaluate the Psd
%  fs - AltBOC sub-carrier frequency (Hz)
%  fc - AltBOC chipping rate (chips/sec)
%
% The Galileo AltBOC signal subcarrier PSD would be given by:
%
%   psdGalileo = PsdAltBocSignalSubCarrier( f, 15*1.023e6, 10*1.023e6 );
%
% The AltBOC signal consists of 4 signal subcarriers and 4 product
% subcarriers. The product subcarriers are provided to ensure a constant
% envelope.
%
% The product subcarrier is defined in Lestarquit et al "AltBOC for Dummies
% or Everything You Always Wanted to Know about AltBOC", ION GNSS 2008.
%
%   sc(t) = gamma * exp( j pi *( 5/8 - floor( t*8*fs )*3/4 ) ) 0 <= t < 1/fc
%         = 0                                                  Otherwise
%
% Where gamma = sqrt( 1 - 1/sqrt(2) ); is chosen to give unit power.
%
% From this the PSD can be derived as:
%                    1    |                 sin( N pi /8 [ 1 + f/fs ] ) |2
%   Galtboc(f) =   ------ | sinc( f/fs/8 ) ---------------------------- |
%                  8 fs N |                   sin( pi/8 [ 1 + f/fs ] )  | 
%
% Where N = 8*fs/fc
%
% 

% (c) 2016 Cillian O'Driscoll

Ts = 1/(fs);

N = 8*fs/fc;

fsOverfc = fs/fc;

Gboc = 1/N/8*Ts*(sinc(f*Ts/8).*sin(pi*fsOverfc*(1+f*Ts))./sin(pi/8*(1+f*Ts)) ).^2;


