function [rr] = BocCosCorrelationFunction(tau, fs, fc)
%
% BocCosCorrelationFunction - Computes the correlation function for BOC cos phasing
%
%	rr = BocCosCorrelationFunction( tau, fs, fc )
%
%		tau, fs, fc - Delay in seconds
%       fs  - Subcarrier rate [Hz]
%       fc - Chip rate [chips/s]
%
%		rr - Correlation function
%
%   This is based on the closed form expression for the BOCcos correlation
%   function from [1].
%
%
% [1] F. Sousa and F. Nunes, "New Expressions for the auto-correlation function
%     BOC GNSS signals", NAVIGATION: Journal of the Institute of Navigation,
%     vol. 60, no. 1, 2013.

% (c) 2016 Cillian O'Driscoll


    Tc = 1/fc;

    p = fs/fc;

    w = Tc/(4*p);
    fw = 1/w;

    rr = BpskCorrelationFunction( tau, fw );

    K = 2*p-1;
    for kk = 1:K
        rr = rr + (-1)^kk*(1-kk/2/p)*BpskCorrelationFunction( abs(tau)-2*kk*w, fw );
    end

    for kk = 1:(K+1)
        rr = rr + 1/(4*p)*(-1)^(kk)*BpskCorrelationFunction( abs(tau)-(2*kk-1)*w, fw );
    end

end

