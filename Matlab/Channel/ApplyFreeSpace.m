function scoRx = ApplyFreeSpace( tSCO, geom )
%
% ApplyFreeSpace - Apply free space propagation to a tagged SCO
%
% scoRx = ApplyFreeSpace( tSCO, geom );
%
%   tSCO - A time tagged SCO to which to apply the free space propagation model
%   geom - A geometry structure containing information on at least the range and range rate
%          at the time tSCO.Trx

% (c) 2016 Cillian O'Driscoll

if nargin < 2
    error( 'Insufficient arguments' );
end

if ~isfield( geom, 'range' )
    error( 'geom struct does not have a ''range'' field' )
end

if ~isfield( geom, 'rangeRate' )
    error( 'geom struct does not have a ''rangeRate'' field' )
end

kLightSpeed =  299792458;

% Extract the range and range rate:
r = geom.range;
rDot = geom.rangeRate;

% Convert to delay and time dilation coefficients
timeDelayGeom = r / kLightSpeed;
timeDilationGeom = -rDot / kLightSpeed; % +ve range rate => decreasing apparent frequency

% Apply the delay:
scoRx = tSCO;
scoRx.Trx = tSCO.Trx + timeDelayGeom;

% Apply the Time Dilation effect:
% freq_out = ( 1 + alpha ) * freq_in;
%
% Everything in the tagged SCO is referred to the time frame of the signal generator
% so freq_after_geom = ( 1 + alpha_geom ) * ( 1 + alpha_up_to_geom ) * freq_in
% = (1 + alpha_geom + alpha_up_to_geom + alpha_geom*alpha_up_to_geom ) * freq_in
% = (1 + alpha_after_geom) * freq_in
scoRx.timeDilation = tSCO.timeDilation + timeDilationGeom + tSCO.timeDilation * timeDilationGeom;

% Apply the free space loss:
phiDotInTrue = ( 1 + tSCO.timeDilation ) * tSCO.phiDot;
freeSpaceLoss = ( 2*abs(phiDotInTrue)* r / kLightSpeed );

scoRx.scaleFactor = tSCO.scaleFactor / freeSpaceLoss;

