function phi = IonoPhase( f, TECU )

% IonoPhase - Phase response of ionosphere at a given freq and TECU
%
%   phi = IonoPhase(f, TECU );
%
%       f - The frequency at which to compute the phase response [Hz]
%     TECU - The total electron content in TEC units
%
%   The phase response is approximately given by:
%
%               2 pi 40.3*TECU*10^16
%       phi =  ----------------------
%                    c f

% (c) 2016 Cillian O'Driscoll

kLightSpeed = 299792458;
phi = 2*pi*40.3.*TECU*10^16./f/kLightSpeed;
