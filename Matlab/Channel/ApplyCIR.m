function scoOut = ApplyCIR( scoIn, CIR, method )
%
% ApplyCIR - Apply a channel impulse response to a subcarier object
%
%   scoOut = ApplyCIR( scoIn, CIR );
%
%       scoIn - Subcarrier object to which to apply the channel impulse response
%       CIR - Channel impulse response matrix
%       method - [Optional] one of 'direct' or 'filter'
%       scoOut - Subcarrier object resulting from applying CIR to scoIn
%
% CIR must be a matrix with at least two columns. Each row represents a single
% reflection with columns:
%   1) Delay (in seconds) realtive to the line of sight
%   2) Complex channel response of the reflection (amplitude and phase)
%
% The direct method is the most accurate, applying the CIR by delaying and
% multiplying the input SCO by the complex channel responses. The 'filter'
% method uses the MakeMultipathFilter to approximate the CIR as a FIR filter
% at the SCO sampling rate.
%
%
% Example usage:
%
%   fo = 1.023e6; fs = fo; fc = fo; Tc = 1/fc; % BOC(1,1)
%
%   fSample = 100*pi*fs;
%
%   sco = MakeSubcarrier( @(t)BocSubcarrier( t, fs ), Tc, fSample );
%
%   % Load a channel impulse response from somewhere....
%
%   scoMp = ApplyCIR( sco, CIR );

% (c) 2016 Cillian O'Driscoll

scoOut = scoIn;

scoOut.y = zeros( size( scoOut.x ) );

if nargin < 3
    method = 'filter';
end



if strcmp( method, 'direct' )

    for ii = 1:size(CIR,1)
        tau = CIR(ii,1);
        a = CIR(ii,2);
        tSco = scoIn;
        tSco.x = tSco.x+tau;
        tSco.y = tSco.y * a;
        scoOut = AddSCO( scoOut, tSco );
    end

elseif strcmp( method, 'filter' )
    b = MakeMultipathFilter( CIR, scoIn.fSample );

    scoOut = FilterSCO( b, 1, scoIn );
end


