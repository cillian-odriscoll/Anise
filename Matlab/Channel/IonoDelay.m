function tau = IonoDelay( f, TECU )
% IonoDelay - Group delay of the ionosphere at a given freq and TECU
%
%   tau = IonoDelay(f, TECU );
%
%       f - The frequency at which to compute the delay [Hz]
%       TECU - The total electron content in TEC Units
%       tau - The delay due to the ionosphere [s]
%
%   The delay is approximately given by:
%
%               40.3*TECU*10^16
%       tau =  -----------------
%                    c f^2

% (c) 2016 Cillian O'Driscoll

tau = IonoPhase(f,TECU)./f/2/pi;
