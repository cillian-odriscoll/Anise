function scoIono = ApplyIono( sco, TECU, fRf )
%
% ApplyIono - Applies the first order ionospheric dispersion to an SCO
%
%   scoIono = ApplyIono( sco, TECU[, fRf] )
%
%   sco - Input SCO to which to apply the ionospheric effect
%   TECU - The total electron content in TEC units along the line of sight
%   fRf - Optional, the RF centre frequency of the broadcast signal
%
% Note: If sco is a tagged SCO, with a phiDot field (denoting the centre freqeuncy
% in rad/s) then this will be used instead of the fRf parameter, which is then optional

% (c) 2016 Cillian O'Driscoll

if isfield( sco, 'phiDot' )
    fRf = sco.phiDot/2/pi * ( 1 + sco.timeDilation );
elseif nargin < 3
    error( 'SCO does not contain a phiDot field and fRf parameter not present' );
end

fo = FftSCO( sco );

foIono = fo;

Hiono = exp(1j*IonoPhase( fo.x + fRf, TECU ) );

foIono.y = fo.y .* Hiono;

scoIono = IFftSCO( foIono );

scoIono.x = scoIono.x + sco.x(1);
scoIono.y = scoIono.y/sco.scaleFactor;
scoIono.scaleFactor = sco.scaleFactor;

