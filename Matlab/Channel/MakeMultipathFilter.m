function b = MakeMultipathFilter( alpha, tau, phi, fs )
%
% MakeMultipathFilter - Create an FIR tapped delay line filter aproximating a multipath channel
%
% b = MakeMultipathFilter( alpha, tau, phi, fs )
%
%   alpha - Relative power attenuation of multipath ray[s] in natural units
%   tau   - Relative delay of multipath ray[s] in seconds
%   phi   - Relative phase shift of multipath ray[s] in radians
%   fs    - Sampling rate of the resultant digital filter
%
% Alpha, phi and tau can either be scalar or vector, but must all be the same
% size.
%
% b = MakeMultipathFilter( CIR, ts );
%
%   CIR - A channel impulse response matrix
%
% CIR must be a matrix with at least two columns. Each row represents a single
% reflection with columns:
%   1) Delay (in seconds) realtive to the line of sight
%   2) Complex channel response of the reflection (amplitude and phase)
%
%
% Example usage:
%
%   fo = 1.023e6; fs = fo; fc = fo; Tc = 1/fc; % BOC(1,1)
%
%   fSample = 100*pi*fs;
%
%   sco = MakeSubcarrier( @(t)BocSubcarrier( t, fs ), Tc, fSample );
%
%   b = MakeMultipathFilter( 0.5, Tc/2, pi/2, fSample );
%
%   scoMP = FilterSCO( b, 1, sco );

% (c) 2016 Cillian O'Driscoll

% When passing in alpha, tau and phi assume that these are MP only components
% in which case the LOS of 1 should be added
doAddLOS = 1;
if nargin == 2
    cir = alpha;
    fs = tau;

    if size( cir, 2 ) < 2
        error( 'Invalid channel impulse response, must have at least two columns' );
    end

    tau = real( cir(:,1) );
    alpha = abs( cir(:,2) ).^2;
    phi = angle( cir(:,2) );
    % When given a CIR assume that the LOS is included in the CIR
    doAddLOS = 0;
elseif nargin ~= 4
    error( 'Incorrect number of arguments' );
end

tauSamps = tau * fs;

integerDelays = floor( tauSamps );
eta = tauSamps - integerDelays;

N = max( integerDelays ); % filter length

% Make the coefficient vector:
b = zeros( 1, N+2 );

if doAddLOS
    b(1) = 1; % LOS
end

for ii = 1:length(integerDelays)
    coeff = sqrt(alpha(ii))*exp(1j*phi(ii));
    b(integerDelays(ii)+1) = b(integerDelays(ii)+1) + (1-eta(ii))*coeff;
    b(integerDelays(ii)+2) = b(integerDelays(ii)+2) + eta(ii)*coeff;
end
