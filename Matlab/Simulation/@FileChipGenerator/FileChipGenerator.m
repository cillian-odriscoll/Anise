function chipGen = FileChipGenerator( varargin )
%
% FileChipGenerator - A ChipGenerator that uses a file as a source of chips
%
%
%     chipGen = FileChipGenerator( fileName, Tc )
%         theChips - vector of code chips [+/-1]
%         Tc - The chip period
%
%         chipGen - A chip generator that generates the periodic code
%         theChips
%
% The file format is a simple ascii file with one entry per line as follow:
%
%   firstChipIndex
%   codeLength
%   +1
%   -1
%   +1
%   ...
%
% There are N+2 lines in the file, where N is the total number of chips stored.
% The first line contains the first chip index (zero based) and the second line
% contains the code length (in chips). In this way the file can contain an
% arbitrary subset of an arbitrarily long code (well, in fact the code is
% limited to be less than 2^64 chips long due to our constraints on representing
% bigger numbers in codeLength).
%
% See Also
%   ChipGenerator

% (c) 2016 Cillian O'Driscoll

chipGen = struct();

if nargin == 0
    chipGen.fileName = '';
    chipGen.chipStore = [];
    chipGen.firstInd = 1;
    chipGen.fid = -1;
    chipgen.storeFirstInd = 0;
    Tc = inf;
elseif nargin == 1
    if isa( varargin{1}, 'FileChipGenerator' ) % copy constructor
        chipGen = varargin{1};
        return;
    else
        error( 'FileChipGenerator: unexpected argument' );
    end
elseif nargin == 2
    if ~ischar( varargin{1} ) || ...
        ~isnumeric( varargin{2} ) || ~isscalar( varargin{2} )
        error( 'FileChipGenerator: 1st argument must be a string and second must be a scalar' );
    else
        chipGen.fileName = varargin{1};
        chipGen.fid = fopen( chipGen.fileName, 'r' );
        if chipGen.fid == -1
            error( [ 'Unable to open chip file: ' chipGen.fileName ] );
        end
        chipGen.firstInd = double( fscanf( chipGen.fid, '%ld', 1 ) );
        chipGen.codeLength = double( fscanf( chipGen.fid, '%ld', 1 ) );
        
        chipGen.bufSize = 1024*1024;
        chipGen.chipStore = [];

        frewind( chipGen.fid );

        tmp = fread( chipGen.fid, inf, 'uint8' );
        numLines = sum( tmp == 10 ) + 1;
        chipGen.numChipsInFile = numLines - 2;
        chipGen.lastInd = chipGen.firstInd + chipGen.numChipsInFile - 1;
        chipGen.storeFirstInd = 0;
        frewind( chipGen.fid );

        Tc = varargin{2};
    end
else
    error( 'FileChipGenerator: unexpected number of arguments' );
end


baseObj = ChipGenerator( Tc, chipGen.codeLength );

chipGen = class( chipGen, 'FileChipGenerator', baseObj );

