function [ theChips, chipGenOut ] = generate( chipGen, tt )
%
% generate - Generate chip values given a series of timestamps
%
%   [ theChips, chipGenOut ] = generate( chipGen, tt );
%
%       chipGen - The FileChipGenerator object to use to generate the chips
%       tt      - A vector of time points at which to generate the chip values 
%
%       theChips - Vector of chip values (+/-1), one for each point in tt
%       chipGenOut - The updated FileChipGenerator object
%
%   This function will throw an error if an attempt is made to generate chips
%   outside of the range of chips stored in the file.
%

% (c) 2016 Cillian O'Driscoll

chipGenOut = chipGen;

inds = timeToChipIndex( chipGen, tt );

inds( inds < inds(1) ) = inds( inds < inds(1) ) + chipGen.codeLength;

firstIdx = inds(1);
lastIdx = inds(end);

% Do we need to refill the store?
doRefill = isempty( chipGen.chipStore ) || ...
    ( firstIdx < chipGen.storeFirstInd ) || ...
    ( lastIdx > chipGen.storeFirstInd + length( chipGen.chipStore ) );

if doRefill
    % Check that we have the required chips in the file:
    if firstIdx < chipGen.firstInd || lastIdx > chipGen.lastInd
        error( ['Attempt to access chips not stored in the file. Requested [' ...
            num2str(firstIdx) ', ' num2str(lastIdx) ']. File contains: [' ...
            num2str( chipGen.firstInd ) ', ' num2str( chipGen.lastInd ) ']' ] );
    end

    linesToSkip = 2 + ( firstIdx - chipGen.firstInd );
    linesToRead = min( chipGen.bufSize, chipGen.lastInd - firstIdx );

    frewind( chipGen.fid );
    chipGenOut.chipStore = textscan( chipGenOut.fid, '%f', linesToRead, 'HeaderLines', linesToSkip );
    chipGenOut.chipStore = [chipGenOut.chipStore{:}];
    chipGenOut.storeFirstInd = firstIdx;
end

storeStartInd = firstIdx - chipGenOut.storeFirstInd + 1;
storeInds = inds - chipGenOut.storeFirstInd + 1;
theChips = chipGenOut.chipStore( storeInds );
    

