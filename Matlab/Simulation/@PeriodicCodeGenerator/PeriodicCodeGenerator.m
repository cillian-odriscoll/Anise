function chipGen = PeriodicCodeGenerator( varargin )
%
% PeriodicCodeGenerator - A ChipGenerator that uses a period code
%
%
%     chipGen = PeriodicCodeGenerator( theChips, Tc )
%         theChips - vector of code chips [+/-1]
%         Tc - The chip period
%
%         chipGen - A chip generator that generates the periodic code
%         theChips
%
% See Also
%   ChipGenerator

% (c) 2016 Cillian O'Driscoll

chipGen = struct();

if nargin == 0
    chipGen.theChips = [1];
    Tc = inf;
elseif nargin == 1
    if isa( varargin{1}, 'PeriodicCodeGenerator' ) % copy constructor
        chipGen = varargin{1};
        return;
    else
        error( 'PeriodicCodeGenerator: unexpected argument' );
    end
elseif nargin == 2
    if ~isnumeric( varargin{1} ) || ~isvector( varargin{1} ) || ...
        ~isnumeric( varargin{2} ) || ~isscalar( varargin{2} )
        error( 'PeriodicCodeGenerator: 1st argument must be a vector and second must be a scalar' );
    else
        chipGen.theChips = varargin{1};
        Tc = varargin{2};
    end
else
    error( 'PeriodicCodeGenerator: unexpected number of arguments' );
end


codeLength = length( chipGen.theChips );
baseObj = ChipGenerator( Tc, codeLength );

chipGen = class( chipGen, 'PeriodicCodeGenerator', baseObj );
