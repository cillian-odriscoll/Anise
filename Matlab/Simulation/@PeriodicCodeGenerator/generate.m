function [ theChips, chipGenOut ] = generate( chipGen, tt )
%
% generate - Generate chip values given a series of timestamps
%
%   [ theChips, chipGenOut ] = generate( chipGen, tt );
%
%       chipGen - The PeriodicCodeGenerator object to use to generate the chips
%       tt      - A vector of time points at which to generate the chip values 
%
%       theChips - Vector of chip values (+/-1), one for each point in tt
%       chipGenOut - The updated PeriodicCodeGenerator object

% (c) 2016 Cillian O'Driscoll

chipGenOut = chipGen;

theChips = chipGen.theChips( timeToChipIndex( chipGen, tt ) );

