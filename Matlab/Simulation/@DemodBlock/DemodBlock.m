function simBlock = DemodBlock( varargin )
%
% DemodBlock - Simulation block implementing demodulation
%
%   The DemodBlock operates on tagged SCOs and is constructed as follows:
%
%       demodBlock = DemodBlock( phiDot );
%
%       demodBlock = DemodBlock( phiDot, phi0 );
%
%           phiDot - The phase rate of change in the time frame of the input SCO [rad/s]
%           phi - The phase at the receive time of the input SCO
%
%  The DemodBlock has state, the phase, which is updated at each call. To apply
%  the demodulator to a tagged SCO, call:
%
%       [ scoOut, demodBlockOut ] = apply( demodBlock, scoIn );
%
%   This has the effect of rotating the carrier of the input SCO.
%
%   See Also:
%       SimulationBlock, TagSCO, Channel

% (c) 2016 Cillian O'Driscoll

simBlock.phiDot = 0;
simBlock.phi0 = 0;
simBlock.t = [];


if nargin == 1
    if isa( varargin{1}, 'SimulationBlock' ) % copy constructor
        simBlock = varargin{1};
        return;
    elseif isnumeric( varargin{1} ) % First argument is phiDot
        simBlock.phiDot = varargin{1};
    else
        error( 'DemodBlock: unexpected parameter' );
    end
elseif nargin == 2
    if isnumeric( varargin{1} ) && isnumeric( varargin{2} )
        simBlock.phiDot = varargin{1};
        simBlock.phi0 = varargin{2};
    else
        error( 'DemodBlock: both parameters should be numeric' );
    end
else
    error( 'DemodBlock: unexpected number of parameters' );
end

simBlock.phi = simBlock.phi0;

baseBlock = SimulationBlock();

simBlock = class( simBlock, 'DemodBlock', baseBlock );


