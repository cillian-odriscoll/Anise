function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the demodulator block to an SCO
%
%   [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
%       simBlock - The demodulator block to apply to scoIn
%       scoIn - The tagged SCO to be demodulated
%
%       scoOut - The demodulated SCO
%       simBlockOut - The demodulator after application 
%
%   See also:
%       SimulationBlock

% (c) 2016 Cillian O'Driscoll

if isempty( simBlock.t )
    dt = 0;
    simBlock.phi = simBlock.phi0;
else
    dt = scoIn.Trx - simBlock.t;
    if isa( dt, 'GnssTimeInterval' )
        dt = AsSeconds(dt);
    end
end


simBlock.phi = simBlock.phi + simBlock.phiDot * dt;
scoOut = scoIn;
scoOut.phi0 = scoIn.phi0 - simBlock.phi;

scoOut.phiDot = scoIn.phiDot - simBlock.phiDot/(1+scoIn.timeDilation);

simBlock.t = scoIn.Trx;

simBlockOut = simBlock;

