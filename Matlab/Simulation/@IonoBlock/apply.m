function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the IonoBlock to an SCO
%
%   [scoOut, simBlockOut] = apply( simBlock, scoIn );
%
%       simBlock - The IonoBlock to apply to scoIn
%       scoIn - The subcarrier object to which to apply the ionosphere
%
%       scoOut - The subcarrier object resulting from passing scoIn through
%               simBlock
%       simBlockOut - The ionosphere block after application to scoIn
%
% See Also:
%   ApplyIono, IonoBlock, SimulationBlock

% (c) 2016 Cillian O'Driscoll

scoOut = ApplyIono( scoIn, ...
    ValueAt( simBlock.tecTrajectory, scoIn.Trx ) ...
);

simBlockOut = simBlock;
