function simBlock = IonoBlock( varargin )
%
% IonoBlock - First order ionospheric simulation block
%
%   The IonoBlock is a SimulationBlock that applies the first order ionospheric
%   effect to subcarrier objects.
%
%   It is constructed from a TEC Trajectory object as follows:
%
%       ionoBlock = IonoBlock( tecTrajectory );
%
%           tecTrajectory - A Trajectory object that gives the line-of-sight TEC
%               in TEC units as a function of the receive time of the input SCO
%
%   The IonoBlock can be applied to a tagged SCO as follows:
%
%       [ scoOut, ionoBlockOut ] = apply( ionoBlock, scoIn );
%
%   where
%       scoIn is the input SCO, to which the ionospheric effect is to be applied
%       scoOut is the output SCO, after applying the ionospheric effect
%
% See Also:
%       ApplyIono, SimulationBlock, Trajectory

% (c) 2016 Cillian O'Driscoll

if nargin == 0
    tecTrajectory = ConstantTrajectory();
elseif nargin == 1 % copy constructor
    otherBlock = varargin{1};
    if isa( otherBlock, 'IonoBlock' )
        simBlock = otherBlock;
        return;
    elseif isa(otherBlock, 'Trajectory' )
        tecTrajectory = otherBlock;
    else
        error( ['IonoBlock: unexpected parameter.' ] );
    end
else
    error( ['IonoBlock: unexpected number of arguments ' num2str( nargin ) ] );
end

simBlock.tecTrajectory = tecTrajectory;
baseBlock = SimulationBlock();

simBlock = class( simBlock, 'IonoBlock', baseBlock );
