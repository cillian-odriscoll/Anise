function chipGen = FunctionalCodeGenerator( varargin )
%
% FunctionalCodeGenerator - Use a function handle to create chips
%
%     chipGen = FunctionalCodeGenerator( chipGenFun, Tc, codeLength )
%
%       chipGenFun - A function that generates code chips as a
%           function of code phase
%       Tc - The chip period
%       codeLength - optional, length of the code generated, defaults to inf
%
%       chipGen - The chip generator object
%
%   chipGenFun has the following signature:
%
%       theChips = chipGenFun( chipIndices )
%
% See Also:
%       ChipGenerator

% (c) 2016 Cillian O'Driscoll

codeLength = inf;
Tc = inf;
chipGenFun = @(ind)1;

if nargin == 1
    if isa( varargin{1}, 'FunctionalCodeGenerator' ) % copy constructor
        chipGen = varargin{1};
        return;
    else
        error( 'FunctionalCodeGenerator: unexpected argument' );
    end
elseif nargin >1 && nargin <=3
    if ~isa( varargin{1}, 'function_handle' )
        error( 'FunctionalCodeGenerator: first argument must be a function handle' );
    else
        chipGenFun = varargin{1};
        % This will throw an error if we haven't passed in a good function
        tmp = chipGenFun(1);
    end

    if isnumeric( varargin{2} )
        Tc = varargin{2};
    else
        error( 'FunctionalCodeGenerator: second argument must be numeric' );
    end

    if nargin == 3
        if isnumeric( varargin{3} )
            codeLength = varargin{3};
        else
            error( 'FunctionalCodeGenerator: third argument must be numeric' );
        end
    end

elseif nargin ~= 0
    error( 'FunctionalCodeGenerator: unexpected number of arguments' );
end

baseObj = ChipGenerator( Tc, codeLength );

chipGen.chipGenFun = chipGenFun;

chipGen = class( chipGen, 'FunctionalCodeGenerator', baseObj );

