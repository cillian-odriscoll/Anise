function chipGen = RandomChipGenerator( varargin )
%
% RandomChipGenerator - Chip generator class that generates random chips
%
%   chipGen = RandomChipGenerator( Tc );
%       Tc - The chip period
%
%       chipGen - A random chip generator that can be used with MakeChipGenerator
%               for simulating GNSS signals
%
%   example usage:
%
%   prn = 1;
%   fc = 1.023e6; Tc = 1/fc;
%   Td = 20*Tc;
%   fs = 5e6; Ts = 1/fs;
%
%   caCodeGen = PeriodicChipGenerator( 1-2*l1ca(prn), Tc );
%
%   dataGen = RandomDataGenerator( Td );
%
%   caChipGen = ChainChipGenerator( { caCodeGen, dataGen } );
%
%   tt = 0:Ts:3*Td;
%
%   theChips = caChipGen.generate( tt );

% (c) 2016 Cillian O'Driscoll

chipGen.bitStore = [];
chipGen.lastInds = [];
Tc = inf;
codeLength = inf;


if nargin == 1
    if isa( varargin{1}, 'RandomChipGenerator' ) % copy constructor
        chipGen = varargin{1};
        return;
    elseif isnumeric( varargin{1} ) && isscalar( varargin{1} )
        Tc = varargin{1};
    else
        error( 'RandomChipGenerator: unexpected argument' );
    end
elseif nargin ~= 0
    error( 'RandomChipGenerator: unexpected number of arguments' );
end

baseObj = ChipGenerator( Tc, codeLength );

chipGen = class( chipGen, 'RandomChipGenerator', baseObj );

