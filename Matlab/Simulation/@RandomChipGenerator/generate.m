function [ theChips, chipGenOut ] = generate( chipGen, tt )
%
% generate - Generate chip values given a series of timestamps
%
%   [ theChips, chipGenOut ] = generate( chipGen, tt );
%
%       chipGen - The RandomChipGenerator object to use to generate the chips
%       tt      - A vector of time points at which to generate the chip values 
%
%       theChips - Vector of chip values (+/-1), one for each point in tt
%       chipGenOut - The updated RandomChipGenerator object

% (c) 2016 Cillian O'Driscoll

chipGenOut = chipGen;

currInds = timeToChipIndex( chipGen, tt );

uniqCurrInds = unique( currInds );

indsToGenerate = setdiff( uniqCurrInds, chipGen.lastInds );

[ indsToKeep, idxOld, idxNew ] = intersect( chipGen.lastInds, uniqCurrInds );

oldInds = chipGen.lastInds(idxOld);
oldBits = chipGen.bitStore(idxOld);

newBits = [];
newInds = [];

if ~isempty( indsToGenerate )

    newInds = min(indsToGenerate):max(indsToGenerate);
    numBitsToGenerate = length( newInds );

    newBits = 1-2*round( rand(1, numBitsToGenerate ) );
end

chipGenOut.bitStore = [ oldBits, newBits ];
chipGenOut.lastInds = [ oldInds, newInds ];

theChips = chipGenOut.bitStore( currInds - chipGenOut.lastInds(1) + 1 );

