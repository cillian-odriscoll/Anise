function varargout = subsref( simBlock, idx )
%
% subsref - Define subscripted references for SimulationBlock objects
%
%   This is an internal utility function to access methods and properties on
%   class objects using dot notation:
%
%       e.g. using
%           [ scoOut, simBlock] = simBlock.apply( scoIn );
%       rather than
%           [scoOut, simBlock ] = apply( simBlock, scoIn );

% (c) 2016 Cillian O'Driscoll

persistent methods__
persistent properties__

if isempty( methods__ )
    methods__.apply = @(o,tSCO)apply(o,tSCO);
end

if idx(1).type ~= '.'
    disp( idx );
    error( [ 'Invalid index for ' class(simBlock) '. with: ' ] );
end

methodOrProperty = idx(1).subs;
out = cell( nargout, 1 );

if isfield( properties__, methodOrProperty ) && numel(idx) == 1
    % Now we have a property
    [out{:}] = simBlock( methodOrProperty );
elseif isfield( methods__, methodOrProperty ) && numel(idx) > 1
    if idx(2).type ~= '()'
        error( [ 'Bad call to method ' methodOrProperty idx(2).type ] );
    end

    args=idx(2).subs;
    fhandle = methods__.(methodOrProperty);

    %disp( [ 'NARGOUT: ' num2str( nargout ) ] );

    if isempty(args)
        [out{:}] = fhandle(simBlock);
    else
        [out{:}] = fhandle(simBlock, args{:});
    end

else
    % Uh oh:
    error( [ 'Unknown property/method: ' methodOrProperty ] );
end

varargout = out;


