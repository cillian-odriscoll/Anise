function simBlock = SimulationBlock( varargin )
%
% SimulationBlock - Abstract base class representing simulation blocks
%
%   All simulation blocks apply a transformation to a SubCarrier Object (SCO) as
%   follows:
%
%       [ scoOut, simBlockOut ] = apply( simBlockIn, scoIn );
%
%       scoIn - The input sco to be transformed
%       simBlockIn - The SimulationBlock to do the transformation
%
%       scoOut - The transformed SCO
%       simBlockOut - The SimulationBlock after the transformation
%
%   In general a SimulationBlock may have some state, hence the need to capture
%   the updated SimulationBlock after the applciation of the transformation.
%
%   SimulationBlocks may be chained together in Channel objects.
%
% See Also:
%   Channel, FilterBlock, IonoBlock, GeomBlock, PowerGainBlock,
%   PowerNormalisationBlock, CIRBlock

% (c) 2016 Cillian O'Driscoll

if nargin == 1
    if isa( varargin{1}, 'SimulationBlock' )
        simBlock = varargin{1};
    else
        error( [ 'SimulationBlock: unexpected argument ' varargin{1} ] );
    end
elseif nargin == 0
    simBlock = struct();
    simBlock = class( simBlock, 'SimulationBlock' );
else
    error( [ 'SimulationBlock: too many arguments'] );
end
