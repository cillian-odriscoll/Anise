function [ theChips, chipGenOut ] = generate( chipGen, tt )
%
% generate - Generate chip values given a series of timestamps
%
%   [ theChips, chipGenOut ] = generate( chipGen, tt );
%
%       chipGen - The ChainChipGenerator object to use to generate the chips
%       tt      - A vector of time points at which to generate the chip values 
%
%       theChips - Vector of chip values (+/-1), one for each point in tt
%       chipGenOut - The updated ChainChipGenerator object

% (c) 2016 Cillian O'Driscoll

theChips = [];
chipGenOut = chipGen;

for ii = 1:length( chipGen.generators )
    [localChips, chipGenOut.generators{ii}] = generate( ...
        chipGen.generators{ii}, tt );

    if isempty( theChips )
        theChips = localChips;
    else
        theChips = theChips.*localChips;
    end

end


