function chipGen = ChainChipGenerator( varargin )
%
% ChainChipGenerator - Class for chaining together chip generators
%
%     chipGen = ChainChipGenerator( chipGenArray );
%         chipGenArray - A cell array of ChipGenerators
%
%         chipGen - A chip generator that returns the product of the
%         array of generators passed in
%
%     E.g. For the GPS L1 C/A code:
%     fc = 1.023e6; Tc = 1/fc;
%     dataRate = 50; % bps
%     Td = 1/dataRate;
%
%     prn = 12;
%     theCode = 1 - 2*l1ca(prn);
%     codeGen = PeriodicCodeGenerator( theCode, Tc );
%
%     dataGenFun = MakeRandomDataGenerator(Td);
%     dataGen = FunctionalCodeGenerator( dataGenFun, Td );
%
%     caChipGen = ChainChipGenerator( { codeGen, dataGen } );
%
%     theChips = generate( caChipGen, 0:Tc:(50*1023*Tc) ); % Generate 50 chips worth

% (c) 2016 Cillian O'Driscoll

theGenerators = {};
Tc = inf;
codePeriod = inf;

if nargin == 1
    if isa( varargin{1}, 'ChainChipGenerator' ) % copy code
        chipGen = varargin{1};
        return;
    end
end

if nargin > 2
    error( 'ChainChipGenerator: unexpected number of arguments' );
end

if nargin > 0
    if ~iscell( varargin{1} )
        error( 'ChainChipGenerator: unexpected argument' );
    end

    theGenerators = varargin{1};
end

if nargin > 1
    if ~isnumeric( varargin{2} ) && ~isscalar( varargin{2} )
        error( 'ChainChipGenerator: second argument should be numeric scalar' );
    else
        Tc = varargin{2};
    end
end

% Check the generators are all ChipGenerators
if ~all( cellfun( @(obj)isa(obj, 'ChipGenerator'), theGenerators ) )
    error( 'ChainChipGenerator: all objects must be ChipGenerator objects' );
end

% Now if we have not already set Tc, try and get it here:
% here we are effectively looking for the gcd of the Tc values
if isinf( Tc )
    tcVec = cellfun( @(o)get( o, 'Tc'), theGenerators );
    fcCell = num2cell( 1./tcVec );
    lcmFc = lcm_vec( [fcCell{:}] );
    Tc = 1./lcmFc;

    codeLengthVec = cellfun( @(o)double(get(o,'codeLength')), theGenerators );

    codeLengthLcmCell = num2cell( lcmFc*tcVec.*codeLengthVec );

    if any( cellfun( @isinf, codeLengthLcmCell ) )
        codePeriod = inf;
    else
        codePeriod = lcm_vec( [codeLengthLcmCell{:}] );
    end

end

baseObj = ChipGenerator( Tc, codePeriod );

chipGen.generators = theGenerators;

chipGen = class( chipGen, 'ChainChipGenerator', baseObj );


function ll = lcm_vec( v )

ll = v(1);
for ii = 2:length(v)
    ll = lcm( ll, v(ii) );
end
