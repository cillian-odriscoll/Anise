function [scoOut, simBlockOut ]= apply( simBlock, scoIn )
%
% apply - Apply the geometry block to an SCO
%
%   [scoOut, simBlockOut] = apply( simBlock, scoIn );
%
%       simBlock - The GeomBlock to apply to scoIn
%       scoIn - The subcarrier object to which to apply GeomBlock
%
%       scoOut - The subcarrier object resulting from applying GeomBlock to
%       scoIn
%

% (c) 2016 Cillian O'Driscoll

geom.range = ValueAt( simBlock.rangeTrajectory, scoIn.Trx );
geom.rangeRate = ValueAt( simBlock.rangeRateTrajectory, scoIn.Trx );

scoOut = ApplyFreeSpace( scoIn, geom );
simBlockOut = simBlock;
