function simBlock = GeomBlock( varargin )
%
% GeomBlock - Geometry simulation block (range and range rate)
%
%   The GeomBlock is a SimulationBlock object that applies the attenuation,
%   delay and Doppler effect to a subcarrier object. It is constructed from two
%   Trajectory objects as follows:
%
%       geomBlock = GeomBlock( rangeTrajectory, rangeRateTrajectory );
%
%           rangeTrajectory - Range [m] trajectory object
%           rangeRateTrajectory - Range rate [m/s] trajectory object
%
%   The two trajectory objects must give the channel range and range rate as a
%   function of the receive time of scoIn. This is important to note.
%
%   The block can be applied to a tagged SCO as follows:
%
%       [ scoOut, geomBlockOut ] = apply( geomBlock, scoIn );
%
%   The receive time of the output SCO will be given by the receive time of the
%   input SCO plus the range at the receive time of the input SCO divided by the
%   speed of light. Similarly the time dilation coefficient will be updated
%   based on the range rate at the receive time of the input SCO.
%
% See Also:
%       ApplyFreeSpace, SimulationBlock, Trajectory

% (c) 2016 Cillian O'Driscoll

if nargin == 0
    rangeTrajectory = ConstantTrajectory();
    rangeRateTrajectory = constantTrajectory();
elseif nargin == 1 % copy constructor
    otherBlock = varargin{1};
    if isa( otherBlock, 'GeomBlock' )
        simBlock = otherBlock;
    else
        error( ['GeomBlock: unexpected parameter.' ] );
    end
elseif nargin == 2 % initialising constructor:
    rangeTrajectory = varargin{1};
    rangeRateTrajectory = varargin{2};
else
    error( [ 'Unexpected number of parameters: ' num2str( nargin ) ] );
end

% now initialise the object if we can
if isa( rangeTrajectory, 'Trajectory' ) && isa( rangeRateTrajectory, 'Trajectory' )
    simBlock.rangeTrajectory = rangeTrajectory;
    simBlock.rangeRateTrajectory = rangeRateTrajectory;

    baseBlock = SimulationBlock();
    simBlock = class( simBlock, 'GeomBlock', baseBlock );
else
    error( [ 'GeomBlock: inputs must be Trajectory objects' ] );
end
