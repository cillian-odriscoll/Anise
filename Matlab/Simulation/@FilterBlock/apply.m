function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the filter block to an SCO
%
%   [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
%       simBlock - The filter block to apply
%       scoIn - The subcarrier object to be filtered
%
%       scoOut - the filtered subcarrier object
%       simBlockOut - The filter block after generating scoOut
%
%

% (c) 2016 Cillian O'Driscoll

scoOut = FilterSCO( simBlock.b, simBlock.a, scoIn );
simBlockOut = simBlock;
