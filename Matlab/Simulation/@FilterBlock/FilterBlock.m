function simBlock = FilterBlock( varargin )
%
% FilterBlock - Linear Time Invariant Filter SimulationBlock
%
%   The FilterBlock is a SimulationBlock object implementing an FIR filter.
%
%       filtBlock = FilterBlock( b, a );
%
%           b - Filter numerator 
%           a - Filter denominator
%
%   The filter can be applied to a subcarrier object as follows:
%
%       [scoOut, filtBlockOut] = apply( filtBlockIn, scoIn );
%
%   See Also:
%       filter, SimulationBlock, FilterSCO

% (c) 2016 Cillian O'Driscoll

if nargin == 0
    a = [1];
    b = [1];
elseif nargin == 1
    otherBlock = varargin{1};
    if isa( otherBlock, 'FilterBlock' ) % copy constructor
        simBlock = otherBlock;
        return;
    else
        error( 'FilterBlock: unexpected parameter' );
    end
elseif nargin == 2
    b = varargin{1};
    a = varargin{2};
else
    error( 'FilterBlock: unexpected number of parameters' );
end

simBlock.a = a;
simBlock.b = b;
baseBlock = SimulationBlock();

simBlock = class( simBlock, 'FilterBlock', baseBlock );


