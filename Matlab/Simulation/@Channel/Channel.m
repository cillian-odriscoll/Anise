function theChannel = Channel( varargin )
%
% Channel - class implementing a channel of cascaded simulation blocks
%
%   simChannel = Channel();
%
%       constructs an empty Channel object
%
%   simChannel = Channel( otherChannel );
%
%       copy constructor
%
%   simChannel = Channel( simBlock );
%
%       creates a Channel with a single simulation block
%
%  In general a simulation Channel consists of a chain of simulation blocks. The
%  simulation chain is built up calling the 'push_back' function, which adds a
%  new SimulationBlock object to the back (last to be applied) of the chain.
%
%   simChannel = Channel();
%   % Create some simulation blocks
%   simChannel = push_back( simChannel, theBlock );
%
%  Once the channel has been constructed, it can be applied to (typcially
%  tagged) subcarrier objects calling the 'apply' function:
%
%   [ scoOut, channelOut ] = apply( channelIn, scoInd );
%
%  Note that SimulationBlocks may have state, and so by extension does the
%  channel, hence the need to return channelOut.

% (c) 2016 Cillian O'Driscoll

theChannel = struct();
theChannel.simBlocks = {};

if nargin == 1
    if isa( varargin{1}, 'Channel' ) % copy constructor
        theChannel = varargin{1};
        return;
    elseif isa( varargin{1}, 'SimulationBlock' ) % initialising constructor:
        theChannel.simBlocks = { varargin{1} };
    else
        error( 'Channel: unexpected parameter' );
    end
elseif nargin ~= 0
    error( 'Channel: unexpected number of arguments' );
end

baseClass = SimulationBlock();

theChannel = class( theChannel, 'Channel', baseClass );


