function chanOut = push_back( theChannel, simBlock )
%
% push_back - Push a simulation block to the back of the channel processing chain
%

if ~isa( simBlock, 'SimulationBlock' )
    error( 'Channel::push_back: second argument must be a SimulationBlock object' );
end

chanOut = theChannel;

chanOut.simBlocks{ length(theChannel.simBlocks)+1} = simBlock;

