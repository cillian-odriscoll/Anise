function [scoOut, channelOut] = apply( theChannel, scoIn )
%
% apply - Apply the channel to an SCO
%
%   [scoOut, channelOut]  = apply( theChannel, scoIn );
%
%       theChannel - SimulationChannel object to apply to scoIn
%       scoIn - SubCarrierObject to which the the channel is to be applied
%
%       scoOut - The SCO resulting from applying theChannel to scoIn
%       channelOut - theChannel after application to scoIn
%
%   Note that a channel may have state that is updated when it is applied to an
%   SCO, hence the need for channelOut

% (c) 2016 Cillian O'Driscoll

scoOut = scoIn;

channelOut = theChannel;

for ii = 1:length( theChannel.simBlocks )
    [ scoOut, tmp ]  = apply( theChannel.simBlocks{ii}, scoOut );
    channelOut.simBlocks{ii} = tmp;
end
