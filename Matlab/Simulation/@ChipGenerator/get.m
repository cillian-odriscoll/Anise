function val = get( chipGen, prop )
%
% get - Get a property of the ChipGenerator
%
%   val = get( chipGen, prop )
%
%       chipGen - A ChipGenerator object
%       prop - One of 'tc' or 'codelength'
%
%       val - The value of the property
%
%   Note 'tc' is the chip period and 'codelength' is the code length.

% (c) 2016 Cillian O'Driscoll

if strcmp( lower( prop ), 'tc' )
    val = chipGen.Tc;
elseif strcmp( lower(prop), 'codelength' )
    val = chipGen.codeLength;
else
    val = [];
end

