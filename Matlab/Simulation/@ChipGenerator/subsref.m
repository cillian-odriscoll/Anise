function varargout = subsref( chipGen, idx )
%
% subsref - Define subscripted references for ChipGenerator objects
%
%   This is an internal utility function to access methods and properties on
%   class objects using dot notation:
%
%       e.g. using
%           Tc = chipGen.Tc;
%       rather than
%           Tc = get( chipGen, 'Tc' );

% (c) 2016 Cillian O'Driscoll

persistent methods__
persistent properties__

if isempty( methods__ )
    methods__.generate = @(o,tt)generate(o,tt);
end

if isempty( properties__ )
    properties__.Tc = @(o)get(o, 'Tc' );
    properties__.codeLength = @(o)get(o, 'codeLength' );
end

if idx(1).type ~= '.'
    disp( idx );
    error( [ 'Invalid index for ' class(chipGen) '. with: ' ] );
end

methodOrProperty = idx(1).subs;
out = cell( nargout, 1 );

if isfield( properties__, methodOrProperty ) && numel(idx) == 1
    % Now we have a property
    accessor = properties__.( methodOrProperty );
    [out{:}] = accessor( chipGen );
elseif isfield( methods__, methodOrProperty ) && numel(idx) > 1
    if idx(2).type ~= '()'
        error( [ 'Bad call to method ' methodOrProperty idx(2).type ] );
    end

    args=idx(2).subs;
    fhandle = methods__.(methodOrProperty);

    %disp( [ 'NARGOUT: ' num2str( nargout ) ] );

    if isempty(args)
        [out{:}] = fhandle(chipGen);
    else
        [out{:}] = fhandle(chipGen, args{:});
    end

else
    % Uh oh:
    error( [ 'Unknown property/method: ' methodOrProperty ] );
end

varargout = out;


