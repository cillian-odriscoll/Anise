function chipGen = ChipGenerator( varargin )
%
% ChipGenerator - Class implementing a generic chip (+/-1) generator
%
%     chipGen = ChipGenerator( theChips, Tc )
%         theChips - vector of code chips
%         Tc - The chip period
%
%         chipGen - A chip generator that generates the periodic code
%         theChips
%
%     chipGen = ChipGenerator( chipGenFun, Tc )
%
%         chipGenFun - A function that generates code chips as a
%         function of code phase
%
%         Tc - The chip period
%
%     chipGen = ChipGenerator( chipGenArray );
%         chipGenArray - A cell array of ChipGenerators
%
%         chipGen - A chip generator that returns the product of the
%         array of generators passed in
%
%     E.g. For the GPS L1 C/A code:
%     fc = 1.023e6; Tc = 1/fc;
%     dataRate = 50; % bps
%     Td = 1/dataRate;
%
%     prn = 12;
%     theCode = 1 - 2*l1ca(prn);
%     codeGen = ChipGenerator( theCode, Tc );
%
%     dataGenFun = MakeRandomDataGenerator(Td);
%     dataGen = ChipGenerator( dataGenFun, Td );
%
%     caChipGen = ChipGenerator( { codeGen, dataGen } );
%
%     theChips = caChipGen.generate( 0:Tc:(50*1023*Tc) ); % Generate 50 chips worth

% (c) 2016 Cillian O'Driscoll

chipGen = struct();

chipGen.Tc = inf;
chipGen.codeLength = inf;

if nargin == 1
    if isa( varargin{1}, 'ChipGenerator' ) % copy constructor
        chipGen = varargin{1};
        return;
    elseif isnumeric( varargin{1} ) && isscalar( varargin{1} )
        chipGen.Tc = varargin{1};
    else
        error( 'ChipGenerator: Unexpected argument' );
    end
elseif nargin == 2
    if isnumeric( varargin{1} ) && isscalar( varargin{1} ) && ...
        isnumeric( varargin{2} ) && isscalar( varargin{2} )
        chipGen.Tc = varargin{1};
        chipGen.codeLength = varargin{2};
    else
        error( 'ChipGenerator: arguments must be real scalars' );
    end
elseif nargin ~= 0
    error( 'ChipGenerator: unexpected number of arguments' );
end


chipGen = class( chipGen, 'ChipGenerator' );

