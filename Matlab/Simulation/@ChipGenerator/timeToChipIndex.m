function inds = timeToChipIndex( chipGen, tt )
%
% timeToChipIndex - Converts time to a chip index for a ChipGenerator
%
%   inds = timeToChipIndex( chipGen, tt )
%       chipGen - The ChipGenerator object for which to do the conversion
%       tt - Timestamps at which to compute chip indices
%
%       inds - the indices computed.

% (c) 2016 Cillian O'Driscoll

inds = int64(floor( tt/chipGen.Tc ) + 1);

if ~isinf( chipGen.codeLength )
    inds = mod( inds-1, int64(chipGen.codeLength) ) + 1;
end
