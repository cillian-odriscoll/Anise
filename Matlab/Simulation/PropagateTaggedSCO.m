function scoOut = PropagateTaggedSCO( tSCO, dt )
%
% PropagateTaggedSCO - Propagates the transmit time of a tagged SCO
%
%   scoOut = PropagateTaggedSCO( tSCO, dt );
%
%       tSCO - The tagged SCO to propagate.
%       dt - The time by which to propagate tSCO
%

% (c) 2016 Cillian O'Driscoll

scoOut = tSCO;

dtRx = dt*( 1 + tSCO.timeDilation );

scoOut.Ttx = tSCO.Ttx + dt;
scoOut.Trx = tSCO.Trx + dtRx;

scoOut.phi0 = tSCO.phi0 + tSCO.phiDot * dt;
