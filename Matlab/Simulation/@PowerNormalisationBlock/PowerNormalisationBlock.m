function simBlock = PowerNormalisationBlock( varargin )
%
% PowerNormalisationBlock - Simulation block to normalise power
%
%   The PowerNormalisationBlock is a SimulationBlock that normalises the power
%   of subcarrier objects.

%   A subcarrier object is an energy signal, not a power signal, however, the
%   SCO objects in Anise form the basis for signal generation. This function
%   tells us the total power in the resultant signal generated from this
%   subcarrier object.
%
%   A PowerNormalisationBlock is constructed as follows:
%
%       normBlock = PowerNormalisationBlock();
%
%   And can be applied to an SCO as follows:
%
%       [ scoOut, normBlockOut ] = apply( normBlock, scoIn );
%
%   This block is useful for ensuring that the power output by a satellite after
%   filtering is correct. GNSS systems provides typically state maximum and
%   minimum received power in a given bandwidth, by filtering the generated
%   signal, normalising and then applying a PowerGainBlock, we can ensure the
%   desired in-band power levels are produced in an Anise simulation.
%
% See Also:
%   ComputeSCOPower, SimulationBlock, PowerGainBlock, FilterBlock

% (c) 2016 Cillian O'Driscoll

if nargin == 0
    simBlock = struct();
    baseBlock = SimulationBlock();

    simBlock = class( simBlock, 'PowerNormalisationBlock', baseBlock );
elseif nargin == 1
    if isa( varargin{1}, 'PowerNormalisationBlock' )
        simBlock = varargin{1};
    else
        error( 'PowerNormalisationBlock: unexpected parameter' );
    end
else
    error( 'PowerNormalisationBlock: unexpected number of parameters' );
end
