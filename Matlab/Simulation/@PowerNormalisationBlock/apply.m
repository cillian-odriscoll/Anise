function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the power normalisation block to an SCO
%
%       [scoOut, simBlockOut] = apply( simBlock, scoIn );
%
%       simBlock - The PowerNormalisationBlock to apply to scoIn
%       scoIn - The subcarrier object whose power is to be normalised
%
%       scoOut - The normalised SCO
%       simBlockOut - The simulation block after the normalisation
%
% See Also:
%   ComputeSCOPower, NormaliseSCO

% (c) 2016 Cillian O'Driscoll

scoOut = NormaliseSCO( scoIn );

simBlockOut = simBlock;
