function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the power gain block to an SCO
%
%   [scoOut, simBlockOut] = apply( simBlock, scoIn );
%
%       simBlock - The Power Gain block to apply to scoIn
%       scoIn - The subcarrier object to which to apply the power gain block
%
%       scoOut - The subcarrier object output by the power gain block.
%       simBlockOut - The power gain block after application to the SCO
%
% See Also:
%   PowerGainBlock

% (c) 2016 Cillian O'Driscoll

gaindB = simBlock.powerGainTrajectory.ValueAt( scoIn.Trx );

scoOut = scoIn;

scoOut.scaleFactor = scoOut.scaleFactor * 10^(0.05*gaindB );

simBlockOut = simBlock;
