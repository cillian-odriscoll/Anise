function simBlock = PowerGainBlock( varargin )
%
% PowerGainBlock - Class implementing a power gain simulation block
%
%   The PowerGainBlock is a SimulationBlock that applies a power gain to a
%   subcarrier object.
%
%   It can be constructed from a simple constant:
%
%       powerGainBlock = PowerGainBlock( 10 );
%
%       where 10 here is the power gain in dB.
%
%   Or from a Trajectory object:
%
%       powerGainBlock = PowerGainBlock( gainTrajectory );
%
%       where gainTrajectory is a Trajectory object that returns the gain in dB
%       as a function of the receive time of the sco input to the power gain
%       block.
%
%   The PowerGainBlock can be applied to a subcarrier object as follows:
%
%       [ scoOut, powerGainBlockOut ] = apply( powerGainBlock, scoIn );
%
%       where scoIn is the subcarrier object to which to apply the power gain
%       scoOut is the subcarrier object with the power gain applied
%       powerGainBlockOut - the PowerGainBlock after computing scoOut.
%
% See Also:
%   SimulationBlock, Trajectory

% (c) 2016 Cillian O'Driscoll

if nargin == 0
    gainTrj = ConstantTrajectory( 1 );
elseif nargin == 1
    if isa( varargin{1}, 'PowerGainBlock' ) % copy constructor
        simBlock = varargin{1};
        return;
    elseif isa( varargin{1}, 'Trajectory' )
        gainTrj = varargin{1};
    elseif isnumeric( varargin{1} )
        gainTrj = ConstantTrajectory( varargin{1} );
    else
        error( 'PowerGainBlock: unexpected parameter' );
    end
else
    error( 'PowerGainBlock: unexpected number of parameters' );
end

simBlock.powerGainTrajectory = gainTrj;

baseBlock = SimulationBlock();

simBlock = class( simBlock, 'PowerGainBlock', baseBlock );

