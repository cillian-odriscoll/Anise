function simBlock = CIRBlock( varargin )
%
% CIRBlock - Channel Impulse Response simulation block
%
%   A CIRBlock is a SimulationChannel object which models a Channel Impulse
%   Response. This is useful for modelling multipath fading channels.
%
%   A CIRBlock is constructed as follows:
%
%       cirBlock = CIRBlock( theCIR );
%
%       or
%
%       cirBlock = CIRBlock( theCIR, 'method' );
%
%   where theCIR is either:
%
%       a) A cell array, where each element is a matrix with at least two
%       columns and one row for each multipath ray. The columns represent the
%       following data:
%
%           Delay relative to LOS [s], Complex Amplitude[, Ray ID]
%
%       b) A struct array where each element is a vector 'vec' of form:
%
%           [ delay_1, amplitude_1, delay_2, amplitude_2, ...
%               delay_N, amplitude_N ]
%
%   and method is a string, and equal to one of:
%
%       'direct' - [Default] To apply the CIR directly to the SCO
%       'filter' - To generate a tapped delay line filter model from theCIR at
%                  each epoch, and apply that to the SCO.
%
%   At present there is no timing information associated with theCIR, as such
%   the CIRBlock applies the first element of the cell/struct arrary the first
%   time it is called, the second element the second time and so on. It is up to
%   the user to ensure that the simulation tick period matches the data period
%   in the CIR structure.
%
%   This is API is not stable as we need a more robust solution to the timing
%   issue.

% (c) 2016 Cillian O'Driscoll

simBlock.method = 'direct';

if nargin == 0
    simBlock.CIR = {};
elseif nargin == 1
    if isa( varargin{1}, 'CIRBlock' ) % copy constructor
        simBlock = varargin{1};
    elseif ~iscell(varargin{1}) && ~isstruct( varargin{1} )
        error( 'CIRBlock: unexpected parameter' );
    end
elseif nargin > 2
    error( 'CIRBlock: unexpected number of parameters' );
end

if nargin >= 1
    if iscell( varargin{1} ) || isstruct( varargin{1} )
        simBlock.CIR = varargin{1};
    else
        error( 'CIRBlock: First argument should be a cell array');
    end
end

if nargin == 2
    if isstr( varargin{2} )
        simBlock.method = varargin{2};
    else
        error( 'CIRBlock: second argument should be a string');
    end
end


simBlock.prevIdx = 0;
simBlock.numEl = length( simBlock.CIR );

baseBlock = SimulationBlock();

simBlock = class( simBlock, 'CIRBlock', baseBlock );
