function [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
% apply - Apply the CIR block to an SCO
%
%    [scoOut, simBlockOut] = apply( simBlock, scoIn )
%
%       simBlock - The CIR block to apply to scoIn
%       scoIn - The subcarrier object at the input to the CIR block
%   
%       scoOut - The SCO at the output of the CIR block
%       simBlockOut - The simlution block after application to the input SCO
%
%   Note that the CIR block has state, hence the need for simBlockOut

% (c) 2016 Cillian O'Driscoll

nextIdx = simBlock.prevIdx + 1;

if nextIdx > simBlock.numEl
    theCIR = [ 0, 1, 0 ];
else
    if iscell( simBlock.CIR )
        theCIR = simBlock.CIR{ nextIdx };
    else
        theVec = simBlock.CIR( nextIdx ).vec;
        theCIR = [ theVec(1:2:end); theVec(2:2:end) ].';
    end
end

scoOut = ApplyCIR( scoIn, theCIR, simBlock.method );

simBlock.prevIdx = nextIdx;
simBlockOut = simBlock;

