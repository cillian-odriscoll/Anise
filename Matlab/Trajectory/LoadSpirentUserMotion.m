function UserMotion = LoadSpirentUserMotion( userMotionFile, interpMethod )
%
% LoadSpirentUserMotion - Loads a Spirent user motion csv file
%
%   UserMotion = LoadSpirentUserMotion( userMotionFile, interpMethod );
%
%       userMotionFile - The path to the user motion csv file to load
%       interpMethod - Optional method to use for trajectory interpolation (see interp1)
%       UserMotion - A struct containing a number of 'Trajectory'
%       objects which can be used to simulate the sv/rx channel
%
%       xTrj - ECEF x component [m] as a function of rx time
%       yTrj - ECEF y component [m] as a function of rx time
%       zTrj - ECEF z component [m] as a function of rx time
%
%       latTrj - User latitude [rad] as a function of rx time
%       lonTrj - User longitude [rad] as a function of rx time
%       hgtTrj - User height [m] as a function of rx time
%
%       speedTrj - User speed [m/s] as a function of rx time
%       headingTrj - User heading [rad] as a function of rx time
%
%       timeVec - List of time points at which the trajectories were calculated
%

% (c) 2016 Cillian O'Driscoll

if nargin < 2
    interpMethod = 'pchip';
end

rawData = csvread( userMotionFile, 3, 0 ); % Skip first two rows

UserMotion = struct();

if isempty( rawData )
    disp( [ 'User motion file is empty!' ] );
    return;
end


% Some indices:
% TODO: Update to use readtable when Octave supports it.

towIdx = 1;
xIdx = 2;
yIdx = 3;
zIdx = 4;
vxIdx = 5;
vyIdx = 6;
vzIdx = 7;
axIdx = 8;
ayIdx = 9;
azIdx = 10;
jxIdx = 11;
jyIdx = 12;
jzIdx = 13;
latIdx = 14;
lonIdx = 15;
hgtIdx = 16;
hdgIdx = 17;

tt = rawData( :, towIdx )/1000.0;

spd = sqrt( sum( rawData(:,vxIdx:vzIdx).^2, 2 ) );

UserMotion.xTrj = InterpolatingTrajectory( tt, rawData(:,xIdx), interpMethod );
UserMotion.yTrj = InterpolatingTrajectory( tt, rawData(:,yIdx), interpMethod );
UserMotion.zTrj = InterpolatingTrajectory( tt, rawData(:,zIdx), interpMethod );

UserMotion.latTrj = InterpolatingTrajectory( tt, rawData(:,latIdx), interpMethod );
UserMotion.lonTrj = InterpolatingTrajectory( tt, rawData(:,lonIdx), interpMethod );
UserMotion.hgtTrj = InterpolatingTrajectory( tt, rawData(:,hgtIdx), interpMethod );

UserMotion.speedTrj = InterpolatingTrajectory( tt, spd, interpMethod );
UserMotion.headingTrj = InterpolatingTrajectory( tt, rawData(:,hdgIdx), interpMethod );

UserMotion.timeVec = tt;
