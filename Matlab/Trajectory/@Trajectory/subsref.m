function varargout = subsref( Trj, idx )
%
% subsref - Define subscripted references for Trajectory objects
%
%   This is an internal utility function to access methods and properties on
%   class objects using dot notation:
%
%       e.g. using
%           yv = Trj.ValueAt( xv );
%       rather than
%           yv = ValueAt( Trj, xv );

% (c) 2016 Cillian O'Driscoll

persistent methods__
persistent properties__

if isempty( methods__ )
    methods__.ValueAt = @(o,xv)ValueAt(o,xv);
end

if idx(1).type ~= '.'
    error( [ 'Invalid index for ' class(Trj) '. with: ' idx ] );
end

methodOrProperty = idx(1).subs;

if isfield( properties__, methodOrProperty ) && numel(idx) == 1
    % Now we have a property
    varargout{1} = Trj( methodOrProperty );
elseif isfield( methods__, methodOrProperty ) && numel(idx) > 1
    if idx(2).type ~= '()'
        error( [ 'Bad call to method ' methodOrProperty idx(2).type ] );
    end

    args=idx(2).subs;
    fhandle = methods__.(methodOrProperty);

    if isempty(args)
        varargout{1} = fhandle(Trj);
    else
        varargout{1} = fhandle(Trj,args{:});
    end

else
    % Uh oh:
    error( [ 'Unknown property/method: ' propertyOrMethod ] );
end



