function Trj = Trajectory( varargin )
%
% Trajectory - Abstract base class representing a trajectory
%
% A Trajectory object is one which gives the value of a dependent parameter as a
% function of a single independent parameter. In Anise the independent parameter
% is typically time.
%
% Given a Trajectory object, Trj, then the value of the dependent parameter can
% be determined using:
%
%   yv = ValueAt( Trj, xv );
%
%       Trj - Trajectory object
%       xv - The value of the independent parameter
%
%       yv - The value of the dependent parameter.
%
% Examples of Trajectory objects include range, range rate, power levels, angles
% of arrival, TEC, etc.
%
%

% (c) 2016 Cillian O'Driscoll

if nargin == 1
    if isa( varargin{1}, 'Trajectory' )
        Trj = varargin{1};
    else
        error( [ 'Trajectory: unexpected argument: ' varargin{1} ] );
    end
elseif nargin == 0
    Trj = struct();
    Trj = class( Trj, 'Trajectory' );
else
    error( [ 'Trajectory: too many arguments' ] );
end

