function SatData = LoadSpirentSatData( satDataFile, gnss, prn, interpMethod )
%
% LoadSpirentSatData - Loads satellite data from a Spirent SatData csv
% file
%
%   SatData = LoadSpirentSatData( satDataFile, gnss, prn );
%
%       satDataFile - The path to the csv file to load.
%       gnss - A string identifying the GNSS system of the sv to load
%       prn - The prn number of the satellite to load
%
%  SatData - Struct containing a list of 'Trajectory' objects which can
%  be used to simulate the sv/rx channel for the given gnss/prn. 
%
%       rangeTrj - Trajectory object given the range [m] as function of transmit time 
%       rangeRateTrj - Gives the range rate [m/s] as a function of transmit time
%       tecuTrj - Total Electron Content [TECU] as a function of transmit time
%       azTrj - SV azimuth [rad] as a function of transmit time
%       elTrj - SV elevation [rad] as a function of transmit time 
%       tropoTrj - Tropospheric delay [s] as a function of transmit time
%       L1PowerTrj - L1 power [dBW] as a function of transmit time
%       L2PowerTrj - L2 power [dBW] as a function of transmit time
%       L5PowerTrj - L5 power [dBW] as a function of transmit time
%       satBoreAngleTrj - Off-boresight angle at SV [rad] as a function of transmit time.
%       
%   For example, to determine the range at transmit time Ttx:
%
%       rangeTtx = ValueAt( SatData.rangeTrj, Ttx );
%
% See Also:
%   Trajectory

% (c) 2016 Cillian O'Driscoll

if nargin < 3
    error( 'Need at least 3 arguments' );
end

rawData = csvread( satDataFile, 3, 0 ); % Skip first two rows

gnssId = StringToSpirentSysId( gnss );

if gnssId < 0
    error( [ 'Unknown GNSS system: ' gnss '. Sorry!' ] );
end

% Some indices:
%TODO: update to use readtable, when Octave supports it
towIdx = 1;
sysIdx = 3;
prnIdx = 5;
satXIdx = 7;
satYIdx = 8;
satZIdx = 9;
echoNumIdx = 6;
elIdx = 14;
rangeIdx = 15;
pseudoRangeL1Idx = 16;
rangeRateIdx = 30;
ionoDelayL1Idx = 20;
tropoDelayIdx = 23;
sigPowerdBL1Idx = 25;
sigPowerdBL2Idx = 26;
sigPowerdBL5Idx = 27;
antAzIdx = 28;
antElIdx = 29;

if nargin < 4
    interpMethod = 'pchip';
end


prnData = rawData( rawData(:,sysIdx) == gnssId & rawData(:,prnIdx) == prn ...
    & rawData(:,echoNumIdx) == 0, : );

SatData = struct();

if isempty( prnData )
    disp( [ 'No data for ' gnss ' PRN: ' num2str( prn ) ] );
    return
end

% Extract time for convenience: (note conversion from ms to seconds)
tt = prnData(:, towIdx)/1000;
SatData.timeVec = tt;

% Now we make some trajectory objects:
SatData.rangeTrj = InterpolatingTrajectory( tt, prnData(:,rangeIdx), interpMethod );
SatData.rangeRateTrj = InterpolatingTrajectory( tt, prnData(:,rangeRateIdx), interpMethod );

SatData.L1PseudoRangeTrj = InterpolatingTrajectory( tt, prnData(:,pseudoRangeL1Idx), interpMethod );

%Iono - Extract TECU:
kLightSpeed = 299792458;
TECU = prnData(:,ionoDelayL1Idx)*kLightSpeed*(1575.42e6/10^8)^2/40.3;
SatData.tecuTrj = InterpolatingTrajectory( tt, TECU, interpMethod );
SatData.tropoTrj = InterpolatingTrajectory( tt, prnData(:,tropoDelayIdx ), interpMethod );
SatData.azTrj = InterpolatingTrajectory( tt, prnData(:,antAzIdx), interpMethod );
SatData.elTrj = InterpolatingTrajectory( tt, prnData(:,antElIdx), interpMethod );

SatData.L1PowerTrj = InterpolatingTrajectory( tt, prnData(:,sigPowerdBL1Idx), interpMethod );
SatData.L2PowerTrj = InterpolatingTrajectory( tt, prnData(:,sigPowerdBL2Idx), interpMethod );
SatData.L5PowerTrj = InterpolatingTrajectory( tt, prnData(:,sigPowerdBL5Idx), interpMethod );

orbitalRadius = sqrt( sum( prnData(:,satXIdx:satZIdx).^2, 2 ) );
offBoreAngle = pi/2 - prnData(:,elIdx) - asin( prnData(:,rangeIdx)./orbitalRadius ...
    .* cos( prnData(:,elIdx) ) );

SatData.satBoreAngleTrj = InterpolatingTrajectory( tt, offBoreAngle, interpMethod );

end

function spirentSysId = StringToSpirentSysId( gnss )
% StringToSpirentSysId - conver a string to a spirent GNSS ID

spirentSysId = -1;
% TODO: Add more here:
if strcmpi( gnss, 'gps' )
    spirentSysId = 0;
elseif strcmpi( gnss, 'gal' ) || strcmpi( gnss, 'galileo' )
    spirentSysId = 9;
end

end
