function yv = ValueAt( Trj, xv )

% ValueAt - Get the value of the ConstantTrajectory at a point xv
%
%   yv = ValueAt( Trj, xv )
%
%   Returns yv = k, where k is the constant value set when constructing the
%   ConstantTrajectory object.
%
%   See Also
%       Trajectory, ConstantTrajectory

yv = Trj.k;
