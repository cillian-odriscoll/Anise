function Trj = ConstantTrajectory( varargin )
%
% ConstantTrajectory - Class implementing a constant trajectory
%
% The ConstantTrajectory object is a Trajectory object that always returns the
% same value.
%
% It is generated as follows:
%
%   constTrj = ConstantTrajectory( 10 );
%
% In this case every call to:
%
%   ValueAt( constTrj, t )
%
% will return 10, irrespective of t.

% (c) 2016 Cillian O'Driscoll


if nargin == 0
    Trj.k = 0;
    Trj = class( Trj, 'ConstantTrajectory' );
elseif nargin == 1
    if strcmp( class( varargin{1} ), 'ConstantTrajectory' )
        Trj = varargin{1};
    else
        Trj.k = varargin{1};
        bTrj = Trajectory();
        Trj = class( Trj, 'ConstantTrajectory', bTrj );
    end
else
    error( [ 'ConstantTrajectory: expecting a constant input' ] );
end

