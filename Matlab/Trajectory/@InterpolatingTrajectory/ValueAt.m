function yv = ValueAt( Trj, xv )
%
% ValueAt - Compute the value of the trajectory at a point
%
%   yv = ValueAt( Trj, xv );
%
%   Interpolates the x and y values at construction to give the value yv at the
%   point xv.
%
% See Also:
%   InterpolatingTrajectory, Trajectory, interp1

% (c) 2016 Cillian O'Driscoll

if isa( xv, 'GnssTime' )
    xx = TOW(xv);
else
    xx = xv;
end

yv = ppval( Trj.pp, xx );
