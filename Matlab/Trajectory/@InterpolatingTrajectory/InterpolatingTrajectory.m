function Trj = InterpolatingTrajectory( varargin )
%
% InterpolatingTrajectory - Class implementing an interpolating trajectory
%
% InterpolatingTrajectory is a Trajectory object that interpolates a given set
% of data points.
%
%   interpTrj = InterpolatingTrajectory( xx, yy );
%
%       xx - Set of x values
%       yy - Set of y values
%
% Generates an interpolation so that
%
%   yv = ValueAt( interpTrj, xv );
%
% Will interpolate the xx yy data to obtain the values yv at the points xv.
%
%   interTrj = InterpolatingTrajectory( xx, yy, method );
%
%   Creates a trajectory using the interpolation method given by the string
%   'method'. This defaults to 'spline'
%
% See Also:
%   Trajectory, interp1

% (c) 2016 Cillian O'Driscoll

if nargin < 3
    Trj.method = 'spline';
else
    Trj.method = varargin{3};
end

if nargin < 2
    Trj.y = [];
else
    Trj.y = varargin{2};
end

if nargin < 1
    Trj.x = [];
else
    Trj.x = varargin{1};
end


if length(Trj.x) ~= length(Trj.y)
    error( [ 'Vectors x and y should have the same length']  );
end

if nargin == 1
    Trj = varargin{1};
else
    bTraj = Trajectory();
    Trj.pp = [];
    Trj = class( Trj, 'InterpolatingTrajectory', bTraj );
end

Trj.pp = interp1( Trj.x, Trj.y, Trj.method, 'pp' );
