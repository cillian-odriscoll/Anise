function gg = IsoFluxGainPattern( theta, orbitalRadius )
%
%  IsoFluxGainPattern - Antenna gain pattern providing constant flux on earth's surface
%
%   gg = IsoFluxGainPattern( theta, orbitalRadius );
%
%       theta - Off-nadir angle at the transmit antenna
%       orbitalRadius - The orbital radius at the tx antenna

% (c) 2016 Cillian O'Driscoll

% Earth radius
RE = 6378e3;
% The zenith angle at the earth's edge
thEdge = acos( sqrt( 1 - RE^2/orbitalRadius^2) );

cosTh = cos(theta);

% Range to user:
rU = orbitalRadius*cosTh - sqrt( RE^2 - (1-cosTh.^2) *orbitalRadius^2 );

% Gain relative to zenith (theta = 0) to ensure isoflux on Earth surface
gIso = rU/(orbitalRadius - RE);


minEl = 30*pi/180;
thMinEl = asin( RE/orbitalRadius*sin(pi/2+minEl) );

thMid = 0.25*thEdge + 0.75*thMinEl;
rUMid = orbitalRadius*cos(thMid) - sqrt( RE^2 - (1-cos(thMid).^2)*orbitalRadius^2 );
gMid = rUMid/(orbitalRadius-RE);

% However, we need to take care of roll-off after the earth edge. We model this
% as sinc(x)
rollOffGain = gMid/( abs( sinc( thMid/thEdge/2) ) );
gRollOff = abs( rollOffGain*sinc( theta/thEdge/2 ) );
dth = thEdge - thMinEl;

% Now we will interpolate between the two:
ath = abs(theta);

gg = zeros( size(theta) );
gg( ath > thEdge ) = gRollOff( ath > thEdge );
gg( ath<thMinEl ) = gIso( ath < thMinEl );

interpInds = find( ath <= thEdge & ath >= thMinEl );

gg(interpInds) = gIso( interpInds ) .* (  thEdge - ath(interpInds) )/dth + ...
    gRollOff( interpInds ) .* ( ath(interpInds) - thMinEl )/dth;

