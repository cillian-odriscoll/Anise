function theAnt = MakeAntennaObject( Hfo, G0, directivityFun )
%
% MakeAntennaObject - Construct an antenna object
%
%   theAnt = MakeAntennaObject( Hfo, G0, directivityFun );
%
%       Hfo - Frequency response object centered at the antenna centre frequency
%       G0  - The nominal gain [dB] along boresight
%       directivityFun - A function returning the directivity of the antenna
%
%   Note the directivityFun must be a function of form:
%
%       d = directivityFun( theta, phi );
%
%   Where d is the directivity, which may be complex, theta is the off-boresight angle in radians and
%   phi is the azimuth angle in radians.

% (c) 2016 Cillian O'Driscoll

theAnt = struct( 'directivityFun', directivityFun, ...
    'G0', G0, ...
    'Hfo', Hfo );

theAnt.getFrequencyResponse = @(theta,phi)GetAntennaFrequencyResponse(theAnt, theta, phi );
