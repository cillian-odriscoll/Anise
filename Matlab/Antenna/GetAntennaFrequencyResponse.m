function Hfo = GetAntennaFrequencyResponse( theAnt, theta, phi )
%
% GetAntennaFrequencyResponse - Get the freqeuncy response of an antenna for a given 'cut'
%
%   Hfo = GetAntennaFrequencyResponse( theAnt, theta, phi );
%
%       theAnt - Antenna object containing all the antenna parameters
%       theta - The offboresight (zenith) angle in radians.
%       phi - The azimuth angle in radians

% (c) 2016 Cillian O'Driscoll

Hfo = theAnt.Hfo; % Boresight Hfo

Hfo.y = Hfo.y .* 10^(0.05*( theAnt.G0 ) ).* theAnt.directivityFun( theta, phi );

