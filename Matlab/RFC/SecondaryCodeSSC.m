function ssc = SecondaryCodeSSC( f, sigma1, K1, sigma2, K2 )
%
% SecondaryCodeSSC - Compute SSC due to secondary code
%
%    ssc = SecondaryCodeSSC( f, sigma1, K1, sigma2, K2 )
%
%   f - Frequency offset
%   sigma1 - The interfering signal secondary code - sequence of +/- 1
%   K1 - The number of milliseconds in each element of the sigma1
%   sigma2 - The victim signal secondary code
%   K2 - The number of milliseconds in each element of sigma2
%
%   This computes the secondary modulation ssc as follows:
%
%   ssc(f) = 1 + 2* sum_k R_sigma1[ k * Khat/K1 ] R_sigma2[ k * KHat /K2 ] 
%       * cos 2*pi*f*k*Khat*To
%
%   Where Khat = LCM( K1, K2 ), To = 1 ms
%
% To account for a coherent integration time that is less than the duration
% of the secondary code, say TCoh = N *K2 * To, where N < length(sigma2),
% then one should simply pass in the first N elements of sigma2. This
% ensures that the integration is normalised by the coherent integration
% time in an appropriate manner.

% (c) 2016 Cillian O'Driscoll

To = 1e-3;
D1 = length( sigma1 );
D2 = length( sigma2 );
Khat = lcm( K1, K2 );
kMax = min( D1*K1/Khat, D2*K2/Khat ) - 1;

% Initialise to 1:
ssc = ones( size( f ) );

% Compute the autocorrelation function of the first sequence:
% zero padding here to compute the linear correlation. This should be
% modified for pilot sequences where the circular correlation ought to be
% computed
rs1 = ( ifft( fft(sigma1, 2*(length(sigma1))-1) .* conj( fft( sigma1, 2*(length(sigma1))-1 ) ) ) )/length( sigma1 );
rs1 = upsample( rs1, K1 );

rs2 = ( ifft( fft(sigma2, 2*(length(sigma2))-1) .* conj( fft( sigma2, 2*(length(sigma2))-1 ) ) ) )/length( sigma2 );
rs2 = upsample( rs2, K2 );


for ii = 1:length( f )
    for kk = 1:kMax
        ssc(ii) = ssc(ii) + 2*rs1(kk*Khat/K1+1)*rs2(kk*Khat/K2+1)*cos(2*pi*f(ii)*kk*Khat*To);
    end
end
