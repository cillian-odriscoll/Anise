function ssc = ComputeSSC( psd1Fun, psd2Fun, BT1, BT2, BR, Df )

% ComputeSSC - Compute the spectral separation coefficient
%
% ssc = ComputeSSC( psd1Fun, psd2Fun, BT1, BT2, BR, Df )
%
%   psd1Fun - PSD of the reference signal (normalised to infinite bw)
%   psd2Fun - PSD of the interfering signal (normalised to infinite bw)
%   BT1 - Transmission bandwidth of reference signal (centered at reference)
%   BT2 - Transmission bandwidth of interfering signal (centered at interferer)
%   BR - Receiver bandwidth (centered at reference signal)
%   Df - fInterferer - fReference (optional: Default = 0)
%
%   Computes:
%       int_{fmin}^{fmax} psd1Norm( f ) psd2Norm( f - Df ) df
%
%   Where:
%       psdXNorm = psd1Fun(f)/int_{-BTX/2}^{BTX/2} psdXFun(v) dv  -BTX/2 < f < BTX/2
%                = 0 otherwise
%       fmin = max( -BR/2, -BT1/2, (Df - BT2/2) )
%       fmax = min( BR/2, BT1/2, (Df + BT2/2) )
%

% (c) 2016 Cillian O'Driscoll

if nargin < 6
    Df = 0;
end

if nargin < 5
    BR = inf;
end

if nargin < 4
    BT2 = inf;
end

if nargin < 3
    BT1 = inf;
end

% Compute the normalisation based on the equivalent tx bandwidth
if isinf( BT1 )
    nrm1 = 1;
else
    nrm1 = quadgk( psd1Fun, -BT1/2, BT1/2, 'AbsTol',1e-26,'MaxIntervalCount', 8800 );
end

if isinf( BT2 )
    nrm2 = 1;
else
    nrm2 = quadgk( psd2Fun, -BT2/2, BT2/2, 'AbsTol',1e-26,'MaxIntervalCount', 8800 );
end

% Check for non-overlap:
if nrm1 == 0 || nrm2 == 0
    ssc = 0;
    return;
end

% Now compute the ssc

minFreq = max( [ -BR/2, -BT1/2, (Df-BT2/2) ] );
maxFreq = min( [ BR/2, BT1/2, (Df+BT2/2) ] );

if minFreq >= maxFreq
    ssc = 0;
    return;
end

if isinf( minFreq ) || isinf( maxFreq )
    ssc = quadgk( @(x) psd1Fun(x).*psd2Fun(x-Df), 0, inf, 'AbsTol',1e-30, 'MaxIntervalCount', 8800 )./nrm1./nrm2;
    ssc = ssc + quadgk( @(x) psd1Fun(x).*psd2Fun(x-Df), -inf, 0, 'AbsTol',1e-30, 'MaxIntervalCount', 8800 )./nrm1./nrm2;
else
    ssc = quadgk( @(x) psd1Fun(x).*psd2Fun(x-Df), minFreq, maxFreq, 'AbsTol',1e-30, 'MaxIntervalCount', 8800 )./nrm1./nrm2;
end
