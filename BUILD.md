# Anise Build Guide

Anise uses the `cmake` system to build the core library, which means the same
code can be used on linux, Windows and Mac OS X. Once you have acquired a copy
of the Anise source code, then the library can be built as follows:

Building libanise in Linux/Mac OS X
-------------------------

First install [boost](http://www.boost.org) using your favourite package manager
(`apt-get`, `yum`, `zypper`, `homebrew`, `macports`) making sure to install the
developer libraries (often called something along the lines of `boost-devel`).

If you want to develop with Anise, it is recommended to also install the
[ CppUnit ]( https://sourceforge.net/projects/cppunit/ ) testing framework.

Once these pre-requisites are installed then `libanise` can be built as follows:

```sh
$ cd path/to/Anise
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ../
$ make
```

Note that this requires an installation of CMake and a functioning C++ compiler.

Once the library has been built, it can optionally be installed on the system,
or simply kept in the `Anise` folder for further use.

```sh
$ make install
```

Building libanise in Windows
---------------------------

Building on Windows is a bit trickier, if you are uncomfortable with building
software on Windows, try obtaining a pre-compiled version of Anise.

First install [Visual Studio](https://www.visualstudio.com), Anise has been
tested with Visual Studio 2015, no guarantees can be made about other versions.

Next, install [boost](http://www.boost.org). Please ensure that you have 64-bit
versions of Visual Studio, Boost and Matlab/Octave. Or, if any of these are
32-bit, they must all be 32-bit.

Once boost has been installed an built, make a note of the boost root folder on
your machine, we'll call this `BOOST_FOLDER` in the following.

Next install [CMake]( https://cmake.org/ ).

Now CMake is going to be used to generate a Visual Studio project, but to do
this CMake will need to be informed about the location of boost. To do this,
open a Visual Studio command prompt, which can be found in the Visual Studio
start menu item. If you are building the 64 bit version make sure you open the
"x64 Native Tools Command Prompt", or if you are building a 32 bit version
then open the "x86 Native Tools Command Prompt".

From within the prompt, navigate to the Anise folder:

```dos
C:\path\to\Anise\>mkdir build
C:\path\to\Anise\>cd build
C:\path\to\Anise\build\>cmake -DBOOST_ROOT=BOOST_FOLDER\boost ^
    -DBoost_USE_STATIC_LIBS=ON ^
    -DBOOST_LIBDIR=BOOST_FOLDER\stage\libs ^
    -DBOOST_INCLUDE_DIR=BOOST_FOLDER ^
    -G "Visual Studio 14 2015 Win64" ..\
```

Note that this uses a Visual Studio 2015 Generator (`-G`) and a 64 bit build.
The command may need to be adjusted for 32-bit builds, refer to the CMake
website for details.

This will generate a Visual Studio Solution `Anise.sln` in the `build` folder.
Open this solution in Visual Studio and press `F7`, or select `Build>Build
Solution` from the menu bar. With luck, this will build `libanise.dll` and place
it in the folder

```dos
C:\path\to\Anise\build\lib\Release\libanise.dll
```

Building the mex files (all systems)
-------------------------------------

Finally, open Matlab or Octave (or both) and run:

```matlab
>> cd path/to/Anise
>> build_mex( path/to/folder/containing/libanise )
>> addpath( genpath( 'path/to/Anise/Matlab' ) )
>> savepath % Optional! This will save the path for future sessions
```

If you have installed `libanise` the argument to `build_mex` can be omitted. For
Mac OS X or linux, assuming you followed the instructions above, the
`path/to/folder/containing/libanise` should be `path/to/anise/build/lib`. On
windows the path should be `path\to\anise\build\lib\Release`.

This will build the mex files used by Matlab/Octave to access `libanise`, add
the Anise/Matlab folder to your path and then save the path for future use.

You are then ready to start using Anise.


