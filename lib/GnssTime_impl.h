#ifndef INCLUDED_GNSS_TIME_IMPL_H
#define INCLUDED_GNSS_TIME_IMPL_H

#include <Anise/GnssTime.h>

namespace Anise {

    class GnssTimeIntervalImpl {
        private:
            // Prevent unitialised construction
            GnssTimeIntervalImpl(){};

            int32_t mWeeks;

            int64_t mSeconds;

            double mFractionalSeconds;

            ///! Normalise: ensure that all numbers are within their repsective ranges:
            void Normalise( void );

        public:

            //! Initialising constructor
            GnssTimeIntervalImpl( int32_t mWeeks, int64_t mSeconds, double mFractionalSeconds );

            //!
            // Get the time interval in seconds.
            // WARNING: this may cause overflow if the interval is large:
            double AsSeconds( void ) const;

            //!
            // Get the time interval in weeks
            // This returns the whole integer number of weeks in the time interval
            int AsWeeks( void ) const;

            GnssTimeIntervalImpl &operator=( GnssTimeIntervalImpl const& otherInterval );
            GnssTimeIntervalImpl &operator+=( GnssTimeIntervalImpl const& dT );
            GnssTimeIntervalImpl &operator-=( GnssTimeIntervalImpl const& dT );

            friend GnssTimeIntervalImpl operator+( GnssTimeIntervalImpl lhs, GnssTimeIntervalImpl const& rhs );
            friend GnssTimeIntervalImpl operator-( GnssTimeIntervalImpl lhs, GnssTimeIntervalImpl const& rhs );

            friend bool operator==( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );
            friend bool operator!=( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );

            friend bool operator<( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );
            friend bool operator>( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );
            friend bool operator<=( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );
            friend bool operator>=( GnssTimeIntervalImpl const &lhs, GnssTimeIntervalImpl const& rhs );

            friend std::ostream &operator<<( std::ostream &os, GnssTimeIntervalImpl const& rhs );


    }; // GnssTimeIntervalImpl


    class GnssTimeImpl {

        private:
            GnssTimeInterval mTimeSinceEpoch;

            GnssSystem mSystem;

        public:
            GnssTimeImpl( GnssSystem const &sys );

            GnssTimeImpl( GnssSystem const &sys, GnssTimeInterval const &timeInterval );

            //! Get the system:
            GnssSystem const& System(void) const;

            int Week( void ) const;

            double TOW( void ) const;

            GnssTimeImpl &operator=( GnssTimeImpl const& rhs );
            GnssTimeImpl &operator+=( GnssTimeInterval const& rhs );
            GnssTimeImpl &operator-=( GnssTimeInterval const& rhs );

            friend GnssTimeInterval operator-( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );
            friend GnssTimeImpl operator-( GnssTimeImpl lhs, GnssTimeInterval const& rhs );
            friend GnssTimeImpl operator+( GnssTimeImpl lhs, GnssTimeInterval const& rhs );

            friend bool operator==( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );
            friend bool operator!=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );

            friend bool operator<( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );
            friend bool operator>( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );
            friend bool operator<=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );
            friend bool operator>=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs );

            friend std::ostream &operator<<( std::ostream &os, GnssTimeImpl const& rhs );


    }; // GnssTimeImpl

} // namespace Anise

#endif // #ifndef INCLUDED_GNSS_TIME_IMPL_H
