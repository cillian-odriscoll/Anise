#include <cppunit/TestAssert.h>
#include "qa_ChipConv.h"
#include <Anise/ChipConv.h>

namespace Anise {

    void qa_ChipConv::test_length_1()
    {
        // We should be able to pass in a pulse
        //
        std::vector< std::complex< double > > chip_shape = { 1 };

        std::vector< double > the_chips = { 1 };

        std::vector< std::complex< double > > bb_sig = { 0 };

        double tau0 = 0; // Initial phase is zero
        double tauChip = 1.0; // One chip per sample
        double dTau = 1.0; // One output sample per sample


        chip_conv_crc( &chip_shape[0], chip_shape.size(),
                &the_chips[0], the_chips.size(),
                tau0, tauChip, dTau,
                &bb_sig[0], bb_sig.size() );
        
        CPPUNIT_ASSERT_EQUAL( bb_sig[0], chip_shape[0]*the_chips[0] );
    }

    void qa_ChipConv::test_insufficient_chips()
    {
        // We should be able to pass in a pulse
        //
        std::vector< std::complex< double > > chip_shape = { 1 };

        std::vector< double > the_chips = { 1 };

        std::vector< std::complex< double > > bb_sig = { 0, 0, 0 };

        double tau0 = 0; // Initial phase is zero
        double tauChip = 1.0; // One chip per sample
        double dTau = 1.0; // One output sample per sample

        CPPUNIT_ASSERT_THROW(
            chip_conv_crc( &chip_shape[0], chip_shape.size(),
                    &the_chips[0], the_chips.size(),
                    tau0, tauChip, dTau,
                    &bb_sig[0], bb_sig.size() ),
            std::out_of_range );
    }


} /* namespace anise */


