#ifndef INCLUDED_QA_ANISE_H
#define INCLUDED_QA_ANISE_H

#include <cppunit/TestSuite.h>

//! collect all the tests for the gr-filter directory

class qa_anise
{
 public:
  //! return suite of tests for all of gr-filter directory
  static CppUnit::TestSuite *suite();
};

#endif // INCLUDED_QA_ANISE_H

