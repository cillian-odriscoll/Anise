#include <Anise/GnssTime.h>
#include "GnssTime_impl.h"
#include <cmath>

namespace Anise{

    const int64_t SECONDS_PER_HOUR = 3600;
    const int64_t SECONDS_PER_DAY = 24*SECONDS_PER_HOUR;
    const int64_t SECONDS_PER_WEEK = 7*SECONDS_PER_DAY;

    GnssTimeIntervalImpl::GnssTimeIntervalImpl( int32_t weeks, int64_t seconds, double fractionalSeconds )
        : mWeeks( weeks ),
        mSeconds( seconds ),
        mFractionalSeconds( fractionalSeconds )
    {
        Normalise();
    }

    void GnssTimeIntervalImpl::Normalise( void )
    {

        int64_t extraSeconds = static_cast< int64_t >( std::floor( mFractionalSeconds ) );

        mSeconds += extraSeconds;
        mFractionalSeconds -= static_cast< double >( extraSeconds );

        int32_t extraWeeks = mSeconds / SECONDS_PER_WEEK;

        mWeeks += extraWeeks;

        mSeconds -= static_cast< int64_t >( extraWeeks * SECONDS_PER_WEEK );

        if( ( mSeconds < 0 && mWeeks > 0 ) ){
            mSeconds += SECONDS_PER_WEEK;
            mWeeks -= 1;
        }
        else if( mSeconds >0 && mWeeks < 0 ){
            mSeconds -= SECONDS_PER_WEEK;
            mWeeks += 1;
        }

    }

    double GnssTimeIntervalImpl::AsSeconds( void ) const
    {
        double seconds = mFractionalSeconds;

        seconds += static_cast< double >( mSeconds );

        if( mWeeks != 0 ){
            seconds += static_cast< double >( mWeeks * SECONDS_PER_WEEK );
        }

        return seconds;
    }

    int GnssTimeIntervalImpl::AsWeeks( void ) const
    {
        return mWeeks;
    }

    GnssTimeIntervalImpl &GnssTimeIntervalImpl::operator=( GnssTimeIntervalImpl const& rhs )
    {
        mWeeks = rhs.mWeeks;
        mSeconds = rhs.mSeconds;
        mFractionalSeconds = rhs.mFractionalSeconds;
        return *this;
    }

    GnssTimeIntervalImpl &GnssTimeIntervalImpl::operator+=( GnssTimeIntervalImpl const& rhs )
    {
        mWeeks += rhs.mWeeks;
        mSeconds += rhs.mSeconds;
        mFractionalSeconds += rhs.mFractionalSeconds;
        Normalise();
        return *this;
    }

    GnssTimeIntervalImpl &GnssTimeIntervalImpl::operator-=( GnssTimeIntervalImpl const& rhs )
    {
        mWeeks -= rhs.mWeeks;
        mSeconds -= rhs.mSeconds;
        mFractionalSeconds -= rhs.mFractionalSeconds;
        Normalise();
        return *this;
    }

    GnssTimeIntervalImpl operator+( GnssTimeIntervalImpl lhs, GnssTimeIntervalImpl const& rhs )
    {
        lhs += rhs;
        return lhs;
    }

    GnssTimeIntervalImpl operator-( GnssTimeIntervalImpl lhs, GnssTimeIntervalImpl const& rhs )
    {
        lhs -= rhs;
        return lhs;
    }

    bool operator==( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const& rhs )
    {
        return (lhs.mWeeks == rhs.mWeeks && lhs.mSeconds == rhs.mSeconds &&
                lhs.mFractionalSeconds == rhs.mFractionalSeconds );
    }

    bool operator!=( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const& rhs )
    {
        return ! (lhs==rhs);
    }

    bool operator<( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const& rhs )
    {
        if( lhs.mWeeks < rhs.mWeeks )
        {
            return true;
        }

        if( lhs.mWeeks > rhs.mWeeks )
        {
            return false;
        }

        // Here weeks are the same

        if( lhs.mSeconds < rhs.mSeconds )
        {
            return true;
        }

        if( lhs.mSeconds > rhs.mSeconds )
        {
            return false;
        }

        // Here weeks and integer seconds are the same:
        return lhs.mFractionalSeconds < rhs.mFractionalSeconds;

    }

    bool operator>( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const&rhs )
    {
        return rhs<lhs;
    }

    bool operator<=( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const&rhs )
    {
        return !(lhs > rhs);
    }

    bool operator>=( GnssTimeIntervalImpl const&lhs, GnssTimeIntervalImpl const&rhs )
    {
        return !(lhs<rhs);
    }

    std::ostream &operator<<( std::ostream & os, GnssTimeIntervalImpl const& rhs )
    {
        if( rhs.mWeeks > 0 ){
            os << rhs.mWeeks << " Week" << ( rhs.mWeeks > 1 ? "s" : "" );
        }

        os << rhs.mSeconds << " Second" << ( rhs.mSeconds > 1 ? "s" : "" );

        double milliSeconds = rhs.mFractionalSeconds * 1000.0;

        os << milliSeconds;

        return os;
    }

    // This copy constructor ensures we do a deep copy of the mImpl member
    GnssTimeInterval::GnssTimeInterval( const GnssTimeInterval &rhs )
        : mImpl( std::make_shared< GnssTimeIntervalImpl >( *(rhs.mImpl) ) )
    {
        // Handled in initialiser
    }

#if defined(__cplusplus) && (__cplusplus >= 201103L)
    // This copy constructor works with temporary variables, allowing us to
    // simply re-use the mImpl created in the temporary variable
    GnssTimeInterval::GnssTimeInterval( GnssTimeInterval &&rhs )
        : mImpl( std::move( rhs.mImpl ) )
    {
        // Handled in initialiser
    }
#endif


    double GnssTimeInterval::AsSeconds( void ) const
    {
        return mImpl->AsSeconds();
    }

    int GnssTimeInterval::AsWeeks( void ) const
    {
        return mImpl->AsWeeks();
    }

    GnssTimeInterval GnssTimeInterval::Weeks( int numWeeks )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( numWeeks, 0, 0.0 ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::Days( int numDays )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, numDays*SECONDS_PER_DAY, 0.0 ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::Hours( int numHours )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, numHours*SECONDS_PER_HOUR, 0.0 ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::Seconds( double numSeconds )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, 0, numSeconds ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::MilliSeconds( double numMS )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, 0, numMS*1e-3  ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::MicroSeconds( double numUS )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, 0, numUS*1e-6  ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval GnssTimeInterval::NanoSeconds( double numNS )
    {
        std::shared_ptr< GnssTimeIntervalImpl > impl( new GnssTimeIntervalImpl( 0, 0, numNS*1e-9  ) );

        return GnssTimeInterval( impl );
    }

    GnssTimeInterval & GnssTimeInterval::operator=( GnssTimeInterval rhs )
    {
        std::swap( mImpl, rhs.mImpl );
        return *this;
    }

    GnssTimeInterval & GnssTimeInterval::operator+=( GnssTimeInterval const& rhs )
    {
        *(this->mImpl) += *(rhs.mImpl);
        return *this;
    }

    GnssTimeInterval & GnssTimeInterval::operator-=( GnssTimeInterval const& rhs )
    {
        *(this->mImpl) -= *(rhs.mImpl);
        return *this;
    }

    GnssTimeInterval operator+( GnssTimeInterval lhs, GnssTimeInterval const& rhs )
    {
        lhs += rhs;
        return lhs;
    }

    GnssTimeInterval operator-( GnssTimeInterval lhs, GnssTimeInterval const& rhs )
    {
        lhs -= rhs;
        return lhs;
    }

    bool operator==( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return *(lhs.mImpl) == *(rhs.mImpl);
    }

    bool operator!=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return !(lhs==rhs);
    }

    bool operator<( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return *(lhs.mImpl) < *(rhs.mImpl);
    }

    bool operator>( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return rhs < lhs;
    }

    bool operator<=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return !(lhs > rhs);
    }

    bool operator>=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs )
    {
        return !(lhs < rhs);
    }

    std::ostream &operator<<( std::ostream &os, GnssTimeInterval const& rhs )
    {
        os << *rhs.mImpl;
        return os;
    }

    GnssTimeImpl::GnssTimeImpl( GnssSystem const &sys )
        : mTimeSinceEpoch( GnssTimeInterval::Weeks(0) ),
        mSystem( sys )
    {}

    GnssTimeImpl::GnssTimeImpl( GnssSystem const &sys, GnssTimeInterval const&timeInterval )
        : mTimeSinceEpoch( timeInterval ),
        mSystem( sys )
    {}

    GnssSystem const & GnssTimeImpl::System( void ) const
    {
        return mSystem;
    }

    int GnssTimeImpl::Week( void ) const
    {
        return mTimeSinceEpoch.AsWeeks();
    }

    double GnssTimeImpl::TOW( void ) const
    {
        // Return the number of seconds since the start of this week:
        return ( mTimeSinceEpoch - GnssTimeInterval::Weeks( Week() ) ).AsSeconds();
    }

    GnssTimeImpl &GnssTimeImpl::operator=( GnssTimeImpl const& rhs )
    {
        mSystem = rhs.mSystem;
        mTimeSinceEpoch = rhs.mTimeSinceEpoch;
        return *this;
    }

    GnssTimeImpl &GnssTimeImpl::operator+=( GnssTimeInterval const& rhs )
    {
        mTimeSinceEpoch += rhs;
        return *this;
    }

    GnssTimeImpl &GnssTimeImpl::operator-=( GnssTimeInterval const &rhs )
    {
        mTimeSinceEpoch -= rhs;
        return *this;
    }


    GnssTimeInterval operator-( GnssTimeImpl const &lhs, GnssTimeImpl const &rhs )
    {
        /// TODO: cillian Find a better way of doing this
        if( lhs.mSystem != rhs.mSystem )
        {
            throw "Incompatible systems";
        }
        return lhs.mTimeSinceEpoch - rhs.mTimeSinceEpoch;
    }

    GnssTimeImpl operator-( GnssTimeImpl lhs, GnssTimeInterval const& rhs )
    {
        lhs -= rhs;
        return lhs;
    }

    GnssTimeImpl operator+( GnssTimeImpl lhs, GnssTimeInterval const& rhs )
    {
        lhs += rhs;
        return lhs;
    }

    bool operator==( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        /// TODO: Find a better way of doing this - use a system comparator? [ default assumes no clock drift between system?]
        return ( lhs.mSystem == rhs.mSystem &&
                lhs.mTimeSinceEpoch == rhs.mTimeSinceEpoch );

    }

    bool operator!=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        return !(lhs == rhs );
    }

    bool operator<( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        /// TODO: Find a better way of doing this:
        return ( lhs.mSystem == rhs.mSystem &&
                lhs.mTimeSinceEpoch < rhs.mTimeSinceEpoch );
    }

    bool operator>( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        return rhs < lhs;
    }

    bool operator<=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        return ! (lhs > rhs);
    }

    bool operator>=( GnssTimeImpl const &lhs, GnssTimeImpl const& rhs )
    {
        return ! (rhs > lhs );
    }

    std::ostream &operator<<( std::ostream &os, GnssTimeImpl const& rhs )
    {
        os << Anise::Systems::SystemToString( rhs.mSystem ) << " ";
        os << "Week: " << rhs.Week();
        os << " TOW: " << rhs.TOW();
        return os;
    }

    GnssTime::GnssTime()
        : mImpl( new GnssTimeImpl( Anise::Systems::UNKNOWN ) )
    {}

    GnssTime::GnssTime( GnssSystem const &sys )
        : mImpl( new GnssTimeImpl( sys ) )
    {}

    GnssTime::GnssTime( GnssSystem const &sys, GnssTimeInterval const & timeInterval )
        : mImpl( new GnssTimeImpl( sys, timeInterval ) )
    {}

    // This copy constructor ensures we do a deep copy of the mImpl member
    GnssTime::GnssTime( const GnssTime&rhs )
        : mImpl( std::make_shared< GnssTimeImpl >( *(rhs.mImpl) ) )
    {
        // Handled in initialiser
    }

#if defined(__cplusplus) && (__cplusplus >= 201103L)
    // This copy constructor works with temporary variables, allowing us to
    // simply re-use the mImpl created in the temporary variable
    GnssTime::GnssTime( GnssTime &&rhs )
        : mImpl( std::move( rhs.mImpl ) )
    {
        // Handled in initialiser
    }
#endif

    GnssSystem const &GnssTime::System( void ) const
    {
        return mImpl->System();
    }

    int GnssTime::Week( void ) const
    {
        return mImpl->Week();
    }

    double GnssTime::TOW( void ) const
    {
        return mImpl->TOW();
    }

    GnssTime & GnssTime::operator=( GnssTime const & rhs )
    {
        *mImpl = *(rhs.mImpl);
        return *this;
    }

    GnssTime & GnssTime::operator+=( GnssTimeInterval const & rhs )
    {
        *mImpl += rhs;
        return *this;
    }

    GnssTime & GnssTime::operator-=( GnssTimeInterval const & rhs )
    {
        *mImpl -= rhs;
        return *this;
    }

    GnssTimeInterval operator-( GnssTime lhs, GnssTime const &rhs )
    {
        return *(lhs.mImpl) - *(rhs.mImpl);
    }

    GnssTime operator-( GnssTime lhs, GnssTimeInterval const &rhs )
    {
        lhs -= rhs;
        return lhs;
    }

    GnssTime operator+( GnssTime lhs, GnssTimeInterval const &rhs )
    {
        lhs += rhs;
        return lhs;
    }

    bool operator==( GnssTime const &lhs, GnssTime const &rhs )
    {
        return *(lhs.mImpl) == *(rhs.mImpl);
    }

    bool operator!=( GnssTime const &lhs, GnssTime const &rhs )
    {
        return !(lhs == rhs);
    }

    bool operator<( GnssTime const &lhs, GnssTime const &rhs )
    {
        return *(lhs.mImpl) < *(rhs.mImpl);
    }

    bool operator>( GnssTime const &lhs, GnssTime const &rhs )
    {
        return rhs < lhs;
    }

    bool operator<=( GnssTime const &lhs, GnssTime const &rhs )
    {
        return !(lhs > rhs);
    }

    bool operator>=( GnssTime const &lhs, GnssTime const &rhs )
    {
        return !(rhs > lhs );
    }

    std::ostream &operator<<( std::ostream &os, GnssTime const& rhs )
    {
        os << *rhs.mImpl;
        return os;
    }



}// namespace Anise


