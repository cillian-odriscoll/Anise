/*!
* \file ChipConv.cc
* \brief Implementation file for the chip convolution functions
* \author Cillian O'Driscoll
*
* Copyright (C) 2016 Cillian O'Driscoll
*
*
* All rights reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Anise/ChipConv.h"
#include <vector>

#include <stdexcept>
#include <cstdint>
/*******************************************************************************
*                        Fixed Point Hepler functions                         *
*******************************************************************************/

int64_t double_to_fxpt64( double in, unsigned int frac_len=32 )
{
    int64_t out = static_cast< int64_t >( in * std::pow(2.0, frac_len) );

    return out;
}

double fxpt64_to_double( int64_t in, unsigned int frac_len=32)
{
    double out = static_cast< double >( in ) * std::pow( 2.0, -static_cast<double>(frac_len) );

    return out;
}

int32_t double_to_fxpt32( double in, unsigned int frac_len=16 )
{
    int32_t out = static_cast< int32_t >( in * std::pow(2.0, frac_len) );

    return out;
}

double fxpt32_to_double( int32_t in, unsigned int frac_len=16)
{
    double out = static_cast< double >( in ) * std::pow( 2.0, -static_cast<double>(frac_len) );

    return out;
}

/*******************************************************************************
*                                  Templates                                  *
*******************************************************************************/

// Generic default implementation:
template < typename TP, typename TC, typename TO, typename TT >
void chip_conv_tmplt(
        TP const* chip_shape, int len_chip_shape,
        TC const* the_chips, int len_chips,
        TT tau0, TT tauChip, TT dTau,
        TO *bb_sig, int len_bb_sig )
{
    // Check that we have sufficient chips:
    int num_chips_req = std::ceil( (len_bb_sig-1) * dTau/tauChip +
            static_cast< TT >(len_chip_shape-1 )/tauChip );

    if( len_chips < num_chips_req ){
        throw std::out_of_range( "Anise::chip_conv_crc: need " + 
                std::to_string( num_chips_req ) +
                " received " + std::to_string( len_chips ) );
    }


    // Count the number of chips required per output sample:
    int N = std::ceil( ( static_cast< TT >( len_chip_shape ) - 0.5 )/tauChip );


    // Ensure that we have enough pulse samples
    int num_pulse_samples_req = std::ceil( N*tauChip );

    std::vector< TP > cs( chip_shape, chip_shape+len_chip_shape  );

    // zero pad if necessary
    if( num_pulse_samples_req > len_chip_shape ){
        cs.resize( num_pulse_samples_req, TP( 0 ) );
    }

    // Do the convolution
    for( int nn = 0, mm=N-1; nn < N; ++nn, --mm ){
        TT tauK = tau0;
        for( int kk=0; kk < len_bb_sig; ++kk ){
            bb_sig[kk] += the_chips[ 
                static_cast<int>( std::floor( tauK/tauChip ) ) + mm ] * 
                cs[ static_cast< int >( 
                        std::floor( std::fmod( tauK, tauChip ) + nn*tauChip )
                        ) ];
            tauK += dTau;
        }
    }

}

// Generic fixed point 64 bit  implementation:
template < typename TP, typename TC, typename TO, typename TT >
void chip_conv_tmplt_fxpt64(
        TP const* chip_shape, int len_chip_shape,
        TC const* the_chips, int len_chips,
        TT tau0, TT tauChip, TT dTau,
        TO *bb_sig, int len_bb_sig )
{
    // Check that we have sufficient chips:
    int num_chips_req = std::ceil( (len_bb_sig-1) * dTau/tauChip +
            static_cast< TT >(len_chip_shape-1 )/tauChip );

    if( len_chips < num_chips_req ){
        throw std::out_of_range( "Anise::chip_conv_crc: need " + 
                std::to_string( num_chips_req ) +
                " received " + std::to_string( len_chips ) );
    }


    // Count the number of chips required per output sample:
    int N = std::ceil( ( static_cast< TT >( len_chip_shape ) - 0.5 )/tauChip );


    // Ensure that we have enough pulse samples
    int num_pulse_samples_req = std::ceil( N*tauChip );

    std::vector< TP > cs( chip_shape, chip_shape+len_chip_shape  );

    // zero pad if necessary
    if( num_pulse_samples_req > len_chip_shape ){
        cs.resize( num_pulse_samples_req, TP( 0 ) );
    }

    // Setup conversion to int64_t
    unsigned int frac_len = 32;

    int64_t tau0_fxp = double_to_fxpt64( tau0, frac_len );
    int64_t tauChip_fxp = double_to_fxpt64( tauChip, frac_len );
    int64_t dTau_fxp = double_to_fxpt64( dTau, frac_len );

    // Do the convolution
    for( int nn = 0, mm=N-1; nn < N; ++nn, --mm ){
        int64_t tauK_fxp = tau0_fxp;
        for( int kk=0; kk < len_bb_sig; ++kk ){
            bb_sig[kk] += the_chips[ (tauK_fxp/tauChip_fxp) + mm ] *
                cs[  ((tauK_fxp % tauChip_fxp ) + nn * tauChip_fxp ) >> frac_len ];
            tauK_fxp += dTau_fxp;
        }
    }

}

// Generic fixed point 32 bit  implementation:
template < typename TP, typename TC, typename TO, typename TT >
void chip_conv_tmplt_fxpt32(
        TP const* chip_shape, int len_chip_shape,
        TC const* the_chips, int len_chips,
        TT tau0, TT tauChip, TT dTau,
        TO *bb_sig, int len_bb_sig )
{
    // Check that we have sufficient chips:
    int num_chips_req = std::ceil( (len_bb_sig-1) * dTau/tauChip +
            static_cast< TT >(len_chip_shape-1 )/tauChip - 1 );

    if( len_chips < num_chips_req ){
        throw std::out_of_range( "Anise::chip_conv_crc: need " + 
                std::to_string( num_chips_req ) +
                " received " + std::to_string( len_chips ) );
    }


    // Count the number of chips required per output sample:
    int N = std::ceil( ( static_cast< TT >( len_chip_shape ) - 0.5 )/tauChip );


    // Ensure that we have enough pulse samples
    int num_pulse_samples_req = std::ceil( N*tauChip );

    std::vector< TP > cs( chip_shape, chip_shape+len_chip_shape  );

    // zero pad if necessary
    if( num_pulse_samples_req > len_chip_shape ){
        cs.resize( num_pulse_samples_req, TP( 0 ) );
    }

    // Setup conversion to int64_t
    unsigned int frac_len = 16;

    int64_t tau0_fxp = double_to_fxpt32( tau0, frac_len );
    int64_t tauChip_fxp = double_to_fxpt32( tauChip, frac_len );
    int64_t dTau_fxp = double_to_fxpt32( dTau, frac_len );

    // Do the convolution
    for( int nn = 0, mm=N-1; nn < N; ++nn, --mm ){
        int64_t tauK_fxp = tau0_fxp;
        for( int kk=0; kk < len_bb_sig; ++kk ){
            bb_sig[kk] += the_chips[ (tauK_fxp/tauChip_fxp) + mm ] *
                cs[  ((tauK_fxp % tauChip_fxp ) + nn * tauChip_fxp ) >> frac_len ];
            tauK_fxp += dTau_fxp;
        }
    }

}

/*******************************************************************************
*                               Specializations                               *
*******************************************************************************/


//*****************************
//**     FLOATING POINT METHOD
void Anise::chip_conv_crc(
        std::complex< double > const* chip_shape, int len_chip_shape,
        double const* the_chips, int len_chips,
        double tau0, double tauChip, double dTau,
        std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt< std::complex<double>,
        double, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rrr( 
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig )

{
    chip_conv_tmplt< double,
        double,  double, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_ccc( 
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig ){
    chip_conv_tmplt< std::complex<double>,
        std::complex< double >, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rcc( 
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt< double,
        std::complex< double>, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

//*****************************
//**     FXPT32 POINT METHOD
void Anise::chip_conv_crc_fxpt32(
        std::complex< double > const* chip_shape, int len_chip_shape,
        double const* the_chips, int len_chips,
        double tau0, double tauChip, double dTau,
        std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt_fxpt32< std::complex<double>,
        double, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rrr_fxpt32( 
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig )

{
    chip_conv_tmplt_fxpt32< double,
        double,  double, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_ccc_fxpt32( 
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig ){
    chip_conv_tmplt_fxpt32< std::complex<double>,
        std::complex< double >, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rcc_fxpt32( 
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt_fxpt32< double,
        std::complex< double>, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

//*****************************
//**     FXPT64 POINT METHOD
void Anise::chip_conv_crc_fxpt64(
        std::complex< double > const* chip_shape, int len_chip_shape,
        double const* the_chips, int len_chips,
        double tau0, double tauChip, double dTau,
        std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt_fxpt64< std::complex<double>,
        double, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rrr_fxpt64( 
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig )

{
    chip_conv_tmplt_fxpt64< double,
        double,  double, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_ccc_fxpt64( 
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig ){
    chip_conv_tmplt_fxpt64< std::complex<double>,
        std::complex< double >, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}

void Anise::chip_conv_rcc_fxpt64( 
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig )
{
    chip_conv_tmplt_fxpt64< double,
        std::complex< double>, std::complex< double >, double>(
                chip_shape, len_chip_shape,
                the_chips, len_chips,
                tau0, tauChip, dTau,
                bb_sig, len_bb_sig );
}
