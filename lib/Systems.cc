#include <Anise/Systems.h>

namespace Anise {

    namespace Systems {

        std::string SystemToString( GnssSystem const &sys )
        {

            switch( sys ){
                case GPS:
                    return "GPS";
                case GLONASS:
                    return "GLONASS";
                case GALILEO:
                    return "Galileo";
                case BEIDOU:
                    return "BeiDou";
                case WAAS:
                    return "WAAS";
                case EGNOS:
                    return "EGNOS";
                case MSAS:
                    return "MSAS";
                case GAGAN:
                    return "GAGAN";
                case SDCM:
                    return "SDCM";
                case QZSS:
                    return "QZSS";
                case IRNSS:
                    return "IRNSS";
                default:
                    return "Unknown";
            }
        }

    } // namespace Systems

} // namespace Anise
