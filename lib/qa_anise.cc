#include "qa_anise.h"
#include "qa_GnssTime.h"
#include "qa_ChipConv.h"

CppUnit::TestSuite *
qa_anise::suite()
{
  CppUnit::TestSuite *s = new CppUnit::TestSuite("anise");
  s->addTest(Anise::qa_GnssTime::suite());
  s->addTest(Anise::qa_GnssTimeInterval::suite());
  s->addTest(Anise::qa_ChipConv::suite());

  return s;
}

