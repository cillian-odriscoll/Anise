#include <cppunit/TextTestRunner.h>
#include <cppunit/XmlOutputter.h>

#include "qa_anise.h"
#include <iostream>
#include <fstream>

int
main (int argc, char **argv)
{
  CppUnit::TextTestRunner runner;
  std::ofstream xmlfile("anise.xml");
  CppUnit::XmlOutputter *xmlout = new CppUnit::XmlOutputter(&runner.result(), xmlfile);

  runner.addTest(qa_anise::suite());
  runner.setOutputter(xmlout);

  bool was_successful = runner.run("", false);

  return was_successful ? 0 : 1;
}

