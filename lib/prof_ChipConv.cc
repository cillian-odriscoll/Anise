/*!
* \file prof_ChipConv.cc
* \brief Profiler for chip convolution
* \author Cillian O'Driscoll
*
* Copyright (C) 2016 Cillian O'Driscoll
*
*
* All rights reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Anise/ChipConv.h>
#include <algorithm>
#include <vector>
#include <iostream>
#include <string>

#include <boost/program_options.hpp>
namespace po = boost::program_options;


int main( int argc, const char **argv )
{

    // Parse input options:
    po::options_description poDesc( "Allowed options" );
    poDesc.add_options()
        ( "pulse_len,P", po::value< int >(), "length of the chip shape")
        ( "sample_rate,f", po::value< double >(), "chip_shape sample rate")
        ( "baseband_rate,b", po::value< double >(), "output sample rate" )
        ( "num_samples,N", po::value< int >(), "number of output samples to generate" )
        ( "num_iter,I", po::value< int >(), "number of iterations to run")
        ( "method,M", po::value< std::string >(), "method to use" );

    po::variables_map poVarMap;
    po::store( po::parse_command_line( argc, argv, poDesc ), poVarMap );

    po::notify( poVarMap );

    // Constants:
    const double tau0 = 265.01;
    const double Tc = 1e-6;

    // Options:
    int chip_shape_len = 1024;
    if( poVarMap.count( "pulse_len" ) ){
        chip_shape_len = poVarMap[ "pulse_len" ].as< int >();
    }

    double fSample = 900e6;
    if( poVarMap.count( "sample_rate" ) ){
        fSample = poVarMap["sample_rate"].as< double >();
    }

    double fSampleBB = 12e6;
    if( poVarMap.count( "baseband_rate" ) ){
        fSampleBB = poVarMap["baseband_rate"].as< double >();
    }

    int nSamples = 10000;
    if( poVarMap.count( "num_samples" ) ){
        nSamples = poVarMap["num_samples"].as< int >();
    }

    int numRuns = 10000;
    if( poVarMap.count( "num_iter" ) ){
        numRuns = poVarMap["num_iter"].as< int >();
    }
    
    enum eMethod { FLOAT, FXPT32, FXPT64 };

    eMethod method = FLOAT;

    if( poVarMap.count( "method" ) ){
        std::string methAsStringS = 
            poVarMap["method"].as< std::string >();

        std::string methAsString;

        std::transform( methAsStringS.begin(), methAsStringS.end(),
                methAsString.begin(), ::tolower );

        if( ! std::strcmp( "float", methAsString.c_str() ) ){
            method = FLOAT;
        }
        else if( ! std::strcmp( "fxpt32", methAsString.c_str() ) ){
            method = FXPT32;
        }
        else if( ! std::strcmp( "fxpt64", methAsString.c_str() ) ){
            method = FXPT64;
        }
        else{
            std::cerr << "Uknown method: " << methAsStringS;
            return -1;
        }
    }


    // Derived:
    double tauChip = Tc*fSample;
    int chip_len = std::floor( tauChip );
    double dTau = fSample/fSampleBB;



    std::vector< std::complex< double > > chip_shape( chip_shape_len, 0.0 );
    std::generate( chip_shape.begin(), chip_shape.begin()+chip_len, std::rand );

    int num_chips_req = std::ceil( (nSamples-1) * dTau/tauChip +
            static_cast< double >(chip_shape_len-1 )/tauChip );


    std::vector< double > the_chips( num_chips_req, 1.0);

    std::vector< std::complex< double > > bb_sig(nSamples, 0.0);

    std::complex< double >  tmp = 0;

    switch( method ){

        case FLOAT:
        for( unsigned int ii=0; ii < numRuns; ++ii )
        {

            Anise::chip_conv_crc( &chip_shape[0], chip_shape_len,
                    &the_chips[0], num_chips_req,
                    tau0, tauChip, dTau,
                    &bb_sig[0], bb_sig.size() );

            tmp += bb_sig[0];
        }
        break;

        case FXPT32:
        for( unsigned int ii=0; ii < numRuns; ++ii )
        {

            Anise::chip_conv_crc_fxpt32( &chip_shape[0], chip_shape_len,
                    &the_chips[0], num_chips_req,
                    tau0, tauChip, dTau,
                    &bb_sig[0], bb_sig.size() );

            tmp += bb_sig[0];
        }
        break;

        case FXPT64:
        for( unsigned int ii=0; ii < numRuns; ++ii )
        {

            Anise::chip_conv_crc_fxpt64( &chip_shape[0], chip_shape_len,
                    &the_chips[0], num_chips_req,
                    tau0, tauChip, dTau,
                    &bb_sig[0], bb_sig.size() );

            tmp += bb_sig[0];
        }
        break;

    }

    return 0;
}

