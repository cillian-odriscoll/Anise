#include <cppunit/TestAssert.h>
#include "qa_GnssTime.h"
#include <Anise/GnssTime.h>

namespace Anise {

    void qa_GnssTime::test_ctor()
    {
        // It should create a Time at the system epoch when no interval is
        // passed in
        GnssTime epoch( Systems::GPS );
        CPPUNIT_ASSERT_EQUAL( epoch.Week(), 0 );
        CPPUNIT_ASSERT_EQUAL( epoch.TOW(), 0.0 );

        // It should create a Time at the specified offset from the epoch
        int aWeek = 1840;
        double TOW = 108000.123456;
        GnssTime epoch2( Systems::GPS, GnssTimeInterval::Weeks( 1840 )
                + GnssTimeInterval::Seconds(TOW) );

        CPPUNIT_ASSERT_EQUAL( epoch2.System(), Systems::GPS );
        CPPUNIT_ASSERT_EQUAL( epoch2.Week(), aWeek );
        CPPUNIT_ASSERT_EQUAL( epoch2.TOW(), TOW );

        // It should do a deep copy of the implementation:
        GnssTime epoch3( epoch2 );

        CPPUNIT_ASSERT( epoch3 == epoch2 );

        epoch3 += GnssTimeInterval::Seconds( 1.0 );

        CPPUNIT_ASSERT( epoch3 != epoch2 );
    }

    void qa_GnssTime::test_assignment()
    {

        // It should do deep copy on assignment:
        GnssTime epoch( Systems::GPS );

        int aWeek = 1840;
        double TOW = 108000.123456;
        GnssTime epoch2( Systems::GPS, GnssTimeInterval::Weeks( 1840 )
                + GnssTimeInterval::Seconds(TOW) );

        CPPUNIT_ASSERT( epoch != epoch2 );

        epoch = epoch2;

        CPPUNIT_ASSERT( epoch == epoch2 );

        epoch2 += GnssTimeInterval::Seconds(1.0);

        CPPUNIT_ASSERT( epoch2 != epoch );

        // += should increment the time interval:
        CPPUNIT_ASSERT_EQUAL( epoch2.TOW(), epoch.TOW() + 1.0 );

        // -= should decrement the time interval:
        epoch -= GnssTimeInterval::Seconds(1.0);
        CPPUNIT_ASSERT_EQUAL( epoch.TOW(), epoch2.TOW() - 2.0 );

    }

    void qa_GnssTime::test_output()
    {
        GnssTime epoch( Systems::GALILEO, GnssTimeInterval::Weeks( 100 ) );

        std::ostringstream oss("");

        oss << epoch;

        CPPUNIT_ASSERT( oss.str().size() > 0 );

        CPPUNIT_ASSERT_EQUAL( std::string("Galileo Week: 100 TOW: 0"), oss.str() );
    }

    void qa_GnssTimeInterval::test_ctor()
    {
        // It should create an interval of one Week:
        GnssTimeInterval oneWeek = GnssTimeInterval::Weeks(1);
        CPPUNIT_ASSERT_EQUAL( oneWeek.AsWeeks(), 1 );

        // It should create an interval of two weeks:
        GnssTimeInterval twoWeeks = GnssTimeInterval::Weeks(2);
        CPPUNIT_ASSERT_EQUAL( twoWeeks.AsWeeks(), 2 );

        // It should create an interval of one hour:
        GnssTimeInterval oneHour = GnssTimeInterval::Hours(1);
        CPPUNIT_ASSERT_EQUAL( oneHour.AsSeconds(), 3600.0 );

        // It should create an interval of one day:
        GnssTimeInterval oneDay = GnssTimeInterval::Days(1);
        CPPUNIT_ASSERT_EQUAL( oneDay.AsSeconds(), 3600.0 * 24.0 );

        // It should create an interval of one second:
        GnssTimeInterval oneSecond = GnssTimeInterval::Seconds(1);
        CPPUNIT_ASSERT_EQUAL( oneSecond.AsSeconds(), 1.0 );

        // It should create an interval of one millisecond:
        GnssTimeInterval oneMS = GnssTimeInterval::MilliSeconds(1);
        CPPUNIT_ASSERT_EQUAL( oneMS.AsSeconds(), 1e-3 );

        // It should create an interval of one microsecond
        GnssTimeInterval oneUS = GnssTimeInterval::MicroSeconds(1);
        CPPUNIT_ASSERT_EQUAL( oneUS.AsSeconds(), 1e-6 );

        // It should create an interval of one nanosecond
        GnssTimeInterval oneNS = GnssTimeInterval::NanoSeconds(1);
        CPPUNIT_ASSERT_EQUAL( oneNS.AsSeconds(), 1e-9 );
    }

    void qa_GnssTimeInterval::test_assignment()
    {
        // It should assign one interval to another:
        GnssTimeInterval dt1 = GnssTimeInterval::Weeks(1);
        GnssTimeInterval dt2 = GnssTimeInterval::NanoSeconds(12);

        dt1 = dt2;

        CPPUNIT_ASSERT_EQUAL( dt1.AsSeconds(), dt2.AsSeconds() );

        // It should sum and assign:
        dt1 += dt2;

        CPPUNIT_ASSERT_EQUAL( dt1.AsSeconds(), 2*dt2.AsSeconds() );

        // It should subtract and assign:
        dt1 -= dt2;

        CPPUNIT_ASSERT_EQUAL( dt1.AsSeconds(), dt2.AsSeconds() );
    }

    void qa_GnssTimeInterval::test_comparators()
    {
        // It should know that a week is bigger than a Day
        GnssTimeInterval dt1 = GnssTimeInterval::Weeks(1);
        GnssTimeInterval dt2 = GnssTimeInterval::Days(1);

        CPPUNIT_ASSERT( dt1 > dt2 );
        CPPUNIT_ASSERT( dt1 >= dt2 );

        // And conversely:
        CPPUNIT_ASSERT( dt2 < dt1 );
        CPPUNIT_ASSERT( dt2 <= dt1 );

        // Or, indeed:
        CPPUNIT_ASSERT( dt1 != dt2 );

        // It should know when two time intervals are identical:
        dt2 = GnssTimeInterval::Weeks(1);

        CPPUNIT_ASSERT( dt1 == dt2 );
        CPPUNIT_ASSERT( dt1 >= dt2 );
        CPPUNIT_ASSERT( dt1 <= dt2 );

    }

    void qa_GnssTimeInterval::test_operators()
    {
        // It should be able to add two time intervals
        GnssTimeInterval dt1 = GnssTimeInterval::Seconds(1);
        GnssTimeInterval dt2 = GnssTimeInterval::Seconds(2);

        GnssTimeInterval dt3 = dt1 + dt2;

        CPPUNIT_ASSERT_EQUAL( dt1.AsSeconds(), 1.0 );
        CPPUNIT_ASSERT_EQUAL( dt2.AsSeconds(), 2.0 );

        CPPUNIT_ASSERT_EQUAL( dt3.AsSeconds(), dt1.AsSeconds() + dt2.AsSeconds() );

        // It should be able to subtract two time intervals:
        dt3 = dt2 - dt1;

        CPPUNIT_ASSERT_EQUAL( dt3.AsSeconds(), dt2.AsSeconds() - dt1.AsSeconds() );
    }

    void qa_GnssTimeInterval::test_large_diffs()
    {
        // It should maintain the ability to determine nanosecond level differences over Weeks
        GnssTimeInterval dt1 = GnssTimeInterval::Weeks( 2048 );
        GnssTimeInterval dt2 = GnssTimeInterval::NanoSeconds( 1 );

        GnssTimeInterval dt3 = dt1 + dt2;

        CPPUNIT_ASSERT_EQUAL( ( dt3 - dt1 ).AsSeconds(), dt2.AsSeconds() );
    }

    void qa_GnssTimeInterval::test_negative_intervals()
    {
        // Check how we handle negative time intervals
        GnssTimeInterval dt1 = GnssTimeInterval::Seconds(1);
        GnssTimeInterval dt2 = GnssTimeInterval::Seconds(2);

        GnssTimeInterval dt3 = dt1 - dt2;

        CPPUNIT_ASSERT_EQUAL( dt3.AsWeeks(),  0 );
        CPPUNIT_ASSERT_EQUAL( dt3.AsSeconds(), -1.0 );

        dt1 += GnssTimeInterval::Weeks(1);
        dt3 = dt1 - dt2;

        CPPUNIT_ASSERT_EQUAL( dt3.AsWeeks(),  0 );
        CPPUNIT_ASSERT_EQUAL( dt3.AsSeconds(), GnssTimeInterval::Weeks(1).AsSeconds()-1.0 );

        dt1 = GnssTimeInterval::Weeks(-1) - GnssTimeInterval::Seconds(1);
        dt3 = dt1 + dt2;

        CPPUNIT_ASSERT_EQUAL( dt3.AsWeeks(), 0 );
        CPPUNIT_ASSERT_EQUAL( dt3.AsSeconds(), 1.0 - GnssTimeInterval::Weeks(1).AsSeconds() );

    }

} /* namespace anise */

