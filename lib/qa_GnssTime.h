#ifndef INCLUDED_QA_GNSS_TIME_H
#define INCLUDED_QA_GNSS_TIME_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestCase.h>

namespace Anise {

    class qa_GnssTime : public CppUnit::TestCase
    {
        public:
            CPPUNIT_TEST_SUITE(qa_GnssTime);
            CPPUNIT_TEST(test_ctor);
            CPPUNIT_TEST(test_assignment);
            CPPUNIT_TEST(test_output);
            CPPUNIT_TEST_SUITE_END();

        private:
            void test_ctor();
            void test_assignment();
            void test_output();
    };

    class qa_GnssTimeInterval : public CppUnit::TestCase
    {

        public:
            CPPUNIT_TEST_SUITE(qa_GnssTimeInterval);
            CPPUNIT_TEST(test_ctor);
            CPPUNIT_TEST(test_assignment);
            CPPUNIT_TEST(test_comparators);
            CPPUNIT_TEST(test_operators);
            CPPUNIT_TEST(test_large_diffs);
            CPPUNIT_TEST( test_negative_intervals );
            CPPUNIT_TEST_SUITE_END();

        private:
            void test_ctor();
            void test_assignment();
            void test_comparators();
            void test_operators();
            void test_large_diffs();
            void test_negative_intervals();
    };

} /* namespace anise */

#endif // INCLUDED_QA_GNSS_TIME_H


