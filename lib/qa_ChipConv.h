#ifndef INCLUDED_QA_CHIP_CONV_H
#define INCLUDED_QA_CHIP_CONV_H

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestCase.h>

namespace Anise {

    class qa_ChipConv : public CppUnit::TestCase
    {
        public:
            CPPUNIT_TEST_SUITE(qa_ChipConv);
            CPPUNIT_TEST(test_length_1);
            CPPUNIT_TEST(test_insufficient_chips);
            CPPUNIT_TEST_SUITE_END();

        private:
            void test_length_1();
            void test_insufficient_chips();
    };

    ;

} /* namespace anise */

#endif // INCLUDED_QA_CHIP_CONV_H



