function build_mex( buildDir )
% build_mex - Builds the Anise mex files
%
% build_mex
%   builds the mex files assuming that the library 'anise' is already installed
%
% build_mex( buildDir )
%   builds the mex files assuming that the anise library is located in buildDir
%

INCLUDE_DIRS = { 'include' };
LIB_DIRS = {};

if nargin 
    LIB_DIRS = { buildDir };
end

libs = { 'anise' };

targets = { ...
    'Matlab/GnssTime/private/GnssTime_interface.cpp', ...
    'Matlab/GnssTime/private/GnssTimeInterval_interface.cpp', ...
    'Matlab/SignalGeneration/private/ChipConv_mex.cc', ...
};

joinArgs = @(flag, args) strjoin( ...
    cellfun( @(anArg)[ '-' flag '"' anArg '"' ], args, 'UniformOutput', false ), ...
    ' ' );

includeStr = joinArgs( 'I', INCLUDE_DIRS );
libDirStr = joinArgs( 'L', LIB_DIRS );
libStr = joinArgs( 'l', libs );

for trgInd = 1:length( targets )
    [ pathStr, pathName, ext ] = fileparts( targets{trgInd} );
    
    mex( '-v', '-output', [ pathStr filesep pathName ],...
        includeStr, libDirStr, libStr, targets{trgInd} );
end
