/* -*- c++ -*- */

%module GnssTime

%include Anise_swig.i

%{
#include <sstream>
#include "Anise/GnssTime.h"
%}

%ignore operator=;
%ignore operator+;
%ignore operator-;
%ignore operator==;
%ignore operator!=;
%ignore operator<;
%ignore operator>;
%ignore operator>=;
%ignore operator<=;

%ignore operator<<;

%feature("valuewrapper") GnssTime;
class GnssTime;

%include "Anise/Systems.h"
%include "Anise/GnssTime.h"

%extend Anise::GnssTime {
    Anise::GnssTime __add__( GnssTimeInterval const& rhs )
    {
        return *self + rhs;
    }

    Anise::GnssTime __sub__( GnssTimeInterval const& rhs )
    {
        return *self - rhs;
    }

    Anise::GnssTimeInterval __sub__( GnssTime const& rhs )
    {
        return *self - rhs;
    }

    bool __eq__( GnssTime const &rhs )
    {
        return *self == rhs;
    }

    bool __neq__( GnssTime const &rhs )
    {
        return *self != rhs;
    }


    bool __ge__( GnssTime const &rhs )
    {
        return *self >= rhs;
    }

    bool __le__( GnssTime const &rhs )
    {
        return *self <= rhs;
    }

    bool __gt__( GnssTime const &rhs )
    {
        return *self > rhs;
    }

    bool __lt__( GnssTime const &rhs )
    {
        return *self < rhs;
    }

    const char * __str__()
    {
        std::ostringstream oss("");
        oss << *self;
        return oss.str().c_str();
    }

};

%extend Anise::GnssTimeInterval {
    Anise::GnssTimeInterval __add__( GnssTimeInterval const & rhs )
    {
        return *self + rhs;
    }

    Anise::GnssTime __add__( GnssTime const & rhs )
    {
        return rhs + *self;
    }

    Anise::GnssTimeInterval __sub__( GnssTimeInterval const & rhs )
    {
        return *self - rhs;
    }

    bool __eq__( GnssTimeInterval const &rhs )
    {
        return *self == rhs;
    }

    bool __neq__( GnssTimeInterval const &rhs )
    {
        return *self != rhs;
    }


    bool __ge__( GnssTimeInterval const &rhs )
    {
        return *self >= rhs;
    }

    bool __le__( GnssTimeInterval const &rhs )
    {
        return *self <= rhs;
    }

    bool __gt__( GnssTimeInterval const &rhs )
    {
        return *self > rhs;
    }
    bool __lt__( GnssTimeInterval const &rhs )
    {
        return *self < rhs;
    }

    const char * __str__()
    {
        std::ostringstream oss("");
        oss << *self;
        return oss.str().c_str();
    }
};
