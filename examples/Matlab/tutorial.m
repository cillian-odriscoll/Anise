%% Anise Tutorial
% Follow this simple tutorial to become familiar with the basic operation of
% Anise
%

%%
% The file 'init.m' in this folder sets up some constants and adds the
% Anise files to the Matlab path
init;

%%
% The fundamental concept in Anise is that every GNSS signal can be expressed
% as the convolution of a finite duration subcarrier with an infinite sequence
% of impulses, called chips:
%
% $$x(t) = g(t) * s(t)$$
% 
% where:
%
% $$g(t) = \int_{\infty}^{\infty} g_k \delta( t - k T_c ) d t$$
%
% $g_k \in \{-1,1\}$
%
% $\delta(t)$ is the Dirac delta function
%
% $T_c$ is the chip duration
%
% $s(t)$ is the finite duration subcarrier pulse, such that:
%
% $$s(t) = 0 : t < 0\ \mathrm{or}\ t >= T_c$$
%
% and
%
% $$\int_{-\infty}^{\infty} \left| s(t) \right|^2 dt = 1$$
%
% Anise provides implementations of all the common subcarriers currently in 
% use in GNSS systems. The full list is in the 'Matlab/Subcarrier' sub-directory
%
% Here, for example, we generate the GPS L1 C/A subcarrier (a Binary Phase
% Shift Keying [BPSK]) subcarrier.

fc = fo; % <-- fo defined in init.m is 1.023 MHz
Tc = 1/fc;

tt = linspace( -0.5*Tc, 1.5*Tc ); % <-- lets look 1/2 a chip either side of Tc
%%
% There is extensive in-line help:
help BpskSubCarrier

%%
% So here we generate the BPSK subcarrier: and plot the result
sBpsk = BpskSubCarrier( tt, fc );

figure;
plot( tt*fc, sBpsk );
grid on; hold all;
ylim( [-0.5, 1.5] );

%%
% Zooming in on the chip edge, we see the signal has been effectively
% undersampled.
xlim( [-0.2, 0.2] );
dt = tt(2) - tt(1);
fsEff = 1./dt;
disp( [ 'Sampling rate: ' num2str( fsEff/1e6 ) ' MHz' ] );



%%
% This can be improved by increasing the sampling rate
%
fSample = 500e6; %<-- set 0.5 GHz sampling rate
tt = -0.5*Tc:1/fSample:1.5*Tc;
sBpskHigh = BpskSubCarrier( tt, fc );
plot( tt*fc, sBpskHigh );

legend( 'Low Rate', 'High Rate' );

%%
% However, the BPSK subcarrier has infinite bandwidth, so we have simply
% pushed the problem to a finer error:
xlim( [-0.02, 0.02] );


%%
% In Anise, the high resolution chip shape is encapsulated in a structure
% called a 'SubCarrier Object' (SCO). The SCO data structure includes the
% components:
%
% * x: The indepentent variable, time (s), frequency (Hz) or delay (s)
% * y: The dependent variable, the subcarrier value at x
% * fSample: The sampling rate of the SCO (sps)
% * fc: The nominal code chipping rate
% * saleFactor: An amplitude gain that should be applied to y.
%
% Note that x and y are typically vectors.
%
% Consider again the previous example, but this time expressed through the
% SCO concept
Tsco = 5*Tc; %<- max time to generate for the SCO
scoBpsk = MakeSubCarrier( ...
    @(t)BpskSubCarrier( t, fc ), ...
    Tc, fSample, Tsco );

% Look at the member variables:
scoBpsk

%%
% We can plot SCOs with ease
figure;
plot( scoBpsk.x*scoBpsk.fc, scoBpsk.y*scoBpsk.scaleFactor );
xlabel( 'Time [chips]' );
ylabel( 'BPSK Subcarrier' );
grid on; hold all;


%%
% Anise provides a number of utility functions for operating on SCOs.
% Here are some common examples:

%%
% AddSCO:
help AddSCO


%%
% DotMultiplySCO:
help DotMultiplySCO


%%
% XCorrSCO:
help XCorrSCO

%%
% FftSCO:
help FftSCO


%%
% IFftSCO:
help IFftSCO



%%
% Note that the XCorrSCO, FftSCO and IFftSCo functions all involve transformation
% from one 'domain' to another
%
%    Function    | Input Domain | Output Domain |
%  ----------------------------------------------
%    XCorrSCO    |    time      |    delay      |
%     FftSCO     |    time      |  Frequency    |
%       "        |    delay     |   Spectrum    |
%    IFftSCO     |   frequency  |     time      |
%       "        |   spectrum   |    delay      |
%
% By convention in Anise, time domain Subcarrier Objects are usually labelled
% 'sco', correlation domain objects are labelled 'ro' and frequency/Spectrum
% domain objects are labelled 'fo'
%
% Let's see that in action:

roBpsk = XCorrSCO( scoBpsk );

figure;

plot( roBpsk.x*fc, roBpsk.y );
grid on; hold all;

xlabel( 'Delay [chips]' );
ylabel( 'R_{bpsk}( \tau )' )


%%
%
foBpsk = FftSCO( scoBpsk );

% FftSCO keeps the Matlab ordering of x and y, reverse that here for plotting:
foBpsk.x = fftshift( foBpsk.x );
foBpsk.y = fftshift( foBpsk.y );

figure;

subplot( 2, 1, 1 );
plot( foBpsk.x/1e6, 20*log10( abs(  foBpsk.y ) ) );
grid on; hold all;
xlim( [-6,6] );

ylabel( '| H_{bpsk}(f) |^2 [dB] ' );

subplot( 2, 1, 2 );
plot( foBpsk.x/1e6, unwrap( angle( foBpsk.y ) ) );
grid on; hold all;
xlim( [-6,6] );

ylabel( '\phi_{bpsk}(f) [rad] ' );
xlabel( 'f [MHz]' );

%%%
% One of the most useful utility functions is FilterSCO

help FilterSCO


%%
% This implements the standard Matlab/Octave filter function on time domain
% SCOs

BRx = 3*fc; % <-- Two-sided RF analytical equivalent bandwidth

[ b, a ] = butter( 6, BRx/fSample );

scoBpskFilt = FilterSCO( b, a, scoBpsk );

% Compare the filtered and 'raw' signal
figure;

subplot( 1, 2, 1 );
plot( scoBpsk.x*fc, scoBpsk.y, scoBpsk.x*fc, scoBpskFilt.y )
grid on; hold all;

xlabel( 't [chips]' )
ylabel( 'x(t)' )
legend( 'Raw', 'Filt' )

% Same thing in the correlation domain
roBpskFilt = XCorrSCO( scoBpskFilt, scoBpsk );
subplot( 1,2,2 );
plot( roBpsk.x*fc, roBpsk.y, roBpskFilt.x*fc, roBpskFilt.y );
grid on; hold all;
xlabel( '\tau [chips]' );
ylabel( 'R(\tau)' );
%%
% .. and in the frequency domain
figure

% This function extracts a 'frequency domain object' from the filter parameters
% This allows us to view the magnitude and phase response of the filter in the
Hfo = FilterToHfo( b, a, fSample );
plot( Hfo.x/1e6, 20*log10(abs(Hfo.y)) );
grid on; hold all;
xlabel( 'f [MHz]' );
ylabel( '| H_{filt}(f) |^2 [dB] ' );
xlim( [-6,6] );
grid on; hold all;

plot( foBpsk.x/1e6, 20*log10(abs(foBpsk.y) ) )
figure(gcf)
foBpskFilt = FftSCO( scoBpskFilt );
foBpskFilt.y = fftshift( foBpskFilt.y );
foBpskFilt.x = fftshift( foBpskFilt.x );
plot( foBpskFilt.x/1e6, 20*log10(abs(foBpskFilt.y) ) )

legend( 'H(f)', 'BPSK', 'BPSK Filt' );
