%
% Testing the ConvChips function

init;

fc = fo; % Code rate
Tc = 1/fc;

fSampleBB = 62.5e6; %15*fo; % Baseband sampling rate
TsBB = 1/fSampleBB;
Ts = 1/fSample;

BW = 12e6; % Two sided bandwidth
[b, a] = butter( 6, (BW/2)/(fSample/2) ); % SCO Butterworth Filter


fRf = fL1; % RF centre frequency
fIF = 0;
fDoppler = -3e3; % Doppler shift

% Start with the BPSK subcarrier:
sco = MakeSubCarrier( @(t)BpskSubCarrier( t, fc ), Tc, fSample, Tsco );

% Filter it:
scoF = FilterSCO( b, a, sco );
%scoF = sco;

% Set up time tags at the beginning and end of the ms of interest:
Ttx = 15;
Trx = Ttx + 0.070 + 0.25/fc;

timeDilation = fDoppler/fRf; % Ignoring any clock effects for now
phi0 = 0;
phiDot = 2*pi*fDoppler;

tSCO1 = TagSCO( scoF, Ttx, Trx, timeDilation, phi0, phiDot );

tSCO1 = TrimSCO( tSCO1, 0, Tsco );

PRN = 1;
theCode = 1-2*l1ca(PRN);
chipGen = PeriodicCodeGenerator( theCode, Tc );

tau0Chips = Ttx/Tc;
histLength = ceil( Tsco/Tc );

tau0 = (Ttx - floor( tau0Chips )*Tc )* fSample;

dTau = fSample/(1+timeDilation)/fSampleBB;

tau0 = tau0;

TgenList = (1:1:15)*0.001;
Tscalar = zeros( size(TgenList) );
Tvec = Tscalar;

for ii = 1:length( TgenList )
    Tgen = TgenList(ii);
    K = ceil( Tgen*fSampleBB );
    %K = 2048;
    %Tgen = K*TsBB;
    NChips = ceil( Tgen/(1+timeDilation)/Tc + histLength - 1 );
    theChips = generate( chipGen, (Ttx - (histLength-1)*Tc) + (0:(NChips-1))*Tc );

    disp( ['Trying scalar method: ' ] );
    tic;
    XS = ConvChips( K, theChips, tSCO1.y, Tc/Ts, dTau, tau0, 'scalar' );
    Tscalar(ii) = toc;
    disp( ['Trying vector method: ' ] );
    tic;
    XV = ConvChips( K, theChips, tSCO1.y, Tc/Ts, dTau, tau0, 'vector' );
    Tvec(ii) = toc;
end

figure;
plot( TgenList/1e-3, Tscalar, TgenList/1e-3, Tvec )
grid on; hold all;
