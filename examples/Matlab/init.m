addpath( genpath( '../../Matlab' ) )
if exist('OCTAVE_VERSION', 'builtin')
    pkg load signal
end

fo = 1.023e6;
fL1 = 1540*fo;
fSample = 100*pi*fo*3;

Tsco = 3/fo;

kLightSpeed = 299792458;
