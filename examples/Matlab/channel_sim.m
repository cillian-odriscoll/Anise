%
% Simple geometric example
%
% We set up a constant range rate simulation and generate the C/A code
% with random data bits

init;

fc = fo;

fSampleGen = 10e6;
BWTx = 90*fo;
BWRx = 8.75e6;

% Set up the Tx filter:
[bTx, aTx] = butter( 6, (BWTx/2)/(fSample/2) ); % SCO Butterworth Filter
% Set up the rx filter:
[bRx, aRx] = butter( 6, (BWRx/2)/(fSample/2) ); % SCO Butterworth Filter


fRf = fL1; % RF centre frequency
fIF = 0;

tickPeriod = 0.01; % Assume we get tagged SCOs every millisecond
numTicks = 10;

%%
% We are going to load this simulation from a Spirent CSV file:
PRN = 1;
SatData = LoadSpirentSatData( './Data/sat_data_V1A1.csv', 'Galileo', PRN );

%%
% Setup the channel
simChannel = Channel();


% The transmit filter:
simChannel = push_back( simChannel, FilterBlock( bTx, aTx ) );

% Normalise the power:
simChannel = push_back( simChannel, PowerNormalisationBlock() );

% Set up the power gain:
Ptx = 25; % Watts
antGainTx = 10^(0.1*16);
EIRP = Ptx * antGainTx;
txGainTrj = ConstantTrajectory( 10*log10(EIRP) );
simChannel = push_back( simChannel, PowerGainBlock( txGainTrj ) );

% Geometry - range, range rate, free space loss
simChannel = push_back( simChannel, GeomBlock( SatData.rangeTrj, SatData.rangeRateTrj ) );

% Ionosphere
simChannel = push_back( simChannel, IonoBlock( SatData.tecuTrj ) );

% Rx antenna:
antGainRx = 6; % dB
rxGainTrj = ConstantTrajectory( antGainRx );
simChannel = push_back( simChannel, PowerGainBlock( rxGainTrj ) );

% Rx filter:
simChannel = push_back( simChannel, FilterBlock( bRx, aRx ) );

% Set up the demodulator:
phi0Rx = 0;
phiDotRx = 2*pi*( fRf - fIF );
simChannel = push_back( simChannel, DemodBlock( phiDotRx, phi0Rx ) );



%%
% Signal Generation:
Ttx = GnssTime( 2, GnssTimeInterval.Weeks( 1853 ) + ...
    GnssTimeInterval.Seconds( SatData.timeVec(1) ) );
% Data generators:
codeGen = PeriodicCodeGenerator( 1-2*l1ca(PRN), 1/fc );

Td = 0.02;
Tcode = 0.001;
secCodeGen = PeriodicCodeGenerator( ones(1,20), Tcode );
randomData = 1-2*round( rand( [250, 1]) );
dataGen = PeriodicCodeGenerator( randomData, Td );
chipGen = ChainChipGenerator( { codeGen, secCodeGen, dataGen } );

% raw subcarrier object:
sco = MakeSubCarrier( @(t)BpskSubCarrier( t, fc ), 1/fc, fSample, 3/fc );

% Setup the modulator:
phi0Tx = 0;
phiDotTx = 2*pi*fRf;


%%
% Simulation start:
%tRxSample = ceil( (Ttx + SatData.rangeTrj.ValueAt(Ttx)/kLightSpeed )*fSampleGen )/fSampleGen ;
tRxSample = Ttx + ceil( (SatData.rangeTrj.ValueAt(Ttx)/kLightSpeed )*fSampleGen )/fSampleGen ;
t0 = tRxSample;

figure;

scoRxFilt = [];

% The time tagged SCO is generated on the satellite:
% Note: sv filter is assumed not to be time varying here:
scoTx = TagSCO( sco, Ttx, Ttx, 0, phi0Tx, phiDotTx );
%%
% Loop over the ticks:
countRate = 1;
for tick = 1:numTicks
    if mod( tick-1, countRate ) == 0
        disp( [ 'Tick: ' num2str(tick) ] );
        tic;
    end
    scoRxFiltPrev = scoRxFilt;

    % Apply the baseband equivalent channel:
    [ ifSig, scoRxFilt, scoTx, simChannel, chipGen ] = ApplyChannelToSCO( ...
        scoTx, simChannel, tRxSample, fSampleGen, chipGen, tickPeriod, scoRxFiltPrev );
    if tick > 1
        t0Rel = (tRxSample - scoRxFiltPrev.Trx );
        tEndRel = (scoRxFilt.Trx - scoRxFiltPrev.Trx );
        ttRel = t0Rel.AsSeconds():(1/fSampleGen):tEndRel.AsSeconds();

    end
    tRxSample = tRxSample + length(ifSig)/fSampleGen;


    if tick > 1

        dt = (scoRxFiltPrev.Trx-t0);
        if isa( dt, 'GnssTimeInterval' )
            dt = dt.AsSeconds();
        end
        plot( (ttRel + dt ), [ real( ifSig ); imag( ifSig ) ], '.-' );
        grid on; hold all;
    end

    if mod( tick-1, countRate ) == countRate-1
        toc;
    end

end

xlabel( 'Time [s]' )
ylabel( 'x_{IF}(t)' )
