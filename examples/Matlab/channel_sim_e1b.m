%% A Simple GNSS Signal Simulator
% Simple geometric example
%
% We set up a simple simulation and generate the Galileo E1b code
% with random data bits
%
% This file demonstrates how easy is it to construct a GNSS signal
% simulator using Anise.
%
% First we need to set up some simulation parameters:

% Overall performance timer:
oTimer = tic;

% Setup paths etc:
init;

% Options:
doWrite = 1; % <- write IF signal to disk?
doCIR = 0;   % <- Do apply a fading channel model?
doPlot = 1;  % <- Generate plots?

if doWrite
    fid = fopen( 'channel_sim_e1b.dat', 'wb' );
end

% Simulation duration:
TotalDuration = 0.1;
tickPeriod = 0.01;  % Assume we get tagged SCOs every 10 milliseconds
numTicks = ceil( (TotalDuration + tickPeriod)/tickPeriod );

% Some signal/transmission parameters: 
fSampleGen = 15e6; % <- Output sampling rate
BWTx = 90*fo;      % <- Satellite transmit bandwidth (2 sided)
BWRx = 12.75e6;    % <- Receiver front-end bandwidth (2 sided)

% Set up the Tx filter:
[bTx, aTx] = butter( 6, (BWTx/2)/(fSample/2) ); % SCO Butterworth Filter
% Set up the rx filter:
[bRx, aRx] = butter( 6, (BWRx/2)/(fSample/2) ); % SCO Butterworth Filter


fRf = fL1; % RF centre frequency
fIF = 0;   % Intermediate Frequency




%%
% We are going to load this simulation from a Spirent CSV file:
PRN = 1;
SatData = LoadSpirentSatData( './Data/sat_data_V1A1.csv', 'Galileo', PRN );

%%
% In the following sequence we set up the channel, consisting of the
% following steps:
%
% # Create the Channel object
% # Add the transmit filter
% # Add a power normalisation block (unit power in tx bandwidth)
% # Add a power gain - representing the transmit amplifier
% # Add a Geometry block, which accounts for range and range rate effects
% # Add an Ionosphere block, which models the 1st order iono effect
% # Optionally add a CIR block for a multipath channel
% # Add the receiver antenna block, constant gain in this case.
% # Add a receiver filter block
% # Add a demodulator block to bring the signal to baseband in the
% receiver.

% Setup the channel
simChannel = Channel();


% The transmit filter:
simChannel = push_back( simChannel, FilterBlock( bTx, aTx ) );

% Normalise the power:
simChannel = push_back( simChannel, PowerNormalisationBlock() );

% Set up the power gain:
Ptx = 25; % Watts
antGainTx = 10^(0.1*16);
EIRP = Ptx * antGainTx;
txGainTrj = ConstantTrajectory( 10*log10(EIRP) );
simChannel = push_back( simChannel, PowerGainBlock( txGainTrj ) );

% Geometry - range, range rate, free space loss
simChannel = push_back( simChannel, GeomBlock( SatData.rangeTrj, SatData.rangeRateTrj ) );

% Ionosphere
simChannel = push_back( simChannel, IonoBlock( SatData.tecuTrj ) );

% Channel impulse response:
if doCIR
    load( 'Data/CIRUrbanCar.mat');
    simChannel = push_back( simChannel, CIRBlock( CIR, 'filter' ) );
end

% Rx antenna:
antGainRx = 6; % dB
rxGainTrj = ConstantTrajectory( antGainRx );
simChannel = push_back( simChannel, PowerGainBlock( rxGainTrj ) );

% Rx filter:
simChannel = push_back( simChannel, FilterBlock( bRx, aRx ) );

% Set up the demodulator:
phi0Rx = 0;
phiDotRx = 2*pi*( fRf - fIF );
simChannel = push_back( simChannel, DemodBlock( phiDotRx, phi0Rx ) );



%%
% Signal Generation:
%
% First we set up the chip generators and the 'transmitted' SCO:
Ttx = GnssTime( 2, GnssTimeInterval.Weeks( 1853 ) + ...
    GnssTimeInterval.Seconds( SatData.timeVec(1) ) );

fc = fo;
Tc = 1/fc;
fs = fo;
fs2 = 6*fo;
rho = 1/11;
Td = 0.004;

% raw subcarrier objects:
scoE1b = MakeSubCarrier( @(t)CBocSubCarrier( t, fs2, fc, rho ), 1/fc, fSample, 4.5/fc );

% Data generators:
e1bCodeGen = PeriodicCodeGenerator( GalileoE1b(PRN), 1/fc );

%e1bDataGen = RandomChipGenerator( Td );
randomData = 1-2*round( rand( [250, 1]) );
e1bDataGen = PeriodicCodeGenerator( randomData, Td );
e1bChipGen = ChainChipGenerator( { e1bCodeGen, e1bDataGen } );

% Setup the modulator:
phi0Tx = 0;
phiDotTx = 2*pi*fRf;


%%
% Simulation start:
% 
% Now we loop over all the ticks.

% tRxSample is the receiver time of the next sample to be computed:
tRxSample = Ttx + ceil( (SatData.rangeTrj.ValueAt(Ttx)/kLightSpeed )*fSampleGen )/fSampleGen ;

t0 = tRxSample;

% Empty SCO,  needed in first iteration of the loop
scoRxFiltE1b = [];

% The time tagged SCO is generated on the satellite:
% Note: sv filter is assumed not to be time varying here:
scoE1bTx = TagSCO( scoE1b, Ttx, Ttx, 0, phi0Tx, phiDotTx );

%%
% Loop over the ticks:
countRate = 5;
% generate a figure onto which to plot the IF data:
if doPlot
    figure;
end
for tick = 1:numTicks
    if mod( tick-1, countRate ) == 0
        disp( [ 'Tick: ' num2str(tick) ] );
        tic;
    end
    
    % Store the previous received SCO:
    scoRxFiltPrevE1b = scoRxFiltE1b;

    % ApplyChannelToSCO - takes care of
    %   1) Applies the Channel to the transmitted SCO
    %   2) Generates all the IF signal samples in the range [ Trx0, Trx1 )
    %      where Trx0 is the receive time of the previous Rx SCO and
    %            Trx1 is the receive time of the current Rx SCO
    %   3) Propagates the transmit SCO to the by tickPeriod
    [ ifSig, scoRxFiltE1b, scoE1bTx, simChannel, e1bChipGen ] = ApplyChannelToSCO( ...
        scoE1bTx, simChannel, tRxSample, fSampleGen, e1bChipGen, tickPeriod, scoRxFiltPrevE1b );
    
    % Plot results
    if doPlot && tick > 1
        t0Rel = (tRxSample - scoRxFiltPrevE1b.Trx );
        tEndRel = (scoRxFiltE1b.Trx - scoRxFiltPrevE1b.Trx );
        ttRel = t0Rel.AsSeconds():(1/fSampleGen):tEndRel.AsSeconds(); 
        dt = (scoRxFiltPrevE1b.Trx-t0);
        if isa( dt, 'GnssTimeInterval' )
            dt = dt.AsSeconds();
        end
        plot( (ttRel + dt ), [ real( ifSig ); imag( ifSig ) ], '.-' );
        grid on; hold all;
        xlabel( 'Time [s]' );
    end
    
    % Propagate the receiver time:
    tRxSample = tRxSample + length(ifSig)/fSampleGen;
    
    % We write the data out in interleaved I,Q double precision floating
    % point format.
    if doWrite && ~isempty( ifSig )
        ifSigInterleaved = zeros( 2*length( ifSig ), 1 );
        ifSigInterleaved(1:2:end) = real(ifSig);
        ifSigInterleaved(2:2:end) = imag(ifSig);
        fwrite( fid, ifSigInterleaved, 'double' );
    end

    % Performance counter:
    if mod( tick-1, countRate ) == countRate-1
        toc;
    end

end


if doWrite
    fclose( fid );
end

% overall performance counter:
toc(oTimer);
dt =  (tRxSample - t0);
disp( [ 'Processed ' num2str( dt.AsSeconds() ) ' seconds' ] );


%%
% Note in the plot that the IF signal generated in each tick has a
% different colour.
%
% Zooming in on the plot we can see that there is a seamless transition
% from one 'tick' to the next
xlim( [ -1, 1 ]*100/fc + tickPeriod );

%%
% Zooming in further again, the CBOC signal structure becomes apparent
xlim( [ -1, 1 ]*10/fc + tickPeriod )