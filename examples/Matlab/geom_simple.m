%
% Simple geometric example
%
% We set up a constant range rate simulation and generate the C/A code
% with random data bits

init;

fc = fo;

fSampleGen = 5e6;
BWTx = 30*fo;

BWRx = 3.75e6;

[bTx, aTx] = butter( 6, (BWTx/2)/(fSample/2) ); % SCO Butterworth Filter


fRf = fL1; % RF centre frequency
fIF = 0;

tickPeriod = 0.001; % Assume we get tagged SCOs every millisecond
numTicks = 3;

%%
% Signal Generation:
Ttx = 0;
theChips = 1 - 2*l1ca(1);

% raw subcarrier object:
sco = MakeSubCarrier( @(t)BpskSubCarrier( t, fc ), 3/fc, fSample );
% Filtered subcarrier object
scoF = FilterSCO( bTx, aTx, sco );

% Setup the modulator:
phi0Tx = 0;
phiDotTx = 2*pi*fRf;


% Set up the geometry
range = 25e6; % 25 000 km to start with:
rangeRate = -500; % m/s
rangeAcc = 0;

geom.range = range;
geom.rangeRate = rangeRate;
geom.rangeAcc = rangeAcc;

% Set up the Ionosphere:
TECU = 100;

% Set up the demodulator:
phi0Rx = 0;
phiDotRx = 2*pi*( fIF - fRf );

% Set up the rx filter:
[bRx, aRx] = butter( 6, (BWRx/2)/(fSample/2) ); % SCO Butterworth Filter


%%
% Simulation start:
Ptx = 25; % Watts
antGainTx = 10^(0.1*32);
EIRP = Ptx * antGainTx;

antGainRx = 2;

tRxSample = ceil( (Ttx + geom.range/kLightSpeed )*fSampleGen )/fSampleGen ;

figure;
genState = [];
%%
% Loop over the ticks:
for tick = 1:numTicks
    disp( [ 'Tick: ' num2str(tick) ] );
    tic;

    % The time tagged SCO is generated on the satellite:
    % Note: sv filter is assumed not to be time varying here:
    scoTx = TagSCO( scoF, Ttx );
    scoTx.phi0 = phi0Tx;
    scoTx.phiDot = phiDotTx;

    % Scale it up by the EIRP
    scoTx.scaleFactor = scoTx.scaleFactor * sqrt( EIRP );

    % Apply the free space propagation:
    scoFS = ApplyFreeSpace( scoTx, geom );

    % Apply the Ionosphere:
    scoIono = ApplyIono( scoFS, TECU );

    % Apply the antenna
    scoRx = scoIono;
    scoRx.scaleFactor = scoIono.scaleFactor * antGainRx;

    % Apply the demodulator
    scoDemod = scoRx;
    scoDemod.phi0 = scoFS.phi0 + phi0Rx;
    scoDemod.phiDot = scoFS.phiDot/(1+scoFS.timeDilation) + phiDotRx;

    % Apply the baseband equivalent filter:
    scoRxFilt = FilterSCO( bRx, aRx, scoDemod );

    % Now propagate:
    Ttx = Ttx + tickPeriod;
    phi0Tx = phi0Tx + phiDotTx * tickPeriod;


    tickPeriodTrue = tickPeriod / ( 1 - scoTx.timeDilation );

    geom.range = geom.range + geom.rangeRate * tickPeriodTrue + 0.5* geom.rangeAcc * tickPeriodTrue^2;

    phi0Rx = phi0Rx + phiDotRx * tickPeriodTrue;
    toc;

    if tick > 1
        ttRx = tRxSample:(1/fSampleGen):scoRxFilt.Trx;
        tic;
        [ifSig, genState] = GenerateSignalFromTaggedSCOs( ...
            scoRxFiltPrev, scoRxFilt, ttRx, theChips, fc, genState );
        toc;
        tRxSample = ttRx(end) + 1/fSampleGen;

        plot( ttRx*fc, [ real( ifSig ); imag( ifSig ) ], '.-' );
        grid on; hold all;

    end

    scoRxFiltPrev = scoRxFilt;

    %plot( (scoRxFilt.x + scoRxFilt.Trx)*fc, [ real(scoRxFilt.y); imag( scoRxFilt.y)] * scoRx.scaleFactor );
    %grid on; hold all;

end
