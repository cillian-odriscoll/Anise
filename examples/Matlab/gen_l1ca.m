%%
% Simple script to generate a millisecond of the GPS L1 C/A signal

% SETUP
init;

fc = fo; % Code rate

fSampleBB = 15*fo; % Baseband sampling rate
fSample = 100*pi*fo; % SCO sampling rate

BW = 12e6; % Two sided bandwidth
[b, a] = butter( 6, (BW/2)/(fSample/2) ); % SCO Butterworth Filter

fDoppler = -3e3; % Doppler shift

fRf = fL1; % RF centre frequency
fIF = 0;

tickPeriod = 0.001; % Assume we get tagged SCOs every millisecond

% Start with the BPSK subcarrier:
sco = MakeSubCarrier( @(t)BpskSubCarrier( t, fc ), 3/fc, fSample );

% Filter it:
scoF = FilterSCO( b, a, sco );
%scoF = sco;

% Set up time tags at the beginning and end of the ms of interest:
Ttx = 0;
Trx = 0.00001;

timeDilation = fDoppler/fRf; % Ignoring any clock effects for now
phi0 = 0;
phiDot = 2*pi*fDoppler;

tSCO1 = TagSCO( scoF, Ttx, Trx, timeDilation, phi0, phiDot );

% propagate to the end of the tick:
Ttx = Ttx + tickPeriod;
Trx = Trx + tickPeriod*(1+timeDilation);
phi0 = phi0 + tickPeriod*phiDot;

tSCO2 = TagSCO( scoF, Ttx, Trx, timeDilation, phi0, phiDot );


% Now we need to generate enough chips for one tick period:
NChips = ceil( tickPeriod*fc );

allChips = 1-2*l1ca(1:30);
allChips = allChips(:);
theChips = allChips(1:NChips);

% Now generate the baseband signal:
ttRx = (tSCO1.Trx):(1/fSampleBB):(tSCO2.Trx);
tic;
theSig = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, ttRx, theChips, fc );
toc;

% Plot the result:
figure;
plot( ttRx*fc, [ real(theSig); imag(theSig) ] );
grid on; hold all;
legend( 'Re\{x\}', 'Im\{x\}' );
xlabel( 't^{Rx} [chips]' )
ylabel( 'x(t)' )

% wipeoff the carrier
bbSig = theSig .* exp(-1j*phiDot*ttRx );
figure;

plot( ttRx*fc, [ real(bbSig); imag(bbSig) ], '.-' );
grid on; hold all;
legend( 'Re\{x\}', 'Im\{x\}' );
xlabel( 't^{Rx} [chips]' )
ylabel( 'x^{wo}(t)' )




