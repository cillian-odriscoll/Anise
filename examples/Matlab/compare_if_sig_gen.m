%%
% Simple script to generate a millisecond of the PRS signal

% SETUP
init;

fs = 15*fo;  % Subcarrier rate
fc = 2.5*fo; % Code rate
Tc = 1/fc;

fSampleBB = 62.5e6; % Baseband sampling rate
BW = 50e6; % Two sided bandwidth
[b, a] = butter( 12, (BW/2)/(fSample/2) ); % SCO Butterworth Filter

fDoppler = 0; %3e3; % Doppler shift

fRf = fL1; % RF centre frequency
fIF = 0;

tickPeriod = 0.01; % Assume we get tagged SCOs every millisecond

% Start with the PRS subcarrier:
sco = MakeSubCarrier( @(t)BocCosSubCarrier( t, fs, fc ), 1/fc, fSample, Tsco );

% Filter it:
scoF = FilterSCO( b, a, sco );
%scoF = sco;
scoF = TrimSCO( scoF, 0, 3*Tc );

% Set up time tags at the beginning and end of the ms of interest:
Ttx = 0;
Trx = 0.00001;

timeDilation = fDoppler/fRf; % Ignoring any clock effects for now
phi0 = 0;
phiDot = 2*pi*fDoppler;

tSCO1 = TagSCO( scoF, Ttx, Trx, timeDilation, phi0, phiDot );

% propagate to the end of the tick:
Ttx = Ttx + tickPeriod;
Trx = Trx + tickPeriod*(1+timeDilation);
phi0 = phi0 + tickPeriod*phiDot;

tSCO2 = TagSCO( scoF, Ttx, Trx, timeDilation, phi0, phiDot );


% Now we need to generate enough chips for one tick period:
NChips = ceil( tickPeriod*fc );

PRN = 1;
allChips = 1-2*l2cm(PRN);
chipGen = PeriodicCodeGenerator( allChips, Tc );

% Now generate the baseband signal:
ttRx = ( (tSCO1.Trx):(1/fSampleBB):(tSCO2.Trx-1/fSampleBB) ) - tSCO1.Trx;
disp( ['Calling original generator'] );
tic;
theSigOrig = GenerateSignalFromTaggedSCOs( tSCO1, tSCO2, ttRx, chipGen );
Torig = toc;
disp( [ 'Done in ' num2str( Torig ) ' s' ] );

disp( ['Calling new generator'] );
tic;
theSigNew = GenerateSignalFromTaggedSCOs_Conv( tSCO1, tSCO2, ttRx, chipGen );
Tnew = toc;
disp( [ 'Done in ' num2str( Tnew ) ' s' ] );

% Plot the result:
figure;
plot( ttRx*fc, [ real(theSigNew); imag(theSigNew) ] );
grid on; hold all;
legend( 'Re\{x\}', 'Im\{x\}' );
xlabel( 't^{Rx} [chips]' )
ylabel( 'x(t)' )

% wipeoff the carrier
bbSigNew = theSigNew .* exp(-1j*phiDot*ttRx );
bbSigOrig = theSigOrig .* exp(-1j*phiDot*ttRx );
figure;

plot( ttRx*fc, [ real(bbSigNew); imag(bbSigNew) ], '.-' );
grid on; hold all;
legend( 'Re\{x\}', 'Im\{x\}' );
xlabel( 't^{Rx} [chips]' )
ylabel( 'x^{wo}(t)' )

scGenPrs = PeriodicCodeGenerator( [1, -1, -1, 1], 1/(4*fs) );
sigGenPrs = ChainChipGenerator( { chipGen, scGenPrs } );

localReplica = generate( sigGenPrs, ttRx );


[ rrNew, tauNew ] = xcorr( bbSigNew, localReplica );
[ rrOrig, tauOrig ] = xcorr( bbSigOrig, localReplica );

figure;
plot( tauNew*fc/fSampleBB, abs( rrNew ), ...
    tauOrig*fc/fSampleBB, abs( rrOrig ) );

legend( 'New', 'Original' )






