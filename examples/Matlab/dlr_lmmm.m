%
% Land mobile multipath model example

init;

% Here we load a sequence of Channel Impulse Responses (CIR) from a .mat
% file. These responses were generated using the DLR Land Mobile Multipath
% Model V3.0 which is available here:
%
% http://www.dlr.de/kn/en/desktopdefault.aspx/tabid-8610/14830_read-36920/
%
% The particular example used here was based on 1.667 seconds of simulation
% of an urban car environment, with a sampling rate of 0.0033 seconds.
load( 'Data/CIRUrbanCar.mat' );

% Here we are going to look at the impact of this multipath channel on the
% Galileo E1 OS and the Galileo E1 PRS

BWTx = 90*fo; % E1 OS very wideband transmitter
BWRx = 55*fo; % Narrower band, but still wideband at receiver
[bTx, aTx] = butter( 6, (BWTx/2)/(fSample/2) );
[bRx, aRx] = butter( 6, (BWRx/2)/(fSample/2) );

fRf = fL1;

% Setup the subcarrier objects
fcOS = fo;
fsOS = fo;
fsOS2 = 6*fo;
rhoOS = 1/11;
fcPrs = 2.5*fo;
fsPrs = 15*fo;
scoT = 3/fcOS;

scoOS = MakeSubCarrier( ...
    @(t)CBocSubCarrier(t, fsOS2, fcOS, rhoOS),...
    scoT, fSample );
scoPrs = MakeSubCarrier( ...
    @(t)BocCosSubCarrier(t, fsPrs, fcPrs), ...
    scoT, fSample );

% Generate the PRS LOC subcarrier
% We will use this in generating the correlation function in the receiver
% which saves us about 0.91 dB compared to using a full BOC subcarrier
% when only the main lobe passes through the receiver front-enD
scoPrsLoc = MakeSubCarrier( ...
    @(t)LocCosSubCarrier( t, fsPrs, fcPrs ), ...
    scoT, fSample );

% Lets plot these together:
figH = figure;
oaxH = subplot(3,1,1);

plot( scoOS.x*fcOS, scoOS.y, scoPrs.x*fcOS, scoPrs.y );

% Now let's filter that at transmission:
scoOSTx = FilterSCO( bTx, aTx, scoOS );
scoPrsTx = FilterSCO( bTx, aTx, scoPrs );

%Normalise transmit power:
scoOSTx = NormaliseSCO( scoOSTx );
scoPrsTx = NormaliseSCO( scoPrsTx );

txAxH = subplot(3,1,2);
plot( scoOSTx.x*fcOS, scoOSTx.y, scoPrsTx.x*fcOS, scoPrsTx.y );

% Now we generate the received signal in the absence of MP
scoOSRx = FilterSCO( bRx, aRx, scoOSTx );
scoPrsRx = FilterSCO( bRx, aRx, scoPrsTx );
rxAxH = subplot(3,1,3);
plot( scoOSRx.x*fcOS, scoOSRx.y, scoPrsRx.x*fcOS, scoPrsRx.y );

linkaxes( [oaxH, txAxH, rxAxH ] );
xlim( [-0.5, 1.5] );

%mpFigH = figure;
%osMpAxH = subplot(1,2,1);
%grid on; hold on;
%set( osMpAxH, 'NextPlot', 'replacechildren' );
%prsMpAxH = subplot(1,2,2);
%grid on; hold on;
%set( prsMpAxH, 'NextPlot', 'replacechildren' );

mpRoFigH= figure;
osMpRoAxH = subplot(1,2,2);
grid on; hold on;
xlabel( 'Delay [Chips]' )
title( 'E1C')
set( osMpRoAxH, 'NextPlot', 'replacechildren' );

prsMpRoAxH = subplot(1,2,1);
grid on; hold on;
xlabel( 'Delay [Chips]' )
title( 'E1A' )
set( prsMpRoAxH, 'NextPlot', 'replacechildren' );
linkaxes( [ osMpRoAxH, prsMpRoAxH ] );


hTitle = axes();
set( hTitle, 'visible', 'off' );


skip = 1;
nCir = floor( length(CIR)/skip );
ff = linspace( -BWRx/2, BWRx/2, 301 );
tfd = zeros( nCir, length(ff) );

ind = 0;

Ts = 1/300;
% Now we loop over the channel impulse response and show the impact on the
% two subcarriers over time:
for ii = 1:skip:length( CIR )
    % The Channel Impulse Response is a cell array. Each element contains
    % the CIR for a given time instant. The CIR consists of one row for each
    % reflection (including LOS) and the columns are:
    % 1. Delay (s)
    % 2. Complex Channel Response
    % 3. Echo number
    thisCir = CIR{ii};
    ind = ind+1;

    bMp = MakeMultipathFilter( thisCir, fSample );

    tfd(ind,:) = freqz( bMp, 1, ff, fSample );

    scoOSMp = FilterSCO( bMp, 1, scoOSRx );
    scoPrsMp = FilterSCO( bMp, 1, scoPrsRx );

    %    axes( osMpAxH );
    %    plot( scoOSMp.x*fcOS, [ real( scoOSMp.y ); imag( scoOSMp.y ) ] );
    %    grid on;
    %
    %    axes( prsMpAxH );
    %    plot( scoPrsMp.x*fcOS, [ real( scoPrsMp.y ); imag( scoPrsMp.y ) ] );
    %    grid on;
    %
    %    linkaxes( [osMpAxH, prsMpAxH ] );
    %    xlim( [-0.5, 2.0] );
    %    ylim( [-2, 2] );

    % ro - Correlation objects
    % Note that the PRS local replica is a LOC signal
    roOSMp = XCorrSCO( scoOSMp, scoOS );
    roPrsMp = XCorrSCO( scoPrsMp, scoPrsLoc );

    plot( osMpRoAxH, roOSMp.x*fcOS, [ real( roOSMp.y ); imag( roOSMp.y ) ]);

    plot( prsMpRoAxH, roPrsMp.x*fcPrs, [ real( roPrsMp.y ); imag( roPrsMp.y ) ] );

    xlim( osMpRoAxH, [-1.5, 1.5] );
    xlim( prsMpRoAxH, [-1.5, 1.5] );
    ylim( osMpRoAxH, [-1, 1] );
    ylim( prsMpRoAxH, [-1, 1] );

    hh = title( hTitle, [ 'Time: ' sprintf( '%01.2f', (ii-1)*Ts ) ' s.' ] );

    set( hh, 'Visible', 'on' );

    drawnow();

end

