# Anise: A Navigation Signal Explorer

Anise is a Matlab/Octave compatible tool for exploring Global Navigation 
Satellite System (GNSS) signals. It is structured as a collection of utility
functions with which the user can build up models of all GNSS signals,
investigate the effect of different channel models on the signals, and quickly
prototype ideas for acquisition and tracking. For a fully featured GNSS software
receiver, we recommend [gnss-sdr](http://gnss-sdr.org).

Anise can be also be used as the foundation for a complete software GNSS
simulator.


## Installing Anise

Anise has been designed with ease of use, rather than computational efficiency,
in mind. However, some core features have been implemented in C++ to provide
some performance improvements. For this reason, the core library `libanise` must
be compiled prior to using the package.

See the [Build Guide](./BUILD.md) for details.

## Using Anise

Anise comes with a number of simple examples to help you get started. They are
located in the `examples/Matlab` folder in the Anise folder. A gentle
introduction to the concepts and code at the heart of Anise can be found
in the [tutorial](./examples/Matlab/tutorial.m).


