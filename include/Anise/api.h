#ifndef INCLUDED_ANISE_API_H
#define INCLUDED_ANISE_API_H


#if defined __GNUC__
#  define __anise_ATTR_DEPRECATED __attribute__((deprecated))
#  if __GNUC__ >= 4
#    define __anise_ATTR_EXPORT   __attribute__((visibility("default")))
#    define __anise_ATTR_IMPORT   __attribute__((visibility("default")))
#  else
#    define __anise_ATTR_EXPORT
#    define __anise_ATTR_IMPORT
#  endif
#elif defined __clang__
#  define __anise_ATTR_DEPRECATED __attribute__((deprecated))
#  define __anise_ATTR_EXPORT     __attribute__((visibility("default")))
#  define __anise_ATTR_IMPORT     __attribute__((visibility("default")))
#elif _MSC_VER
#  define __anise_ATTR_DEPRECATED __declspec(deprecated)
#  define __anise_ATTR_EXPORT     __declspec(dllexport)
#  define __anise_ATTR_IMPORT     __declspec(dllimport)
#else
#  define __anise_ATTR_DEPRECATED
#  define __anise_ATTR_EXPORT
#  define __anise_ATTR_IMPORT
#endif


#ifdef anise_EXPORTS
#define anise_PUBLIC_API __anise_ATTR_EXPORT
#else
#define anise_PUBLIC_API __anise_ATTR_IMPORT
#endif

#endif // #ifndef INCLUDED_ANISE_API_H
