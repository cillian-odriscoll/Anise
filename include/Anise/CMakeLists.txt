########################################################################
# Install public header files
########################################################################
install(FILES
    api.h
    Anise.h
    GnssTime.h
    Systems.h
    DESTINATION include/Anise
)

