#ifndef INCLUDED_GNSS_TIME_H
#define INCLUDED_GNSS_TIME_H

#include <Anise/api.h>
#include <Anise/Systems.h>
#include <memory>
#include <ostream>

namespace Anise {

    /// Forward declaration:
    class GnssTimeInterval;

    class GnssTimeImpl;

    /*!
     * Simple class to capture the range of time scales needed in GNSS
     *
     * Includes a system identifier, a week number and time stored as the number
     * of seconds into the week as an integer plus a floating point representation
     * of the time sub one second.
     *
     * Sample usage:
     *
     *  int weekNumber = 1884;
     *  int tow = 345600;
     *  double fracTow = 0.0786;
     *
     *  GnssTime currTime( Anise::Systems::GPS,
     *      Anise::GnssTimeInterval::Weeks( weekNumber ) +
     *      Anise::GnssTimeInterval::Seconds( tow ) +
     *      Anise::GnssTimeInterval::Seconds( fracTow ) );
     */
    class anise_PUBLIC_API GnssTime
    {
        private:

	   
            // Disable visual studio warning C4251 about 
            // making the definition of GnssTimeImpl public
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4251)
#endif
            /// Private implementation class
            std::shared_ptr< GnssTimeImpl > mImpl;
#ifdef _MSC_VER
#pragma warning(pop)
#endif


        public:
            GnssTime();
            GnssTime( GnssSystem const &sys );
            GnssTime( GnssSystem const &sys, GnssTimeInterval const & timeInterval);

            /// Copy Constructors
            /// Ensure Deep copy of implementation
            GnssTime( const GnssTime&rhs );
#if defined(__cplusplus) && (__cplusplus >= 201103L)
            GnssTime( GnssTime&& rhs );
#endif

            //! Get the system:
            GnssSystem const& System(void) const;

            //! Get the week number
            int Week( void ) const;

            //! Get the time of Week in seconds
            double TOW( void ) const;


            GnssTime &operator=( GnssTime const&otherTime );
            GnssTime &operator+=( GnssTimeInterval const &dT );
            GnssTime &operator-=( GnssTimeInterval const &dT );

            anise_PUBLIC_API friend GnssTimeInterval operator-( GnssTime lhs, GnssTime const &rhs );
            anise_PUBLIC_API friend GnssTime operator-( GnssTime lhs, GnssTimeInterval const &rhs );
            anise_PUBLIC_API friend GnssTime operator+( GnssTime lhs, GnssTimeInterval const &rhs );

            anise_PUBLIC_API friend bool operator==( GnssTime const &lhs, GnssTime const& rhs );
            anise_PUBLIC_API friend bool operator!=( GnssTime const &lhs, GnssTime const& rhs );

            anise_PUBLIC_API friend bool operator<( GnssTime const &lhs, GnssTime const& rhs );
            anise_PUBLIC_API friend bool operator>( GnssTime const &lhs, GnssTime const& rhs );
            anise_PUBLIC_API friend bool operator<=( GnssTime const &lhs, GnssTime const& rhs );
            anise_PUBLIC_API friend bool operator>=( GnssTime const &lhs, GnssTime const& rhs );

            anise_PUBLIC_API friend std::ostream &operator<<( std::ostream &os, GnssTime const& rhs );
    };

    /// Forward declaration of the GnssTimeIntervalImpl
    class GnssTimeIntervalImpl;

    /*!
     * Simple class to capture the range of time scales needed in GNSS
     */
    class anise_PUBLIC_API GnssTimeInterval
    {
        private:
            // Prevent unitialised construction
            GnssTimeInterval(){};

            // Disable visual studio warning C4251 about 
            // making the definition of GnssTimeImpl public
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4251)
#endif
            /// Private implementation class
            // Store a pointer to the implementation:
            std::shared_ptr< GnssTimeIntervalImpl > mImpl;
#ifdef _MSC_VER
#pragma warning(pop)
#endif

            // Allow the static members to construct objects of this class
            // when then create an implementation object:
            GnssTimeInterval( std::shared_ptr< GnssTimeIntervalImpl > impl )
                : mImpl( impl ) {};

        public:

            /// Copy Constructors
            /// Ensure Deep copy of implementation
            GnssTimeInterval( const GnssTimeInterval &rhs );
#if defined(__cplusplus) && (__cplusplus >= 201103L)
            GnssTimeInterval( GnssTimeInterval&& rhs );
#endif

            /// Accessors:
            // @{

            //!
            // Get the time interval in seconds.
            // WARNING: this may cause overflow if the interval is large:
            double AsSeconds( void ) const;

            //!
            // Get the time interval in weeks
            // This returns the whole integer number of weeks in the time interval
            int AsWeeks( void ) const;

            // @}

            /// Static member functions to create time intervals:
            // @{
            static GnssTimeInterval Weeks( int  numWeeks );

            static GnssTimeInterval Days( int numDays );
            static GnssTimeInterval Hours( int numHours );

            static GnssTimeInterval Seconds( double numSeconds );

            static GnssTimeInterval MilliSeconds( double numMs );
            static GnssTimeInterval MicroSeconds( double numUs );
            static GnssTimeInterval NanoSeconds( double numNs );
            // @}

            GnssTimeInterval &operator=( GnssTimeInterval otherInterval );
            GnssTimeInterval &operator+=( GnssTimeInterval const& dT );
            GnssTimeInterval &operator-=( GnssTimeInterval const& dT );

            anise_PUBLIC_API friend GnssTimeInterval operator+( GnssTimeInterval lhs, GnssTimeInterval const& rhs );
            anise_PUBLIC_API friend GnssTimeInterval operator-( GnssTimeInterval lhs, GnssTimeInterval const& rhs );

            anise_PUBLIC_API friend bool operator==( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );
            anise_PUBLIC_API friend bool operator!=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );

            anise_PUBLIC_API friend bool operator<( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );
            anise_PUBLIC_API friend bool operator>( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );
            anise_PUBLIC_API friend bool operator<=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );
            anise_PUBLIC_API friend bool operator>=( GnssTimeInterval const &lhs, GnssTimeInterval const& rhs );

            anise_PUBLIC_API friend std::ostream &operator<<( std::ostream &os, GnssTimeInterval const &rhs );
    };


} // namespace Anise

#endif // #ifndef INCLUDED_GNSS_TIME_H
