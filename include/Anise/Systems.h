#ifndef INCLUDED_ANISE_SYSTEMS_H
#define INCLUDED_ANISE_SYSTEMS_H

#include <Anise/api.h>
#include <string>

namespace Anise {

    //! TODO: cillian Better GnssSystem class
    typedef unsigned int GnssSystem;

    namespace Systems {

        static const GnssSystem UNKNOWN = 0;
        static const GnssSystem GPS = 1;
        static const GnssSystem GLONASS = 2;
        static const GnssSystem GALILEO = 3;
        static const GnssSystem BEIDOU = 4;

        static const GnssSystem WAAS = 5;
        static const GnssSystem EGNOS = 6;
        static const GnssSystem MSAS = 7;
        static const GnssSystem GAGAN = 8;
        static const GnssSystem SDCM = 9;

        static const GnssSystem QZSS = 10;
        static const GnssSystem IRNSS = 11;

        anise_PUBLIC_API std::string SystemToString( GnssSystem const &sys );

    }

} // namespace Anise


#endif // #ifndef INCLUDED_ANISE_SYSTEMS_H

