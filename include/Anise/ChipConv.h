/*!
* \file ChipConv.h
* \brief Definitions for chip convolution functions
* \author Cillian O'Driscoll
*
* Copyright (C) 2016 Cillian O'Driscoll
*
*
* All rights reserved.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDED_CHIP_TIME_H
#define INCLUDED_CHIP_TIME_H

#include <complex>
#include <Anise/api.h>

namespace Anise {


    /*!
     * \brief Convolve high resolution pulse shape with a sequence of chips
     *
     * GNSS signals can be expressed using the convolutional model as:
     *
     *      $x(t) = g(t) * s(t)$
     * 
     * where:
     *  $x(t)$ is a baseband signal
     *  $g(t)$ is an infinite train of impulses:
     *
     *      $g(t) = \sum_{n=-\infty}^{\infty} g_n \delta( t - n T_c)$
     *
     *  $s(t)$ is a finite duration pulse of length $T_c$ seconds
     *
     * This function computes values of $x(t)$ at discrete epochs $t = k T_s^{bb}$, 
     * given a high resolution representation of $s(t)$, evaluated at points
     * $t = l T_s^{\mathrm{chip}}$, where typically $T_s^{bb} \gg T_s^{\mathrm{chip}}$.
     *
     * Note that the pulse shape $s(t)$ may be filtered, in which case the duration
     * may extend beyond $T_c$. In this case there will be inter-symbol interference
     * i.e. each output sample may be affected by multiple chips.
     *
     * In these functions we assume that duration of the pulse is given by:
     *
     *  $T_p = \frac{N_p}{ \tau_c} T_c$
     *
     * where $N_p$ is the number of samples in the high resolution pulse,
     * $\tau_c$ is the number of samples per chip. To compute the output sample
     * at $k T_s^{bb}$ requires $N$ \emph{previous} chips in addition to the
     * current chip, where:
     *
     *  $N = \mathrm{ceil}\left( \frac{T_p}{T_c} \right) - 1$
     *
     * We assume that we are given sufficient chips to generate all requested
     * output samples, and that the $N+1$th chip in the input vector is the
     * "current" chip at the time of the first output sample.
     *
     * \param chip_shape - Vector of values of the chip pulse
     * \param len_chip_shape - The length of chip_shape
     * \param the_chips - Vector of chips to convolve with chip_shape
     * \param len_chips - The length of the_chips
     * \param tau0 - The phase of the first output sample in bb_sig in units of
     * samples of chip_shape
     * \param tauChip - The duration of one chip in units of samples of 
     * chip_shape
     * \param dTau - The increment in chip_shape samples for every step of
     * one output sample (samples in bb_sig)
     * \param bb_sig - Vector where the generated baseband samples will be stored
     * \param len_bb_sig - The length of bb_sig
     */
    anise_PUBLIC_API void chip_conv_crc(
            std::complex< double > const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rrr(
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_ccc(
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rcc(
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_crc_fxpt32(
            std::complex< double > const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rrr_fxpt32(
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_ccc_fxpt32(
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rcc_fxpt32(
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_crc_fxpt64(
            std::complex< double > const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rrr_fxpt64(
            double const* chip_shape, int len_chip_shape,
            double const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            double *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_ccc_fxpt64(
            std::complex< double > const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

    anise_PUBLIC_API void chip_conv_rcc_fxpt64(
            double const* chip_shape, int len_chip_shape,
            std::complex< double > const* the_chips, int len_chips,
            double tau0, double tauChip, double dTau,
            std::complex< double > *bb_sig, int len_bb_sig );

}; // namespace Anise

#endif
